echo "-------------------------------------"
echo "building docker image"
echo "-------------------------------------"
docker build -t school-db-front .
echo

echo "-------------------------------------"
echo "launching docker container"
echo "-------------------------------------"
docker run -p 3000:3000 --rm -it --net=mynet  school-db-front