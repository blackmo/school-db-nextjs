
//======================================================================
// Api Home Route  
//======================================================================
const HOST_IP = process.env.BACKEND_HOST_IP
const HOST_PORT = process.env.BACKEND_HOST_PORT
const BASE = 'http://' + HOST_IP + ':' + HOST_PORT +'/home/school' 
const BASE2 = 'http://' + HOST_IP + ':' + HOST_PORT +'/home/web' 

//======================================================================
// Api Address Routes  
//======================================================================
export  class SchoolStudent {
    
    public static readonly REGISTER: string = BASE + '/student/register'
    public static readonly SEARCH: string = BASE + '/student/search'
    public static readonly SEARCH_BY: string = BASE + '/student/search/by'
    public static readonly UPDATE: string = BASE + '/student/update'
}

export class SchoolAdviser {

    public static readonly REGISTER = BASE + '/adviser/register'
    public static readonly SEARCH: string = BASE + '/adviser/search'
    public static readonly UPDATE: string = BASE + '/adviser/update'
    public static readonly GET_ALL: string = BASE + '/adviser/get/all'
}

export class SchoolAdvisory {

    public static readonly REGISTER = BASE + '/advisory/register'
    public static readonly SEARCH: string = BASE + '/advisory/search'
    public static readonly UPDATE: string = BASE + '/advisory/update'
    public static readonly GET_ALL: string = BASE + '/advisory/get/all'
}

export class SchoolRefAdvisory {

    public static readonly ADD_REFERENCE = BASE + '/ref-advisory/add'
    public static readonly DELETE_REFERENCE = BASE + '/ref-advisory/delete'
    public static readonly SEARCH_BY = BASE + '/ref-advisory/find/by'
}

export class SchoolUploadBatch {
    public static readonly UPLOAD_REGISTER: string = BASE + '/upload/register-csv'
}

export class PersonCalls {
    public static readonly UPDATE = BASE + '/person/update'
}

export class UserCalls {
    public static readonly UPDATE_PASSWORD = BASE2 + '/user/update/password'
    public static readonly LOGIN = BASE2 + '/user/login'
    public static readonly LOGOUT = BASE2 + '/user/logout'
}