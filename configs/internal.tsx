
export class InternalAddress {
    public static readonly LOGIN = 'api/login'
    public static readonly LOGOOUT = 'api/logout'


    public static readonly STUDENT_REGISTER = '/api/student/register'
    public static readonly STUDENT_SEARCH = '/api/student/search'
    public static readonly STUDENT_SEARCH_BY = '/api/student/search-by'
    public static readonly STUDENT_UPDATE = '/api/student/update'

    public static readonly ADVISER_REGISTER = '/api/adviser/register'
    public static readonly ADVISER_SEARCH = '/api/adviser/search'
    public static readonly ADVISER_UPDATE = '/api/adviser/update'
    public static readonly ADVISER_GET_ALL = '/api/adviser/get/all'

    public static readonly ADVISORY_REGISTER = '/api/advisory/register'
    public static readonly ADVISORY_SEARCH = '/api/advisory/search'
    public static readonly ADVISORY_DETAILS = '/api/advisory/details'
    public static readonly ADVISORY_UPDATE = '/api/advisory/update'
    public static readonly ADVISORY_GET_ALL = '/api/advisory/get/all'

    
    public static readonly REF_ADVISORY_STUDENT_ADD = '/api/ref/add/advisory-student'
    public static readonly REF_ADVISORY_STUDENT_DELETE = '/api/ref/delete/advisory-student'
    public static readonly REF_ADVISORY_STUDENT_SEAECH_BY = '/api/ref/search-by'

    public static readonly PERSON_UPDATE = '/api/person/update'

    public static readonly UPLOAD_BATCH_REG_CSV = '/api/upload/registerCsv'

    public static readonly UPDATE_PASSWORD = '/api/user/update'
    public static readonly USER_LOGOUT = '/api/user/logout'
    public static readonly USER_LOGIN = '/api/user/login'
}