export const  SC_GRADLE_LEVELS = [ 7, 8, 9, 10, 11, 12]
export const  SC_GRADE_SECTIONS = [1, 2, 3, 4]

export const SX_DEFAULT_GRADE_LEVELS = [ 'All', ...SC_GRADLE_LEVELS]
export const SX_DEFAULT_GRADE_SECTIONS = [ 'All', ...SC_GRADE_SECTIONS]