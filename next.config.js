require('dotenv').config()
const webpack = require('webpack')
const withSass = require('@zeit/next-sass');

module.exports = withSass({
  cssModules: true,
  cssLoaderOptions: {
    importLoaders: 2,
  },
  webpack: config => {
    config.module.rules.forEach(rule => {
      config.plugins.push(
        new webpack.EnvironmentPlugin(process.env)
      )
        if(rule.test) {
          if (rule.test.toString().includes('.scss')) {
                rule.rules = rule.use.map(useRule => {
                  if (typeof useRule === 'string') {
                    return { loader: useRule };
                  }          if (useRule.loader === 'css-loader') {
                    return {
                      oneOf: [
                        {
                          test: new RegExp('.global.scss$'),
                          loader: useRule.loader,
                          options: {},
                        },
                        {
                          loader: useRule.loader,
                          options: { modules: true }
                        },
                      ],
                    };
                  }
                  return useRule;
                });
                delete rule.use;
              }
            }
        }
    );
    return config;
  },
});

// module.exports = withSass({
//   sassLoaderOptions: {
//     includePaths: ["absolute/path/a", "absolute/path/b"]
//   }
// })