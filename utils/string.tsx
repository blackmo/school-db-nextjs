export const sanitizeString = (input: string) => {
    return input.replace(/\s\s+/g, ' ')
}

export const sanitizeMobileNum = (num) => {
    return num.replace(/[-]/g, '')
}

export const checkRequiredFields = (input, keys: string[]) => {
    for (let i = 0; i < keys.length; i++) {
        if (input[keys[i]] === '' || input[keys[i]] === undefined) {
            return false
        }
    }
    return true
}

export const validateMobileNum = (number: string) => {
    if (number === '' || number === undefined) {
        return false
    } else if (number === null) {
        return true
    } else {
        const output = sanitizeMobileNum(number)
        return !isNaN(output)
    }
}