export default class PrompStatus {
    public constructor(open: boolean, message: string,  index: number, severity?: string) {
        this.open = open
        this.message = message
        this.index = index
        this.severity = severity
    } 

    public open: boolean
    public message: string
    public index: number
    public severity: string

    public static ERROR = 'error'
    public static INFO = 'info'
    public static WARNING =  'warning'
    public static SUCCESS = 'success'
}