export default class DialogMessage {
    public constructor(title: string, message: string) {
        this.title = title
        this.message = message
    }
    public title: string
    public message: string
}