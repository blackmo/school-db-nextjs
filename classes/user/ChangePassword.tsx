
export default class ChangePassword {

    constructor(oldPassword: string, newPassword: string, conPassword: string) {
        this.oldPassword = oldPassword
        this.newPassword = newPassword
        this.conPassword = conPassword
    }

    public oldPassword: string
    public newPassword: string
    public conPassword: string
}
