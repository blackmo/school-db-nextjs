import Person from "../person/Person"
import BaseStudent from "./BaseStudent"

class Student extends BaseStudent{
    public mother?: Person
    public father?: Person
}

export default Student