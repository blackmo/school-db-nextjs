export default class StudentDetails {
    public constructor(id: number, lrnID: string, lastName: string, firstName: string, middleName: string, status: string) {
        this.id = id
        this.lrnID = lrnID
        this.lastName = lastName
        this.firstName = firstName
        this.middleName = middleName
        this.status = status
    }
    public id: number
    public lrnID: string
    public lastName: string
    public firstName: string
    public middleName: string
    public status: String
}