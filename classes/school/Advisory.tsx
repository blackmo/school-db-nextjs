import Adviser from "./Adviser"
import Student from "./Student"

export default class Advisory {
    public id: number = 0
    public classYear: number
    public classLevel: number
    public advisoryName: string
    public sectionName: string
    public sectionNumber: number
    public adviser: Adviser = new Adviser()
    public listStudent: Student[] = []
    public listLrnInString: string = ''
}