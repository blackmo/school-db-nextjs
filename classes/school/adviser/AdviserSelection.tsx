import Adviser from "../Adviser"
import PersonUtils from "../../../functions/person/PersonUtils"

export class AdviserSelection {
    public constructor(id: number, fullName: string, index: number) {
        this.id = id
        this.fullName = fullName
        this.index = index
    }
    public id: number
    public fullName: string
    public index: number
}

export class CategorizedSelection extends AdviserSelection {
  public constructor(firstLetter: string, index: number, id: number, fullName: string) {
    super(id, fullName, index)
    this.firstLetter = firstLetter
  }

  public firstLetter: string
}
