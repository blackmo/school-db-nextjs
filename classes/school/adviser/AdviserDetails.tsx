export default class AdviserDetials {
    public constructor(id: number, lastName: string, firstName: string, middleName: string) {
        this.id = id
        this.lastName = lastName
        this.firstName = firstName
        this.middleName = middleName
    }

    public id: number
    public lastName: string
    public firstName: string
    public middleName: string
}