import Person from "../person/Person"

class BaseStudent {
    public id: number
    public lrnID: string
    public person: Person = new Person()
    public status: string = ''
}

export default BaseStudent