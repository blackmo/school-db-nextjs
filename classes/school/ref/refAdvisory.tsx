export class RefAdvisoryStudent {

    public constructor(advisoryId: number, studentId: number) {
        this.advisoryId = advisoryId
        this.studentId = studentId
    }

    public advisoryId: number
    public studentId: number
}