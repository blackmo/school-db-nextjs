export default class AdvisoryTableEntry {

    public constructor(
    advisoryId: number,
    classYear: number,
    classLevel: number,
    sectionNumber: number,
    advisoryName: string,
    adviserName: string,
    ) {
        this.id = advisoryId
        this.classYear = classYear
        this.classLevel = classLevel
        this.sectionNumber = sectionNumber
        this.advisoryName = advisoryName
        this.adviserName = adviserName
    }

    public id: number
    public classYear: number
    public classLevel: number
    public sectionNumber: number
    public advisoryName: string
    public adviserName: string
}