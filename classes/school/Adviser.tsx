import Person from "../person/Person"

export default class Adviser {
    public id: number = 0
    public person: Person = new Person()
    public licenseNumber: string
}
