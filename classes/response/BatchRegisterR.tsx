import Student from "../school/Student";
import { BRegFailedResponse } from "./BRegFailedResponse";

export default class BatchREgisterR {
    public success: Student[]
    public failed: BRegFailedResponse[]
}