export default class SearchStudentP {

    public byLrnId: boolean 
    public lastName: string
    public status: string

    public lrnID: string
    public classYear: number
    public classLevel: string
    public classSection: string

}