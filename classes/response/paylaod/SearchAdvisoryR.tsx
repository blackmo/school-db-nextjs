import Person from "../../person/Person"

export class SearchAdvisoryR {
    public advisoryId: number
    public classYear: number
    public classLevel: number
    public advisoryName: string
    public sectoinName: string
    public sectionNumber: number
    public adviser: Person
}