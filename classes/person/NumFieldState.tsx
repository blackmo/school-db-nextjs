class NumFieldState {
    public constructor(enabled?: boolean) {
        this.enabled = enabled
    }

    public  enabled: boolean = true
    public number: string = ''
}

export default NumFieldState