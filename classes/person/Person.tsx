
class Person {
    id: number = 0
    firstName: string
    middleName?: string
    lastName: string
    birthday: string
    address: string
    mobileNo: string
    gender: string
}

export default Person