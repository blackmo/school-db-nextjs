import Person from "./Person";
import NumFieldState from "./NumFieldState";

class PersonState {
    public constructor(person: Person){
        this.person = person
    }
    public person: Person
    public includeNumField: boolean =  true 
}

export default PersonState