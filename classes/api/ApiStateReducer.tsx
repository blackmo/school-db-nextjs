export class ApiStateReducer {
    public call: CallStatus
    public actionTitle: string
    public message: string
    public type: string
    public title: string
}

export interface CallStatus {
    apiCall(data)
    onSuccess(data)
    onUpdate?(data)
    onError(data)
    successMessage: string
}

export enum TYPE {
    save,
    search,
    upload
}

