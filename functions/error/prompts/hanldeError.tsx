import PrompStatus from "../../../classes/prompt/PromptStatus";

export const getErrorPrompt = (prompts: PrompStatus[]): PrompStatus =>  {
    for (let i = 0; i < prompts.length; i++) {
      if (prompts[i].open) {
        return prompts[i]
      }
    }
    return new PrompStatus(false, 'no error', 0, PrompStatus.INFO)
} 