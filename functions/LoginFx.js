import axios from 'axios'

const USER_NAME = 'username'
const LOGIN_STATUS = 'login-status'
const TOKKEN = 'tokken'

export let loginUser = async (credential) => {
    let url = 'http://localhost:3001/home/user/web/login'
    const response =  {body: {}}
    try {

        const  {data} = await axios.post(url, credential, {headers: {'Content-Type': 'application/json'}})
        response.body = data
    } catch (error) {
        response.body.message = error.message
    }

    return response
}

export const logout = async (secret) =>  {
    let url = 'http://localhost:3001/home/user/web/logout'
    let token = secret.replace(/"/g,'')
    const response =  {body: {}}
    try {
        const data = await axios({
            url: url,
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                'Authorization':`Bearer ${token}` 
            }
        })
        response.body = data
    } catch(error) {
        response.body.message = error.message
    }
}

const setUserLoginSession = (username, loginStatus, tokken) => {
    localStorage.setItem(USER_NAME, username)
    localStorage.setItem(LOGIN_STATUS, loginStatus)
    localStorage.setItem(TOKKEN, tokken)
}


const getUsetLoginSession = () => {
    const userLogSession = {}
    userLogSession.username = localStorage.getItem(USER_NAME)
    userLogSession.loginStatus = localStorage.getItem(LOGIN_STATUS)
    userLogSession.tokken = localStorage.getItem(TOKKEN)

    return userLogSession
}

const hasUserLogin = () => {
    const session = getUsetLoginSession()
    if (session.username && session.loginStatus) return true
    else return false
}


// module.exports.logReq = loginUser
module.exports.setUserLogSession = setUserLoginSession
module.exports.getUsetLoginSession = getUsetLoginSession
module.exports.hasUserLogin = hasUserLogin
