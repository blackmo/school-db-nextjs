import Person from "../../classes/person/Person"
import Student from "../../classes/school/Student"
import moment from 'moment'
import Adviser from "../../classes/school/Adviser"
import Advisory from "../../classes/school/Advisory"

export const composePerson = (name: string, data): Person => {
   const person = new Person()

   
   let validate = moment(data[`${name}_birthday`], 'L')
   let date = validate.isValid() ? validate.format('L') : moment().format('L')

   person.firstName = data[`${name}_firstName`]
   person.middleName = data[`${name}_middleName`]
   person.lastName = data[`${name}_lastName`]
   person.gender = data[`${name}_gender`]
   person.birthday =  date
   person.address = data[`${name}_address`]
   
   if (data[`${name}_includeNumber`]) {
      person.mobileNo = data[`${name}_number`]
   } else {
      person.mobileNo = null
   }

   return person
}

export const composeStudent = (data): Student => {
   const student = new Student()
   student.lrnID = data['student_lrnID']
   student.person = composePerson('student', data)

   if (data.include_father) {
      student.father = composePerson('father', data)
   } else {
      student.father = null
   }

   if (data.include_mother) {
      student.mother = composePerson('mother', data)
   } else {
      student.mother = null
   }

   return student
}

export const composeAdviser = (data): Adviser => {
   const adviser = new Adviser()
   adviser.person = composePerson('adviser', data)
   adviser.licenseNumber = data['adviser_licenseNumber']

   return adviser
}

export const comsposeAdvisory = (data): Advisory => {
   const advisory = new Advisory()

   advisory.classYear = data['classYear']
   advisory.classLevel = data['classLevel']
   advisory.advisoryName = data['advisoryName']
   advisory.sectionName  = data['sectionName']
   advisory.sectionNumber = data['sectionNumber']

   return advisory
}