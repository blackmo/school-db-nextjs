import {checkRequiredFields, validateMobileNum} from '../utils/string'
import Student from '../classes/school/Student'

export const isValidatedFields = (student: Student, includes: {father: boolean, mother: boolean}):
{mother: boolean, father:boolean} => {
  let validatedComplete = {mother : !includes.mother, father: !includes.father}
  const keys = ['firstName', 'lastName', 'address']
  if (includes.mother) {
    validatedComplete.mother = student.mother ? checkRequiredFields(student.mother, keys): false
  }

  if (includes.father) {
    validatedComplete.father = student.father ? checkRequiredFields(student.father, keys): false
  }

  return validatedComplete
}

export const isValidNumber = (student: Student, includes):
{student: boolean, mother: boolean, father: boolean} => {
  const output = {student: false , mother: !includes.mother, father: !includes.father}

   if (student.person) {
     output.student = validateMobileNum(student.person.mobileNo) 
   }

   if (student.father) {
    output.father =  validateMobileNum(student.father.mobileNo) 
   }

   if (student.mother) {
    output.mother = validateMobileNum(student.mother.mobileNo)
   }
  return output
}