import Response from "../../classes/response/Response";

export function handleResponse<T>(response: Response<T>, res) {
    switch (response.code) {
        case 'SUCCESS': {

            res.status(200).json(response)
            break;
        }

        case 'UNAUTHORIZED': {
            res.status(401).json(response)
            break;
        }

        case 'FAILED' : {
            res.status(400).json(response)
            break;
        }

        default: {
            res.status(400).json(response)
        }
    }
}

export function handleErrorResponse<T>(error): Response<T> {
    if (error.response) {
        if (error.response.status === 401 && error.response.data==='') {
            return new Response<T>('UNAUTHORIZED', 'Request Unauthorized. Try relogin your account', null)
        }

        const dataReturn: Response<T> = error.response.data
        if (dataReturn) {
            return dataReturn
        } else {
            return new Response<T>('ERROR', error.message, null)
        }
    } else {
        return new Response<T>('ERROR', error.message, null)
    }
}
