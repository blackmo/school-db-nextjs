import { handleErrorResponse } from "../../api/apiHandleResponse";
import {InternalAddress} from '../../../configs/internal'

import Student from "../../../classes/school/Student";
import cookie from 'js-cookie'
import axios, { AxiosResponse } from 'axios'
import Response from "../../../classes/response/Response";
import SearchStudentP from "../../../classes/response/paylaod/SearchStudentP";


export const callRegister = async (student: Student): Promise<Response<Student>> => {
    let response: Response<Student>
    const secret = cookie.get('token').replace(/"/g,'')    

    try {
        const  dataResposne = await axios.post(
            InternalAddress.STUDENT_REGISTER,
            student,
            {headers: {secret: secret}}
        )
        response =  dataResposne.data
    } catch (error) {
        response = handleErrorResponse(error)
    }

    return response 
}

export const callSearchStudent = async (body: SearchStudentP ): Promise<Response<Student[]>> => {
    let response: Response<Student[]>
    const secret = cookie.get('token').replace(/"/g,'')    

    try {
        const  dataResposne = await axios.post(
            InternalAddress.STUDENT_SEARCH,
            body,
            {headers: {secret: secret}}
        )
        response =  dataResposne.data
    } catch (error) {
        response = handleErrorResponse(error)
    }

    return response
}

export const callSearchStudentBy = async (body: SearchStudentP ): Promise<Response<Student[]>> => {
    let response: Response<Student[]>
    const secret = cookie.get('token').replace(/"/g,'')    

    try {
        const  dataResposne = await axios.post(
            InternalAddress.STUDENT_SEARCH_BY,
            body,
            {headers: {secret: secret}}
        )
        response =  dataResposne.data
    } catch (error) {
        response = handleErrorResponse(error)
    }

    return response
}

export const callUpdateStudent = async (student: Student): Promise<Response<Student>> => {
    let response: Response<Student>
    const secret = cookie.get('token').replace(/"/g,'')    

    try {
        const  dataResposne = await axios.post(
            InternalAddress.STUDENT_UPDATE,
            student,
            {headers: {secret: secret}}
        )
        response =  dataResposne.data
    } catch (error) {
        response = handleErrorResponse(error)
    }

    return response 
}