import Student from "../../../classes/school/Student"
import StudentDetails from "../../../classes/school/student/StudentDetails";

export const updateStudentDetails = (which: string, student: Student, value): Student => {
  switch (which) {
    case 'update-student-person': {
      student.person = value
      break;
    }

    case 'update-student-mother': {
      student.mother = value
      break;
    }

    case 'update-student-father': {
      student.father = value
      break;
    }
  }
  return student
}

export const mapToStudentDetails = (students: Student[]): StudentDetails[]=> {
   return students.map((student, index) => {
     const person = student.person
     return new StudentDetails(index,student.lrnID, person.lastName, person.firstName, person.middleName, student.status)
  })

}