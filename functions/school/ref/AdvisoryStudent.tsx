import { RefAdvisoryStudent } from "../../../classes/school/ref/refAdvisory"
import { InternalAddress } from '../../../configs/internal'
import { handleErrorResponse } from '../../api/apiHandleResponse'

import Response from "../../../classes/response/Response"
import axios from 'axios'
import cookie from 'js-cookie'
import SearchStudentP from "../../../classes/response/paylaod/SearchStudentP"
import Student from "../../../classes/school/Student"

export const callAddRefAdvisory = async (ref: RefAdvisoryStudent): Promise<Response<string>> => {
    let response: Response<string>
    const secret = cookie.get('token').replace(/"/g,'')    

    try {
        const  dataResposne = await axios.post(
            InternalAddress.REF_ADVISORY_STUDENT_ADD,
            ref,
            {headers: {secret: secret}}
        )
        response = dataResposne.data
    } catch (error) {
        response = handleErrorResponse(error)
    }

    return response 
}

export const callDeleteRefAdvisory = async (ref: RefAdvisoryStudent): Promise<Response<string>> => {
    let response: Response<string>
    const secret = cookie.get('token').replace(/"/g,'')    

    try {
        const  dataResposne = await axios.post(
            InternalAddress.REF_ADVISORY_STUDENT_DELETE,
            ref,
            {headers: {secret: secret}}
        )
        response = dataResposne.data
    } catch (error) {
        response = handleErrorResponse(error)
    }

    return response 
}

export const callRefAdvisorySearchBy = async (criteria : SearchStudentP): Promise<Response<Student[]>> => {
    let response: Response<Student[]>
    const secret = cookie.get('token').replace(/"/g,'')    

    try {
        const  dataResposne = await axios.post(
            InternalAddress.REF_ADVISORY_STUDENT_SEAECH_BY,
            criteria,
            {headers: {secret: secret}}
        )
        response = dataResposne.data
    } catch (error) {
        response = handleErrorResponse(error)
    }

    return response 
}