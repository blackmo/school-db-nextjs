
import { handleErrorResponse} from "../../api/apiHandleResponse";
import { InternalAddress } from '../../../configs/internal'
import { SchoolAdviser } from "../../../configs/external";

import cookie from 'js-cookie'
import axios from 'axios'
import Response from "../../../classes/response/Response";
import Adviser from "../../../classes/school/Adviser";


export const callRegisterAdviser = async (adviser: Adviser): Promise<Response<Adviser>> => {
    let response: Response<Adviser>
    const secret = cookie.get('token').replace(/"/g,'')    

    try {
        const  dataResposne = await axios.post(
            InternalAddress.ADVISER_REGISTER,
            adviser,
            {headers: {secret: secret}}
        )
        response = dataResposne.data
    } catch (error) {
        response = handleErrorResponse(error)
    }

    return response 
}

export const callSearchAdviser = async (searchCriteria: SchoolAdviser): Promise<Response<Adviser[]>> => {
    let response: Response<Adviser[]>
    const secret = cookie.get('token').replace(/"/g,'')    

    try {
        const  dataResposne = await axios.post(
            InternalAddress.ADVISER_SEARCH,
            searchCriteria,
            {headers: {secret: secret}}
        )
        response = dataResposne.data
    } catch (error) {
        response = handleErrorResponse(error)
    }

    return response 
}


export const callUpdateAdviserId = async (adviser: Adviser): Promise<Response<string>> => {
    let response: Response<string>
    const secret = cookie.get('token').replace(/"/g,'')    

    try {
        const  dataResposne = await axios.post(
            InternalAddress.ADVISER_UPDATE,
            adviser,
            {headers: {secret: secret}}
        )
        response = dataResposne.data
    } catch (error) {
        response = handleErrorResponse(error)
    }

    return response 
}

export const callGetAllAdviser = async (): Promise<Response<string>> => {
    let response: Response<string>
    const secret = cookie.get('token').replace(/"/g,'')    

    try {
        const  dataResposne = await axios.get(
            InternalAddress.ADVISER_GET_ALL,
            {headers: {secret: secret}}
        )
        response = dataResposne.data
    } catch (error) {
        response = handleErrorResponse(error)
    }

    return response 
}
