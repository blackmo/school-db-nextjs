import { AdviserSelection, CategorizedSelection } from "../../../classes/school/adviser/AdviserSelection"
import PersonUtils from "../../person/PersonUtils"
import Adviser from "../../../classes/school/Adviser"

export const transformToAdvisersSelction = (advisers: Adviser[]) : AdviserSelection[] => {
  return advisers.map((adviser: Adviser, index) => {
      return new AdviserSelection(adviser.id, PersonUtils.fullName(adviser.person), index)
    })

}

export function categorizedAdviserSx(inputData: Adviser[]): CategorizedSelection[] {
    return inputData.map((item: Adviser, index) => {
        const fullName = PersonUtils.fullName(item.person)
        const firstLetter = fullName[0].toUpperCase()
        return new CategorizedSelection(
            /[0-9]/.test(firstLetter) ? '0-9' : firstLetter,
            index,
            item.id,
            fullName
        )
    })
}