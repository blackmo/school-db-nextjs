import { InternalAddress } from '../../../configs/internal'
import { handleErrorResponse } from '../../api/apiHandleResponse'

import axios from 'axios'
import cookie from 'js-cookie'
import Advisory from "../../../classes/school/Advisory"
import Response from "../../../classes/response/Response"
import SearchAdviosryP from '../../../classes/response/paylaod/SeachAdvisoryP'

export const callRegisterAdvisory = async (adviser: Advisory): Promise<Response<Advisory>> => {
    let response: Response<Advisory>
    const secret = cookie.get('token').replace(/"/g,'')    

    try {
        const  dataResposne = await axios.post(
            InternalAddress.ADVISORY_REGISTER,
            adviser,
            {headers: {secret: secret}}
        )
        response = dataResposne.data
    } catch (error) {
        response = handleErrorResponse(error)
    }

    return response 
}

export const callSearchAdvisory = async (criteria: SearchAdviosryP): Promise<Response<Advisory>> => {
    let response: Response<Advisory>
    const secret = cookie.get('token').replace(/"/g,'')    

    try {
        const  dataResposne = await axios.post(
            InternalAddress.ADVISORY_SEARCH,
            criteria,
            {headers: {secret: secret}}
        )
        response = dataResposne.data
    } catch (error) {
        response = handleErrorResponse(error)
    }

    return response 
}

export const findOneById = async (id: number): Promise<Response<Advisory>> => {
    let response: Response<Advisory>
    const secret = cookie.get('token').replace(/"/g,'')    

    try {
        const  dataResposne = await axios.post(
            InternalAddress.ADVISORY_DETAILS,
            {id: id},
            {headers: {secret: secret}}
        )
        response = dataResposne.data
    } catch (error) {
        response = handleErrorResponse(error)
    }

    return response 
}

export const callUpdateAdvisory = async (adviser: Advisory): Promise<Response<Advisory>> => {
    let response: Response<Advisory>
    const secret = cookie.get('token').replace(/"/g,'')    

    try {
        const  dataResposne = await axios.post(
            InternalAddress.ADVISORY_UPDATE,
            adviser,
            {headers: {secret: secret}}
        )
        response = dataResposne.data
    } catch (error) {
        response = handleErrorResponse(error)
    }

    return response 
}