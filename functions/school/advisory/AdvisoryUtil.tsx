import { SearchAdvisoryR } from "../../../classes/response/paylaod/SearchAdvisoryR";
import AdvisoryTableEntry from "../../../classes/school/advisory/AdvisoryTableEntry";
import PersonUtils from "../../person/PersonUtils";
import Student from "../../../classes/school/Student";
import arraySort from 'array-sort'

export function maptReusltAdvisoryEntry(result: SearchAdvisoryR[]) {
    return result.map((item: SearchAdvisoryR, index)=> {
        return new AdvisoryTableEntry(
            item.advisoryId,
            item.classYear,
            item.classLevel,
            item.sectionNumber,
            item.advisoryName,
            PersonUtils.fullName(item.adviser)
        )
    }) 
}

export function mapAdvisoryStudentToList(students: Student[]) {
   const list =  students.map((item: Student, index: number)=> {
        return {fullName: PersonUtils.fullName(item.person), lrnId: item.lrnID, id: item.id}
    })

    return arraySort(list, 'fullName')
}