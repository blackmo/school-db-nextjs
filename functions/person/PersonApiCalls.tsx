import { InternalAddress } from "../../configs/internal";
import { handleErrorResponse } from "../api/apiHandleResponse";

import Person from "../../classes/person/Person";
import Response from "../../classes/response/Response";
import cookie from 'js-cookie'
import axios from 'axios'

export const updatePersonDetails = async (person: Person) => {
    let response: Response<Person>
    const secret = cookie.get('token').replace(/"/g,'')    

    try {
        const dataResponse = await axios.post(
            InternalAddress.PERSON_UPDATE,
            person,
            {headers: {secret: secret}}
        )

        let code = dataResponse.data.code
        let  message =  dataResponse.data.message
        let dataValue = dataResponse.data.data

        response = new Response(code, message, dataValue)
    } catch(error) {
        response = handleErrorResponse(error)
    }

    return response

}