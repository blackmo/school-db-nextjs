import Person from "../../classes/person/Person";
import moment from "moment";
import { strict } from "assert";

export default class PersonUtils {
    public static fullName(person: Person):string {
        if (!person || (person.lastName === undefined)) {
            return undefined
        }
        let middleName = person.middleName? person.middleName : ''
        return  `${person.lastName}, ${person.firstName} ${middleName}`
    }

    public static getCurrentAge(birthday: string): number {
        let today = moment().get('y');
        let birthYear = moment(birthday, 'L').get('y');

        return (today-birthYear)
    }
}