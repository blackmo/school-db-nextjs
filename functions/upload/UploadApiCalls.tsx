import { InternalAddress } from "../../configs/internal";
import { handleErrorResponse } from "../api/apiHandleResponse";

import BatchREgisterR from "../../classes/response/BatchRegisterR"
import Response from "../../classes/response/Response"
import cookie from 'js-cookie'
import axios from 'axios'

export const uploadCsvRegFile = async (file: File, header: {type: string, section: string, year: string}) => {
    let response: Response<BatchREgisterR>

    const multiForm = new FormData()
    multiForm.append(file.name, file)

    try {
        const secret = cookie.get('token').replace(/"/g,'')    
        const dataResponse = await axios.post(
            InternalAddress.UPLOAD_BATCH_REG_CSV,
            multiForm,
            {headers: {
                'Content-Type' : 'multipart/form-data',
                secret: secret,
                 ...header,
                 disposition: 'register'
             }}
        )

        let code = dataResponse.data.code
        let  message =  dataResponse.data.message
        let dataValue = dataResponse.data.data

        response = new Response(code, message, dataValue)
    } catch(error) {
        response = handleErrorResponse(error)
    }

    return response
}