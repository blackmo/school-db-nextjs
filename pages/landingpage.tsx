import { HeaderLayout, CommonLayout } from "../components/layout/Layout"; 
import { withAuhtSync, whenAuthorized, logout } from "../utils/auths";
import { makeStyles } from "@material-ui/core/styles";
import { useState } from "react";
import { AdviserComponent } from "../components/school/adviser/component";
import { StudentComponent } from "../components/school/student/component";
import { AdvisoriesCompnent } from "../components/school/advisories/component";
import { drawerStyle } from '../styles/make/component/drawer/drawerStyle'
import { SettingsComponent } from '../components/settings/SettingsComponent'

import CommonDrawer from '../components/drawer/commonDrawer'
import PeopleIcon from '@material-ui/icons/People'
import PersonIcon from '@material-ui/icons/Person'
import DescriptionIcon from '@material-ui/icons/Description';
import SettingsIcon from '@material-ui/icons/Settings';



const contentList = [
  {
    label: "Student",
    icon: () => <PersonIcon/>
  },
  {
    label: "Advisories",
    icon: () => <PeopleIcon/>
  },
  {
    label: "Adviser",
    icon: () => <DescriptionIcon/>
  },{
    label: 'Settings',
    icon: () => <SettingsIcon/>
  }
];

const getComponent = id => {
  switch (id) {
    case 0: {
      return <StudentComponent />;
    }
    case 1: {
      return <AdvisoriesCompnent />;
    }
    case 2: {
      return <AdviserComponent />;
    }
    case 3: {
      return <SettingsComponent/>
    }
  }
};

const useStyle = makeStyles(theme => drawerStyle(theme, null));

const LandingPage = () => {
  const [currentLink, setLink] = useState(0);

  const putLink = link => {
    setLink(link);
  };

  const classes = useStyle();

  return (
    <div>
      <HeaderLayout logout = {logout}/>
      <CommonLayout className={classes.container}>
        <div className={classes.root}>
          <CommonDrawer
            // className={classes.drawer}
            variant="permanent"
            anchor= 'left'
            classes={{
              paper: classes.drawerPaper
            }}
            contentList={contentList}
            onClick={putLink}
            withIcon
          />
          <div>{getComponent(currentLink)}</div>
        </div>
      </CommonLayout>
    </div>
  );
};

LandingPage.getInitialProps = async ctx => {
  whenAuthorized(ctx);
};

export default withAuhtSync(LandingPage);
