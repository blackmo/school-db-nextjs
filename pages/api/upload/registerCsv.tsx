import { SchoolUploadBatch } from "../../../configs/external"
import { handleResponse, handleErrorResponse } from "../../../functions/api/apiHandleResponse"
import axios, { AxiosResponse } from "axios"

import BatchREgisterR from "../../../classes/response/BatchRegisterR"
import Response from "../../../classes/response/Response"


export default async (req, res) => {
    const formData = req.body
    const key = req.headers.secret

    const headers = {
        year:  req.headers.year,
        section: req.headers.section,
        type: req.headers.type,
        'Content-Type' : req.headers['content-type']
    }

    try {
        let response = await upload(formData, key, headers) 
        handleResponse(response, res)
    } catch (error) {
        res.status(400).json(new Response('ERROR', error, null))
    }
}

const upload = async (formData: FormData, secret: string, headers) => {
    const url = SchoolUploadBatch.UPLOAD_REGISTER
    let token = secret.replace(/"/g,'')
    let dataResponse: Response<BatchREgisterR>
    let response: AxiosResponse

    try {
        response = await axios.post(
            url,
            formData,
            {
                headers: {
                    'Authorization':`Bearer ${token}`,
                    'disposition': 'csv',
                    ...headers }
            }
        )

        dataResponse = response.data
    } catch (error) {
        dataResponse = handleErrorResponse(error)
    }

    return dataResponse
}