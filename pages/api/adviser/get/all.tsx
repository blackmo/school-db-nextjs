import { handleResponse, handleErrorResponse } from "../../../../functions/api/apiHandleResponse"
import { SchoolAdviser } from "../../../../configs/external"
import axios, { AxiosResponse } from "axios"

import Response from "../../../../classes/response/Response"
import Adviser from "../../../../classes/school/Adviser"


export default async (req, res) => {
    const key = req.headers.secret

    try {
        let response = await getAdvisers(key) 
        handleResponse(response, res)
    } catch (error) {
        res.status(400).json(new Response('ERROR', error, null))
    }
}

const getAdvisers = async (secret: string) => {
    const url = SchoolAdviser.GET_ALL
    let token = secret.replace(/"/g,'')
    let dataResponse: Response<Adviser[]>
    let response: AxiosResponse

    try {
        response = await axios.get(
            url,
            {
                headers: {
                    'Content-Type' : 'application/json',
                    'Authorization':`Bearer ${token}` 
                }
            }
        )

        dataResponse = response.data
    } catch (error) {
        dataResponse = handleErrorResponse(error)
    }

    return dataResponse
}