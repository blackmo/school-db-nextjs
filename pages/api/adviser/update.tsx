
import { SchoolAdviser } from "../../../configs/external"
import { handleResponse, handleErrorResponse } from "../../../functions/api/apiHandleResponse"
import axios, { AxiosResponse } from 'axios'

import Response from "../../../classes/response/Response"
import Adviser from "../../../classes/school/Adviser"

export default async (req, res) => {
    const adviser = req.body
    const key = req.headers.secret

    try {
        let response = await register(adviser, key) 
        handleResponse(response, res)
    } catch (error) {
        res.status(400).body(new Response('ERROR', error, null))
    }
}

const register = async (adviser: Adviser, secret: string): Promise<Response<string>> => {
    const url = SchoolAdviser.UPDATE
    let token = secret.replace(/"/g,'')
    let dataResponse: Response<string>
    let response: AxiosResponse
    
    try {
        response = await axios.post(
            url,
            adviser,
            {
                headers: {
                    'Content-Type' : 'application/json',
                    'Authorization':`Bearer ${token}` ,
                }
            }
         )

        dataResponse = response.data
    } catch(error) {
        dataResponse = handleErrorResponse(error)
    }

    return dataResponse
}