
import { handleResponse, handleErrorResponse } from "../../../functions/api/apiHandleResponse"
import { SchoolAdviser } from "../../../configs/external"
import axios, { AxiosResponse } from "axios"

import Response from "../../../classes/response/Response"
import SearchAdviserP from "../../../classes/response/paylaod/SearchAdviserP"
import Adviser from "../../../classes/school/Adviser"

export default async (req, res) => {
    const search = req.body
    const key = req.headers.secret

    try {
        let response = await searchAdviser(search, key) 
        handleResponse(response, res)
    } catch (error) {
        res.status(400).json(new Response('ERROR', error, null))
    }
}

const searchAdviser = async (searchCriteria: SearchAdviserP, secret: string) => {
    const url = SchoolAdviser.SEARCH
    let token = secret.replace(/"/g,'')
    let dataResponse: Response<Adviser[]>
    let response: AxiosResponse

    try {
        response = await axios.post(
            url,
            searchCriteria,
            {
                headers: {
                    'Content-Type' : 'application/json',
                    'Authorization':`Bearer ${token}` 
                }
            }
        )

        dataResponse = response.data
    } catch (error) {
        dataResponse = handleErrorResponse(error)
    }

    return dataResponse
}