
import { SchoolRefAdvisory } from "../../../../configs/external"
import { handleResponse, handleErrorResponse } from "../../../../functions/api/apiHandleResponse"
import { RefAdvisoryStudent } from "../../../../classes/school/ref/refAdvisory"
import axios, { AxiosResponse } from 'axios'

import Response from "../../../../classes/response/Response"

export default async (req, res) => {
    const refBody = req.body
    const key = req.headers.secret

    try {
        let response = await addRef(refBody, key) 
        handleResponse(response, res)
    } catch (error) {
        res.status(400).body(new Response('ERROR', error, null))
    }
}

const addRef = async (student: RefAdvisoryStudent, secret: string): Promise<Response<String>> => {
    const url = SchoolRefAdvisory.ADD_REFERENCE
    let token = secret.replace(/"/g,'')
    let dataResponse: Response<String>
    let response: AxiosResponse
    
    try {
        response = await axios.post(
            url,
            student,
            {
                headers: {
                    'Content-Type' : 'application/json',
                    'Authorization':`Bearer ${token}` 
                }
            }
         )

        dataResponse = response.data
    } catch(error) {
        dataResponse = handleErrorResponse(error)
    }

    return dataResponse
}