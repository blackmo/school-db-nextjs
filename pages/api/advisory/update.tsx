import { SchoolAdvisory } from "../../../configs/external"
import { handleResponse, handleErrorResponse } from "../../../functions/api/apiHandleResponse"
import axios, { AxiosResponse } from 'axios'

import Response from "../../../classes/response/Response"
import Advisory from "../../../classes/school/Advisory"

export default async (req, res) => {
    const advisory = req.body
    const key = req.headers.secret

    try {
        let response = await update(advisory, key) 
        handleResponse(response, res)
    } catch (error) {
        res.status(400).body(new Response('ERROR', error, null))
    }
}

const update = async (advisory: Advisory, secret: string): Promise<Response<Advisory>> => {
    const url = SchoolAdvisory.UPDATE
    let token = secret.replace(/"/g,'')
    let dataResponse: Response<Advisory>
    let response: AxiosResponse
    
    try {
        response = await axios.post(
            url,
            advisory,
            {
                headers: {
                    'Content-Type' : 'application/json',
                    'Authorization':`Bearer ${token}` 
                }
            }
         )

        dataResponse = response.data
    } catch(error) {
        dataResponse = handleErrorResponse(error)
    }

    return dataResponse
}