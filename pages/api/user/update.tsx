
import { UserCalls } from "../../../configs/external"
import { handleResponse, handleErrorResponse } from "../../../functions/api/apiHandleResponse"
import axios, { AxiosResponse } from 'axios'

import Response from "../../../classes/response/Response"
import ChangePassword from "../../../classes/user/ChangePassword"

export default async (req, res) => {
    const passwordDetails = req.body
    const key = req.headers.secret

    try {
        let response = await updatePassword(passwordDetails, key) 
        handleResponse(response, res)
    } catch (error) {
        res.status(400).body(new Response('ERROR', error, null))
    }
}

const updatePassword = async (password: ChangePassword, secret: string): Promise<Response<string>> => {
    const url = UserCalls.UPDATE_PASSWORD
    let token = secret.replace(/"/g,'')
    let dataResponse: Response<string>
    let response: AxiosResponse
    
    try {
        response = await axios.post(
            url,
            password,
            {
                headers: {
                    'Content-Type' : 'application/json',
                    'Authorization':`Bearer ${token}` 
                }
            }
         )

        dataResponse = response.data
    } catch(error) {
        dataResponse = handleErrorResponse(error)
    }

    return dataResponse
}