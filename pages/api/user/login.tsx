
import { UserCalls } from "../../../configs/external"
import { handleResponse, handleErrorResponse } from "../../../functions/api/apiHandleResponse"
import axios, { AxiosResponse } from 'axios'

import Student from "../../../classes/school/Student"
import Response from "../../../classes/response/Response"
import ChangePassword from "../../../classes/user/ChangePassword"

export default async (req, res) => {
    const credentials = req.body
    const key = req.headers.secret

    try {
        let response = await login(credentials) 
        handleResponse(response, res)
    } catch (error) {
        res.status(400).body(new Response('ERROR', error, null))
    }
}

const login = async (credential): Promise<Response<string>> => {
    const url = UserCalls.LOGIN
    let dataResponse: Response<string>
    let response: AxiosResponse
    
    try {
        response = await axios.post(
            url,
            credential,
            {
                headers: {
                    'Content-Type' : 'application/json',
                }
            }
         )

        dataResponse = response.data
    } catch(error) {
        dataResponse = handleErrorResponse(error)
    }

    return dataResponse
}