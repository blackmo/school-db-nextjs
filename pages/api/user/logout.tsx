
import { UserCalls } from "../../../configs/external"
import { handleResponse, handleErrorResponse } from "../../../functions/api/apiHandleResponse"
import axios, { AxiosResponse } from 'axios'

import Response from "../../../classes/response/Response"

export default async (req, res) => {
    const key = req.headers.secret

    try {
        let response = await logout(key) 
        handleResponse(response, res)
    } catch (error) {
        res.status(400).body(new Response('ERROR', error, null))
    }
}

const logout = async (secret: string): Promise<Response<string>> => {
    const url = UserCalls.LOGOUT
    let token = secret.replace(/"/g,'')
    let dataResponse: Response<string>
    let response: AxiosResponse
    
    try {
        response = await axios({
            url: url,
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                'Authorization':`Bearer ${token}` 
            }
        })

        dataResponse = response.data
    } catch(error) {
        dataResponse = handleErrorResponse(error)
    }

    return dataResponse
}