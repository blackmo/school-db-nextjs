import { handleResponse, handleErrorResponse } from "../../../functions/api/apiHandleResponse"
import { SchoolStudent } from "../../../configs/external"
import axios, { AxiosResponse } from "axios"

import SearchStudentP from "../../../classes/response/paylaod/SearchStudentP"
import Response from "../../../classes/response/Response"
import Student from "../../../classes/school/Student"

export default async (req, res) => {
    const search = req.body
    const key = req.headers.secret

    try {
        let response = await searchStudent(search, key) 
        handleResponse(response, res)
    } catch (error) {
        res.status(400).json(new Response('ERROR', error, null))
    }
}

const searchStudent = async (searchCriteria: SearchStudentP, secret: string) => {
    const url = SchoolStudent.SEARCH
    let token = secret.replace(/"/g,'')
    let dataResponse: Response<Student[]>
    let response: AxiosResponse

    try {
        response = await axios.post(
            url,
            searchCriteria,
            {
                headers: {
                    'Content-Type' : 'application/json',
                    'Authorization':`Bearer ${token}` 
                }
            }
        )

        dataResponse = response.data
    } catch (error) {
        dataResponse = handleErrorResponse(error)
    }

    return dataResponse
}