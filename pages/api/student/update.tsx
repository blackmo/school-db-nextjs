
import { SchoolStudent } from "../../../configs/external"
import { handleResponse, handleErrorResponse } from "../../../functions/api/apiHandleResponse"
import axios, { AxiosResponse } from 'axios'

import Student from "../../../classes/school/Student"
import Response from "../../../classes/response/Response"

export default async (req, res) => {
    const student = req.body
    const key = req.headers.secret

    try {
        let response = await register(student, key) 
        handleResponse(response, res)
    } catch (error) {
        res.status(400).body(new Response('ERROR', error, null))
    }
}

const register = async (student: Student, secret: string): Promise<Response<Student>> => {
    const url = SchoolStudent.REGISTER
    let token = secret.replace(/"/g,'')
    let dataResponse: Response<Student>
    let response: AxiosResponse
    
    try {
        response = await axios.post(
            url,
            student,
            {
                headers: {
                    'Content-Type' : 'application/json',
                    'Authorization':`Bearer ${token}` 
                }
            }
         )

        dataResponse = response.data
    } catch(error) {
        dataResponse = handleErrorResponse(error)
    }

    return dataResponse
}