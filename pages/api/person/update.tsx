import { PersonCalls } from "../../../configs/external"
import { handleResponse, handleErrorResponse } from "../../../functions/api/apiHandleResponse"
import axios, { AxiosResponse } from "axios"

import Person from "../../../classes/person/Person"
import Response from "../../../classes/response/Response"

export default async (req, res) => {

    const person = req.body
    const key = req.headers.secret
    
    try {
        let response = await updatePerson(person, key) 
        handleResponse(response, res)
    } catch (error) {
        res.status(400).json(new Response('ERROR', error, null))
    }
}

const updatePerson = async (person: Person, secret: string) => {
    const url = PersonCalls.UPDATE
    const token = secret.replace(/"/g,'')

    let dataResponse: Response<Person>
    let response: AxiosResponse
    
    try {
        response = await axios.post(
            url,
            person,
            {
                headers: {
                    'Content-Type' : 'application/json',
                    'Authorization':`Bearer ${token}` 
                }
            }
        )

        dataResponse = response.data
    } catch (error) {
        dataResponse = handleErrorResponse(error)
    }

    return dataResponse

}