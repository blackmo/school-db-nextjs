import { useState } from 'react'
import { HeaderLayout, CommonLayout } from '../components/layout/Layout'
import { login } from '../utils/auths'
import { clearStorage } from '../utils/LocalStorageUtil'
import {userLogin} from '../functions/user/UserApiCall'

import LoginPane from '../components/pane/Login'

const Login = () => {

  const [visible, setVissible] = useState(false)
  const execLogin = async (credential) => {

    let  response  = await userLogin(credential)

    try {
      clearStorage()
      if (response.code == 'SUCCESS') {
        setVissible(false)
        let token = JSON.stringify(response.data)
        login(token);
      } else {
        setVissible(true)
      }
    } catch (err) {
      console.error('Network Issues', err)
    }

  }

  return (
    <div>
      <HeaderLayout hideLogout />
      <CommonLayout>
        <p>Enter Login Credential</p>
        <LoginPane loginUser={execLogin} visible={visible} />
      </CommonLayout>
    </div>
  )
}

export default Login
