import { HeaderLayout } from '../components/layout/Layout'
import '../styles/layout/layout.global.scss'


const Index = () => {

    return(
        <div className = 'root'>
            <HeaderLayout>
                <p>School Registration</p>
            </HeaderLayout>
        </div>
    )
}

export default Index
