
import { HeaderLayout, CommonLayout } from "../../../../components/layout/Layout"
import { makeStyles, Paper, Divider } from "@material-ui/core";
import { useEffect, useState } from "react";
import { drawerStyle } from "../../../../styles/make/component/drawer/drawerStyle";
import { clearSotrageOn, saveLocal, getLocalS } from "../../../../utils/LocalStorageUtil";
import { callUpdateAdviserId } from "../../../../functions/school/adviser/AdviserAPICalls";
import { StorageKeys } from "../../../../configs/constants/keys";

import CommonDrawer from "../../../../components/drawer/commonDrawer";
import SimpleInfo from "../../../../components/school/student/simpleInfo";
import ContactMailIcon from '@material-ui/icons/ContactMail';
import PersonUtils from "../../../../functions/person/PersonUtils";
import DrawerBuddy from "../../../../components/pane/DrawerBuddy";
import Adviser from "../../../../classes/school/Adviser";
import AdviserInfo from "../../../../components/school/adviser/adviserInfo";
import Person from "../../../../classes/person/Person";
import LoadingOnApiCall from "../../../../components/prompts/ApiCaller";

const useStyle = makeStyles(theme => drawerStyle(theme))

const contentList = [
  {
    label: 'Details',
    icon: () => { return <ContactMailIcon /> }
  },
]

const getComponent = (id, index, input, onUpdate) => {
  switch (index) {
    case 0: {
      const { person} = input
      return <AdviserInfo id={id} person={person} onUpdate={onUpdate} />
    }
  }
}

const AdviserDetails = props => {

  const [currentLink, setLink] = useState(0)
  const [open, setOpen] = useState(true)
  const [adviser, setAdviser] = useState<Adviser>(new Adviser())
  const [id, setsId] = useState(0)
  const [call, setCall] = useState(false)

  useEffect(() => {
    parse()
  }, [])

  const handleDrawerOpen = () => {
    setOpen(true);
  };

  async function parse() {
    let result: Adviser = await getLocalS(StorageKeys.LOCAL_ADVISER)
    setAdviser(result || new Adviser())
    clearSotrageOn(StorageKeys.LOCAL_ADVISER)
  }

  const handleDrawerClose = () => {
    setOpen(false);
  };

  const onUpdate = (code: string, person: Person) => {
    if (code.startsWith('update')) {
      
      let updatedAdviser = {...adviser, person: person}
      setAdviser(updatedAdviser)
      saveLocal('current-adviser', JSON.stringify(updatedAdviser))

      setsId(id + 1)
    }
  }

  const handleUpdateId = licenseNumber => {
    adviser.licenseNumber = licenseNumber
    setCall(true)
  }

  const handleOnSuccess = (adviser: Adviser) => {
    setAdviser(adviser)
    setCall(false)
  }

  const callApi = async () => {
    return await callUpdateAdviserId(adviser)
  }


  const classes = useStyle()
  return (
    <div>
      <HeaderLayout />
      <CommonLayout className={classes.container}>
        <div className={classes.root}>
          <CommonDrawer
            variant='persistent'
            anchor='left'
            open={open}
            classes={{
              paper: classes.drawerPaper
            }}
            contentList={contentList}
            onClick={setLink}
            handleDrawerClose={handleDrawerClose}
            withIcon
            shrinkable
          />
          <DrawerBuddy
            drawerClasses={classes}
            title='Adviser Details'
            onDrawerOpen={handleDrawerOpen}
            open={open}
          >
            <Paper elevation={0}>
              <SimpleInfo
                idName = 'License Number'
                idx= {adviser.licenseNumber}
                name={ PersonUtils.fullName(adviser.person) }
                onUpdate={ handleUpdateId }
              />
              <Divider className={classes.spacer} />
              <div>{getComponent(id, currentLink, adviser, onUpdate)}</div>
            </Paper>
          </DrawerBuddy>
        </div>
      </CommonLayout>
      <LoadingOnApiCall
        title='Update Adviser'
        message='Saving Details'
        actionTitle='OK'
        type='save'
        autoClose
        call={{
          call:call,
          apiCall: callApi ,
          onSuccess: handleOnSuccess,
        }}
      />
    </div>
  )
}

export default AdviserDetails