import { HeaderLayout, CommonLayout } from "../../../../components/layout/Layout"
import { makeStyles } from "@material-ui/core"
import { drawerStyle } from "../../../../styles/make/component/drawer/drawerStyle"
import { useState, useRef } from "react";
import { useRouter } from 'next/router'
import { whenAuthorized, withAuhtSync } from "../../../../utils/auths";
import { saveLocal } from "../../../../utils/LocalStorageUtil";
import { callSearchAdviser } from "../../../../functions/school/adviser/AdviserAPICalls";
import { StorageKeys } from "../../../../configs/constants/keys";

import EmptyDrawer from "../../../../components/drawer/ emptyDrawer"
import SearchTable from "../../../../components/tables/SearchTable";
import SimpleDialog from "../../../../components/prompts/SimpleDialog";
import PersonUtils from "../../../../functions/person/PersonUtils";
import AdviserSelectionCtx from "../../../../components/school/adviser/AdviserSelectionCtx";
import SearchAdviserP from "../../../../classes/response/paylaod/SearchAdviserP";
import Adviser from "../../../../classes/school/Adviser";
import AdviserDetials from "../../../../classes/school/adviser/AdviserDetails";
import DrawerBuddy from "../../../../components/pane/DrawerBuddy";
import LoadingOnApiCall from "../../../../components/prompts/ApiCaller";

const useStyle = makeStyles(theme => drawerStyle(theme, { width: 350 }))

const tableHeaders = [
  { title: 'Last Name', field: 'lastName' },
  { title: 'First Name', field: 'firstName' },
  { title: 'Middle Name', field: 'middleName' }
]

const AdviserSearch = props => {

  const [openConfirm, setOpenConfirm] = useState(false)
  const [advisers, setAdvisers] = useState<Adviser[]>([])
  const adviserDetails = useRef<Adviser>(new Adviser())
  const [open, setOpen] = useState(true)
  const [call, setCall] = useState(false)
  const Router = useRouter()
  const crieria = useRef<SearchAdviserP>(null)

  const handleDrawerClose = () => {
    setOpen(false)
  }

  const handleDrawerOpen = () => {
    setOpen(true)
  }


  const handleConfirm = () => {
    saveLocal(StorageKeys.LOCAL_ADVISER, JSON.stringify(adviserDetails.current))
    Router.push('/school/adviser/details')
  }

  const handleCancel = () => {
    setOpenConfirm(false)
  }

  const handleSelect = (from: Adviser) => {
    setOpenConfirm(true)
    adviserDetails.current = advisers.find(entry => entry.id == from.id)
  }

  const onSearch = async (searchBody: SearchAdviserP) => {
    crieria.current = searchBody
    setCall(true)
  }

  const callApi = async () => {
    return await callSearchAdviser(crieria.current)
  }

  const handleOnSuccess = (listAdvisers: Adviser[]) => {
    setAdvisers(listAdvisers)
    setCall(false)
  }


  const classes = useStyle()
  return (
    <div>
      <HeaderLayout />
      <CommonLayout className={classes.container}>
        <div className={classes.root}>
          <EmptyDrawer
            variant='persistent'
            anchor='left'
            open={open}
            classes={{
              paper: classes.drawerPaper
            }}
            handleDrawerClose={handleDrawerClose}
            shrinkable
          >
            <AdviserSelectionCtx
              onSearch={onSearch}
            />
          </EmptyDrawer>
          <DrawerBuddy
            drawerClasses={classes}
            title='Find Adviser'
            onDrawerOpen={handleDrawerOpen}
            open={open}
          >
            <SearchTable
              title='Adviser List'
              columns={tableHeaders}
              data={mapValues(advisers)}
              onSelect={handleSelect}
            />
          </DrawerBuddy>
        </div>
        <LoadingOnApiCall
          title='Search Adviser'
          message='Searching Results'
          actionTitle='OK'
          type='search'
          autoClose
          call={{
            call: call,
            apiCall: callApi,
            onSuccess: handleOnSuccess,
            succesMessage: 'Retreived Data Successfully'
          }}
        />
        <SimpleDialog
          title='Adviser Details'
          open={openConfirm}
          content={'View Details of ' + PersonUtils.fullName(adviserDetails.current.person)}
          positiveAction={handleConfirm}
          negativeAction={handleCancel}
        />
      </CommonLayout>
    </div>
  )
}

const mapValues = (adviser: Adviser[]): AdviserDetials[] => {
  return adviser.map((adviser) => {
    const person = adviser.person
    return new AdviserDetials(adviser.id, person.lastName, person.firstName, person.middleName)
  })

}

AdviserSearch.getInitialProps = async ctx => {
  whenAuthorized(ctx);
}

export default withAuhtSync(AdviserSearch)