import { whenAuthorized, withAuhtSync } from "../../../../utils/auths";
import { HeaderLayout, CommonLayout } from "../../../../components/layout/Layout";
import { callRegisterAdviser } from "../../../../functions/school/adviser/AdviserAPICalls";
import { useState, useRef } from "react";
import { saveLocal } from "../../../../utils/LocalStorageUtil";
import { useRouter } from 'next/router'

import AviserRegister from "../../../../components/school/adviser/AdviserRegister";
import SimpleSnackBar from "../../../../components/prompts/SimpleSnackBar";
import Adviser from "../../../../classes/school/Adviser";
import PrompStatus from "../../../../classes/prompt/PromptStatus";
import PersonUtils from "../../../../functions/person/PersonUtils";
import Person from "../../../../classes/person/Person";
import LoadingOnApiCall from "../../../../components/prompts/ApiCaller";

const RegisterAdviser = props => {
  const [promptStatus, setStatus] = useState<PrompStatus>(new PrompStatus(false, '', 0))
  const [counter, setCounter] = useState(0)
  const [call, setCall] = useState(false)
  const Router = useRouter()

  const adviserDetails = useRef<Adviser>(new Adviser())
  const person = useRef<Person>()

  const handleOnNextPrompt = (event, reason?) => {
    if (reason == 'clickeaway') {
      return
    }

    promptStatus.open = false
    setStatus({ ...promptStatus })
  }

  const handleSubmit = (submit: { hasError: boolean, prompt: PrompStatus, adviser: Adviser }) => {
    if (submit.hasError) {
      setStatus(submit.prompt)
    } else {
      adviserDetails.current = submit.adviser
      person.current = submit.adviser.person
      setCall(true)
    }
  }

  const callApi = async () => {
    return await callRegisterAdviser(adviserDetails.current)
  }

  const handleOnSuccess = (adviser: Adviser) => {
    saveLocal('current-adviser', JSON.stringify(adviser))
    adviserDetails.current = adviser
  }

  const handleOnAction = () => {
    setCounter(counter + 1)
    Router.push('/school/adviser/details')
  }

  const handleCancel = () => {
    Router.push('/landingpage')
  }

  return (
    <div>
      <HeaderLayout />
      <CommonLayout>
        <AviserRegister onSubmit={handleSubmit} onCancel={ handleCancel } resetCounter={counter} />
      </CommonLayout>
      <SimpleSnackBar
        open={promptStatus.open}
        onClose={handleOnNextPrompt}
        message={promptStatus.message}
        severity={promptStatus.severity}
        variant='filled'
      />
      <LoadingOnApiCall
        title='Save Adviser'
        message={'Confirm Adviser Registration: ' + PersonUtils.fullName(person.current)}
        actionTitle='OK'
        type='save'
        call={{
          call: call,
          apiCall: callApi,
          onSuccess: handleOnSuccess,
          onAction: handleOnAction,
          successMessage: 'View Adviser Details?'
        }}
      />
    </div>
  )
}

RegisterAdviser.getInitialProps = async ctx => {
  whenAuthorized(ctx);
}

export default withAuhtSync(RegisterAdviser)