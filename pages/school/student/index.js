import { CommonLayout, HeaderLayout } from "../../../components/layout/Layout";
import { whenAuthorized, withAuhtSync } from "../../../utils/auths";
import { Button } from "@material-ui/core";

import Link from "next/link";

const Index = props => {
  return (
    <div>
      <HeaderLayout />
      <CommonLayout>
        <Link href="/student/register">
          <a>REGISTER</a>
        </Link>
        <Link href="/student/search">
          <a>SEARCH</a>
        </Link>
      </CommonLayout>
      <Button>Hello</Button>
    </div>
  );
};

Index.getInitialProps = async ctx => {
  whenAuthorized(ctx);
};

export default withAuhtSync(Index);