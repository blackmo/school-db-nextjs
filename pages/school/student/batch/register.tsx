import { HeaderLayout, CommonLayout } from "../../../../components/layout/Layout"
import { useRef, useState } from "react"
import {BathRegistrationResultPane} from  '../../../../components/school/student/batchResult'
import { uploadCsvRegFile } from "../../../../functions/upload/UploadApiCalls"
import { whenAuthorized, withAuhtSync } from "../../../../utils/auths"

import BatchConfig from "../../../../components/school/student/batchconfig"
import BatchREgisterR from "../../../../classes/response/BatchRegisterR"
import LoadingOnApiCall from "../../../../components/prompts/ApiCaller"

const BatchRegister = (props) => {

  const csvFile = useRef<File>()
  const headerProps = useRef(null)
  const buttonTile = useRef('Cancel')
  const [batchResponse, setBatchResponse] = useState<BatchREgisterR>( new BatchREgisterR())
  const [call, setCall] = useState(false)


  const handleSubmit = async (data) => {
    headerProps.current = data
    setCall(true)
  }
  
  const handleOnSuccess = (batchResponse: BatchREgisterR) => {
    buttonTile.current = 'Close'
    setBatchResponse(batchResponse)
    setCall(false)
  }

  const handleFile = (file: File) => {
    csvFile.current = file
  }

  const callApi = async () => {
    return await uploadCsvRegFile(csvFile.current, headerProps.current) 
  }
  
  const handleOnClose = () => setCall(false)

  return (
    <div>
      <HeaderLayout />
      <CommonLayout>
        <BatchConfig
          onSubmit={ handleSubmit }
          onChange={ handleFile }
        />
        <BathRegistrationResultPane failed= {batchResponse.failed}/>
      </CommonLayout>
      <LoadingOnApiCall
        title='Batch Registration'
        message='Confirm Upload Batch Upload'
        actionTitle={ buttonTile.current }
        type='upload'
        call={{
          call:call,
          apiCall: callApi ,
          onSuccess: handleOnSuccess,
          onClose: handleOnClose
        }}
      />
    </div>
  )
}

BatchRegister.getInitialProps = async ctx => {
  whenAuthorized(ctx)
}

export default withAuhtSync(BatchRegister)