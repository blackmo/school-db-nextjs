import { useState, useRef } from 'react'
import { withAuhtSync, whenAuthorized } from '../../../utils/auths'
import { HeaderLayout, CommonLayout } from '../../../components/layout/Layout'
import { callRegister } from '../../../functions/school/student/StudenApiCalls'
import { detApiState } from '../../../functions/api/ApiUtils'
import { useRouter} from 'next/router'
import { saveLocal } from '../../../utils/LocalStorageUtil'
import { StorageKeys } from '../../../configs/constants/keys'

import Student from '../../../classes/school/Student'
import PrompStatus from '../../../classes/prompt/PromptStatus'
import LoadingDialog from '../../../components/prompts/loadingDialog'
import ApiResponseSate from '../../../classes/response/ApiResponseState'
import PersonUtils from '../../../functions/person/PersonUtils'
import SimpleSnackBar from '../../../components/prompts/SimpleSnackBar'
import RegisterForm from '../../../components/school/student/RegisterForm'


const RegisterStudent = (props) => {
  const [promptStatus, setStatus] = useState<PrompStatus>(new PrompStatus(false, '', 0))
  const [dialogOpen, setDialogOpen] = useState(false)
  const [apiState, setApiState] = useState<ApiResponseSate>(new ApiResponseSate())
  const [counter, setCounter] = useState(0)
  const studentDetails = useRef<Student>(new Student())
  const Router = useRouter()


  const handleOnNextPrompt = (event, reason?) => {
    if (reason == 'clickeaway') {
      return
    }

    promptStatus.open = false
    setStatus({...promptStatus})
  }

  const callApi = async () => {
    setApiState({...apiState, called: true})
    let response = await callRegister(studentDetails.current)
    let api =  detApiState(response, 'View Student Details?')
    saveLocal(StorageKeys.LOCAL_STUDENTS, JSON.stringify(response.data))
    studentDetails.current = response.data
    setApiState(api)

  }

  const handleActionDisplay = () => {
    if (apiState.success && !apiState.called) {
      setCounter(counter+1)
      let lrnid = studentDetails.current.lrnID
      Router.push(`/school/student/details/${lrnid}`)  
    }
    else callApi()
  }

  const handleCancelDialogAction = () => {
    if (apiState.success && !apiState.called) {
      setCounter(counter+1)
    }

    setDialogOpen(false) 
    setTimeout(() => {
      setApiState(new ApiResponseSate())
    }, 1000)
  }

  const handleSubmit = (submit: {hasError: boolean, prompt: PrompStatus, student: Student}) => {
    if (submit.hasError) {
      setStatus(submit.prompt)
    } else {
      studentDetails.current = submit.student
      console.log(PersonUtils.fullName(submit.student.person))
      setDialogOpen(true)
    }
  }

  const handleCancel = () => { Router.push('/landingpage') }

  return (
    <div>
      <HeaderLayout />
      <CommonLayout>
        <RegisterForm onSubmit={handleSubmit} onCancel={ handleCancel } resetCounter={counter}/>
      </CommonLayout>
      <SimpleSnackBar
        open={promptStatus.open}
        onClose={handleOnNextPrompt}
        message={promptStatus.message}
        severity= {promptStatus.severity}
        variant='filled'
      />
      <LoadingDialog
        title={'Register Student'}
        open={dialogOpen}
        message={ apiState.message || 'Confirm Student Registration: '+PersonUtils.fullName(studentDetails.current.person)}
        onClose={handleCancelDialogAction}
        onAction={handleActionDisplay}
        actionTitle={'Cancel'}
        loading={apiState.called}
        success = {apiState.success}
        error = {apiState.error}
      />
    </div>
  )
}

RegisterStudent.getInitialProps = async ctx => {
  whenAuthorized(ctx);
}

export default withAuhtSync(RegisterStudent)