import { HeaderLayout, CommonLayout } from "../../../../components/layout/Layout"
import { makeStyles } from "@material-ui/core"
import { drawerStyle } from "../../../../styles/make/component/drawer/drawerStyle"
import { useState, useRef, useReducer } from "react";
import { callSearchStudent, callSearchStudentBy } from "../../../../functions/school/student/StudenApiCalls";
import { useRouter } from 'next/router'
import { whenAuthorized, withAuhtSync } from "../../../../utils/auths";
import { saveLocal } from "../../../../utils/LocalStorageUtil";
import { StorageKeys } from "../../../../configs/constants/keys";
import { mapToStudentDetails } from "../../../../functions/school/student/StudentUtils";
import { callRefAdvisorySearchBy } from "../../../../functions/school/ref/AdvisoryStudent";

import SelectionCriteria from "../../../../components/actions/SeletionCriteria";
import EmptyDrawer from "../../../../components/drawer/ emptyDrawer"
import SearchStudentP from "../../../../classes/response/paylaod/SearchStudentP";
import SearchTable from "../../../../components/tables/SearchTable";
import StudentDetails from "../../../../classes/school/student/StudentDetails";
import SimpleDialog from "../../../../components/prompts/SimpleDialog";
import PersonUtils from "../../../../functions/person/PersonUtils";
import DrawerBuddy from "../../../../components/pane/DrawerBuddy";
import LoadingOnApiCall from "../../../../components/prompts/ApiCaller";
import Student from "../../../../classes/school/Student";

const useStyle = makeStyles(theme => drawerStyle(theme, { width: 350 }))

const tableHeaders = [
  { title: 'LRN ID', field: 'lrnID' },
  { title: 'Satus', field: 'status' },
  { title: 'Last Name', field: 'lastName' },
  { title: 'First Name', field: 'firstName' },
  { title: 'Middle Name', field: 'middleName' }
]

const StudentSearch = props => {

  const [openConfirm, setOpenConfirm] = useState(false)
  const [students, setStudent] = useState<Student[]>([])
  const [open, setOpen] = useState(true)
  const [call, setCall] = useState(false)
  const Router = useRouter()
  const studenForDetails = useRef<Student>(new Student())

  const searchByLrnid = {
    apiCall: async function (data: SearchStudentP) {
      return callSearchStudent(data)
    },
    onSuccess: (data: Student[]) => {
      setStudent(data)
      setCall(false)
    },
    onError: (data) => {
      setCall(false)
    },
    succesMessage: 'Successfully Retreived Students'
  }

  const searchByLrnIdOrLastName = {
    async apiCall(data: SearchStudentP) {
      return await callSearchStudentBy(data)
    },
    onSuccess(data: Student[]) {
      setStudent(data)
      setCall(false)
    },
    onError(data) {
      setCall(false)
    },
    successMessage: 'Successfully Retrieved Student List'
  }

  const searchByOthers = {
    apiCall: async function (data: SearchStudentP) {
      return callRefAdvisorySearchBy(data)
    },
    onSuccess: (data: Student[]) => {
      setStudent(data)
      setCall(false)
    },
    onError: (data) => {
      setCall(false)
    },
    succesMessage: 'Successfully Retreived Students'
  }

  const defaultState = {
    title: 'Search Student',
    message: 'Getting Student',
    actionTitle: 'OK',
    type: 'search',
    call: searchByLrnid,
    payload: undefined
  }

  const reducer = (state: { call, title, message, actionTitle, type, payload }, calltype: {type: string, payload}) => {
    switch (calltype.type) {
      case 'search-by-lrnid': {
        state.call= searchByLrnid,
        state.payload = calltype.payload
        return state
      }

      case 'search-student-by': {
        state.call= searchByLrnIdOrLastName,
        state.payload = calltype.payload
        return state
      }

      default: {
        state.call= searchByOthers,
        state.payload = calltype.payload
        return state
      }
    }
  }

  const [apiState, dispatch] = useReducer(reducer, defaultState)

  const handleDrawerClose = () => {
    setOpen(false)
  }

  const handleDrawerOpen = () => {
    setOpen(true)
  }

  const handleConfirm = () => {
    saveLocal(StorageKeys.LOCAL_STUDENTS, JSON.stringify(studenForDetails.current))

    Router.push(`/school/student/details/${studenForDetails.current.lrnID}`)
  }

  const handleCancel = () => {
    setOpenConfirm(false)
  }

  const handleSelect = (from: StudentDetails) => {
    setOpenConfirm(true)
    studenForDetails.current = students.find(entry => entry.lrnID == from.lrnID)
  }

  const onSearch = async (searchBody: SearchStudentP) => {
    if (searchBody.byLrnId) {
      dispatch({type:'search-by-lrnid', payload: searchBody})
    } else if (searchBody.status.toUpperCase() == 'NEW') {
      dispatch({type:'search-student-by', payload: searchBody})
    } else {
      dispatch({type:'search-by-else', payload: searchBody})
    }
    setCall(true)
  }

  const classes = useStyle()
  return (
    <div>
      <HeaderLayout />
      <CommonLayout className={classes.container}>
        <div className={classes.root}>
          <EmptyDrawer
            variant='persistent'
            anchor='left'
            open={open}
            classes={{
              paper: classes.drawerPaper
            }}
            handleDrawerClose={handleDrawerClose}
            shrinkable
          >
            <SelectionCriteria
              onSearch={onSearch}
            />
          </EmptyDrawer>

          <DrawerBuddy
            drawerClasses={classes}
            title='Find Student'
            onDrawerOpen={handleDrawerOpen}
            open={open}
          >
            <SearchTable
              title='Student List'
              columns={tableHeaders}
              data={mapToStudentDetails(students)}
              onSelect={handleSelect}
            />
          </DrawerBuddy>
        </div>
        <SimpleDialog
          title='Student Details'
          open={openConfirm}
          content={'View Details of ' + PersonUtils.fullName(studenForDetails.current.person)}
          positiveAction={handleConfirm}
          negativeAction={handleCancel}
        />
        <LoadingOnApiCall
          title={apiState.title}
          message={apiState.message}
          actionTitle={apiState.actionTitle}
          type={apiState.type}
          autoClose
          call={{
            call: call,
            apiCall: apiState.call.apiCall,
            onSuccess: apiState.call.onSuccess,
            succesMessage: apiState.call.succesMessage,
            payload: apiState.payload,
            onError: apiState.call.onError,
          }}
        />
      </CommonLayout>
    </div>
  )
}

StudentSearch.getInitialProps = async ctx => {
  whenAuthorized(ctx);
}


export default withAuhtSync(StudentSearch)