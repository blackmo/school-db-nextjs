import { HeaderLayout, CommonLayout } from "../../../../components/layout/Layout"
import { makeStyles, Paper, Divider, Typography, IconButton, TextField } from "@material-ui/core";
import { useEffect, useState, useRef } from "react";
import { drawerStyle } from "../../../../styles/make/component/drawer/drawerStyle";
import { clearSotrageOn, saveLocal, getLocalS} from "../../../../utils/LocalStorageUtil";
import { updateStudentDetails } from "../../../../functions/school/student/StudentUtils";
import { callUpdateStudent } from "../../../../functions/school/student/StudenApiCalls";
import { StorageKeys } from "../../../../configs/constants/keys";

import CommonDrawer from "../../../../components/drawer/commonDrawer";
import SimpleInfo from "../../../../components/school/student/simpleInfo";
import ContactMailIcon from '@material-ui/icons/ContactMail';
import Student from "../../../../classes/school/Student";
import StudentInfo from "../../../../components/school/student/studentInfo";
import PersonUtils from "../../../../functions/person/PersonUtils";
import DrawerBuddy from "../../../../components/pane/DrawerBuddy";
import LoadingOnApiCall from "../../../../components/prompts/ApiCaller";

const useStyle = makeStyles(theme => drawerStyle(theme))

const contentList = [
  {
    label: 'Details',
    icon: () => { return <ContactMailIcon /> }
  },
]

const getComponent = (id, index, input, onUpdate) => {
  switch (index) {
    case 0: {
      const {person, father, mother} = input
      return <StudentInfo id={ id } student={person} father={father} mother={mother} onUpdate={ onUpdate } />
    }
  }
}

const StudentDetails = props => {

  const [currentLink, setLink] = useState(0)
  const [open, setOpen] = useState(true)
  const [student, setStudent] = useState<Student>(new Student())
  const [id, setsId] = useState(0)
  const [call, setCall] = useState(false)
  const tempStudent = useRef<Student>(null)

  useEffect(() => {
    parse()
  }, [])

  const handleDrawerOpen = () => {
    setOpen(true);
  };

  async function parse() {
    let result: Student = await getLocalS(StorageKeys.LOCAL_STUDENTS)
    setStudent(result || new Student())
    clearSotrageOn(StorageKeys.LOCAL_STUDENTS)
  }

  const handleDrawerClose = () => {
    setOpen(false);
  };

  const onUpdate = (code: string, value) => {
    if (code.startsWith('update')) {
      let updatedStudent = updateStudentDetails(code, student, value)
      saveLocal('current-student', JSON.stringify(value))
      setStudent(updatedStudent)
      setsId(id+1)
    }
  }

  const handleUpdateLrnId = lrnId => {
    tempStudent.current = {...student, lrnID: lrnId}
  }

  const handleOnSuccess = (adviser: Student) => {
    setStudent(adviser)
    setCall(false)
  }

  const callApi = async () => {
    return await callUpdateStudent(tempStudent.current)
  }

  const classes = useStyle()
  return (
    <div>
      <HeaderLayout />
      <CommonLayout className={classes.container}>
          <div className={classes.root}>
            <CommonDrawer
              variant='persistent'
              anchor='left'
              open={open }
            classes={{
              paper: classes.drawerPaper
            }}
            contentList={contentList}
            onClick={setLink}
            handleDrawerClose = { handleDrawerClose }
            withIcon
            shrinkable
          />
          <DrawerBuddy
            drawerClasses={ classes }
            title='Student Details'
            onDrawerOpen={ handleDrawerOpen }
            open = {open}
          >
              <Paper elevation={0}>
                <SimpleInfo
                  id={student.lrnID}
                  name={ PersonUtils.fullName(student.person)}
                  idName='LRNID'
                  onUpdate={ handleUpdateLrnId}
                />
                <Divider className={classes.spacer} />
                <div>{getComponent( id, currentLink, student, onUpdate)}</div>
              </Paper>
          </DrawerBuddy>
        </div>
     </CommonLayout>
      <LoadingOnApiCall
        title='Update Sudent'
        message='Saving Details'
        actionTitle='OK'
        type='save'
        autoClose
        call={{
          call:call,
          apiCall: callApi ,
          onSuccess: handleOnSuccess,
        }}
      />
    </div>
  )
}

export default StudentDetails