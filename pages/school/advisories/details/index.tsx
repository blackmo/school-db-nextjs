
import { HeaderLayout, CommonLayout } from "../../../../components/layout/Layout"
import { makeStyles, Paper, Divider } from "@material-ui/core";
import { useEffect, useState, useRef, useReducer } from "react";
import { drawerStyle } from "../../../../styles/make/component/drawer/drawerStyle";
import { getLocalS } from "../../../../utils/LocalStorageUtil";
import { StorageKeys } from "../../../../configs/constants/keys";
import { CallStatus, ApiStateReducer } from "../../../../classes/api/ApiStateReducer";
import { findOneById } from "../../../../functions/school/advisory/AdvisoryApiCalls";
import { SearchAdvisoryR } from "../../../../classes/response/paylaod/SearchAdvisoryR";
import { GroupAdd, ContactMail } from "@material-ui/icons";

import CommonDrawer from "../../../../components/drawer/commonDrawer";
import SimpleInfoAdvisory from '../../../../components/school/advisories/SimpleInfoAdvisory'
import DrawerBuddy from "../../../../components/pane/DrawerBuddy";
import LoadingOnApiCall from "../../../../components/prompts/ApiCaller";
import Advisory from "../../../../classes/school/Advisory";
import AdvisoryStudents from "../../../../components/school/advisories/AdivsoryStudents";

const useStyle = makeStyles(theme => drawerStyle(theme))

const contentList = [
  {
    label: 'Details',
    icon: () => <ContactMail />
  },
  {
    label: 'Add Student',
    icon: () => <GroupAdd />
  }
]

const getComponent = (id, index, input, onUpdate?) => {
  switch (index) {
    case 0: {
      const { classes, advisory } = input
      return (
        <div>
          <SimpleInfoAdvisory
            advisory={advisory}
          />
          <Divider className={classes.spacer} />
        </div>
      )
    }

    case 1: {
      const { classes, advisory } = input
      return <AdvisoryStudents advisory={advisory} onUpdate={ onUpdate} />
    }
  }
}

const AdvisoryDetails = props => {

  const [currentLink, setLink] = useState(0)
  const [open, setOpen] = useState(true)
  const [advisory, setAdvisory] = useState<Advisory>(new Advisory())
  const [call, setCall] = useState(false)
  const id = useRef<number>(0)


  const getAdvisoryDetails: CallStatus = {
    async apiCall(data: Advisory) {
      return await findOneById(id.current)
    },
    onSuccess(data: Advisory) {
      setAdvisory(data)
      setCall(false)
    },
    onError(data) {

    },
    successMessage: 'Retreived Advisory Details'
  }

  const defaultState = new ApiStateReducer()
  defaultState.call = getAdvisoryDetails
  defaultState.actionTitle = 'OK'
  defaultState.message = 'Getting Advisory Details'
  defaultState.title = 'Getting Resources'
  defaultState.type = 'search'

  const reducer = (state: ApiStateReducer, callType: string) => {
    switch (callType) {
      case 'init-resources': {
        return defaultState
      }
    }
  }

  const [apiState, dispatch] = useReducer(reducer, defaultState)

  useEffect(() => {
    parse()
  }, [])

  const handleDrawerOpen = () => {
    setOpen(true);
  };

  async function parse() {
    let result: SearchAdvisoryR = await getLocalS(StorageKeys.LOCAL_TEMP_ADVISORY)
    id.current = result.advisoryId
    dispatch('init-resources')
    setCall(true)
  }

  const handleDrawerClose = () => {
    setOpen(false);
  };

  const handleUpdate = (advisory: Advisory) => {
    setAdvisory(advisory)
  }

  const classes = useStyle()
  return (
    <div>
      <HeaderLayout />
      <CommonLayout className={classes.container}>
        <div className={classes.root}>
          <CommonDrawer
            variant='persistent'
            anchor='left'
            open={open}
            classes={{
              paper: classes.drawerPaper
            }}
            contentList={contentList}
            onClick={setLink}
            handleDrawerClose={handleDrawerClose}
            withIcon
            shrinkable
          />
          <DrawerBuddy
            drawerClasses={classes}
            title='Advisory Details'
            onDrawerOpen={handleDrawerOpen}
            open={open}
          >
            <Paper elevation={0}>
              {getComponent(0, currentLink, {classes, advisory}, handleUpdate)}
            </Paper>
          </DrawerBuddy>
        </div>
      </CommonLayout>
      <LoadingOnApiCall
        title={apiState.title}
        message={apiState.message}
        actionTitle={apiState.actionTitle}
        type={apiState.type.toString()}
        autoClose
        call={{
          call: call,
          apiCall: apiState.call.apiCall,
          onSuccess: apiState.call.onSuccess,
          onError: apiState.call.onError,
          successMessage: apiState.call.successMessage

        }}
      />
    </div>
  )
}

export default AdvisoryDetails