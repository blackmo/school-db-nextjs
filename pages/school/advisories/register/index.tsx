import { HeaderLayout, CommonLayout } from "../../../../components/layout/Layout"
import { useState, useRef } from "react"
import { callRegisterAdvisory } from "../../../../functions/school/advisory/AdvisoryApiCalls"
import { useRouter } from 'next/router'

import AdvisoryCreateForm from "../../../../components/school/advisories/advisoryCreateForm"
import LoadingOnApiCall from "../../../../components/prompts/ApiCaller"
import Advisory from "../../../../classes/school/Advisory"

const CreateAdvisory = props => {

  const [call, setCall] = useState(false)
  const tempAdvisory = useRef<Advisory>()
  const Router = useRouter()
  const resetCounter = useRef(0)
  
  const handleSubmit = (advisory: Advisory) => {
    tempAdvisory.current = advisory
    setCall(true)
  }

  const handleCancel = () => { Router.push('/landingpage') }

  const handleOnSuccess = (advisory: Advisory) => {
    resetCounter.current += 1
    setCall(false)
  }

  const apiCall = async () => {
    return await callRegisterAdvisory(tempAdvisory.current)
  }

  const handleError = error => { setCall(false)}

  return (
    <div>
      <HeaderLayout/>
      <CommonLayout>
        <AdvisoryCreateForm
          onSubmit={ handleSubmit }
          onCancel={ handleCancel }
          resetCounter={ resetCounter.current }
        />
      </CommonLayout>
      <LoadingOnApiCall
        title='Create and Save Advisory'
        message='Saving details'
        actionTitle='ok'
        type='save'
        autoClose
        call={{
          call: call,
          apiCall: apiCall,
          onSuccess: handleOnSuccess,
          succesMessage: 'Advisory Created',
          onError: handleError
        }}
      />
    </div>
  )
}

export default CreateAdvisory