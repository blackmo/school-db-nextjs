import { makeStyles } from "@material-ui/core"
import { drawerStyle } from "../../../../styles/make/component/drawer/drawerStyle"
import { HeaderLayout, CommonLayout } from "../../../../components/layout/Layout"
import { useState, useRef, useReducer, useEffect } from "react"
import { useRouter } from 'next/router'
import { saveLocal, getLocal } from "../../../../utils/LocalStorageUtil"
import { callGetAllAdviser } from "../../../../functions/school/adviser/AdviserAPICalls"
import { maptReusltAdvisoryEntry } from "../../../../functions/school/advisory/AdvisoryUtil"
import { categorizedAdviserSx } from "../../../../functions/school/adviser/AdviserUtils"
import { StorageKeys } from "../../../../configs/constants/keys"
import { CategorizedSelection } from "../../../../classes/school/adviser/AdviserSelection"
import { callSearchAdvisory } from "../../../../functions/school/advisory/AdvisoryApiCalls"
import { SearchAdvisoryR } from "../../../../classes/response/paylaod/SearchAdvisoryR"
import { withAuhtSync, whenAuthorized } from "../../../../utils/auths"

import EmptyDrawer from "../../../../components/drawer/ emptyDrawer"
import SearchTable from "../../../../components/tables/SearchTable"
import LoadingOnApiCall from "../../../../components/prompts/ApiCaller"
import DrawerBuddy from "../../../../components/pane/DrawerBuddy"
import SimpleDialog from "../../../../components/prompts/SimpleDialog"
import AdvisorySelectionCtx from "../../../../components/school/advisories/AdvisorySelectionCtx"
import Adviser from "../../../../classes/school/Adviser"
import SearchAdviosryP from "../../../../classes/response/paylaod/SeachAdvisoryP"
import AdvisoryTableEntry from "../../../../classes/school/advisory/AdvisoryTableEntry"

const tableHeaders = [
  { title: 'Class Year', field: 'classYear' },
  { title: 'Level', field: 'classLevel' },
  { title: 'Adviosry Name', field: 'advisoryName' },
  { title: 'Section', field: 'sectionNumber' },
  { title: 'Adviser', field: 'adviserName' },
]

const useStyle = makeStyles(theme => drawerStyle(theme, { width: 350 }))

const SearchAdvisories = props => {

  const [openConfirm, setOpenConfirm] = useState(false)
  const [selections, setSelection] = useState<CategorizedSelection[]>([])
  const [advisories, setAdvisories] = useState<SearchAdvisoryR[]>([])
  const [open, setOpen] = useState(true)
  const [call, setCall] = useState(false)

  const advisoryDetails = useRef<SearchAdvisoryR>(new SearchAdvisoryR())
  const Router = useRouter()
  const classes = useStyle()
  const payloadSearch = useRef<SearchAdviosryP>()

  /**
   * api call setup getting all advisers
   */
  const callSearchAdviser = {
    apiCall: async function(data) {
      return callGetAllAdviser()
    },
    onSuccess: (advisers: Adviser[]) => {
      saveLocal(StorageKeys.LOCAL_LIST_ADVISER, advisers)
      let selections = categorizedAdviserSx(advisers)
      setSelection(selections)
      setCall(false)
    },
    onError: (data) => {
      setCall(false)
    },
    succesMessage: 'Retreived Data Successfully'
  }

  /**
   * api call setup search advisories
   */
  const callApiSearchAdviser = {
    apiCall: async function(data: SearchAdviosryP) {
      return callSearchAdvisory(data)
    },
    onSuccess: (advisories: SearchAdvisoryR[]) => {
      setAdvisories(advisories)
      setCall(false)
    },
    onError: (data) => {
      setCall(false)
    },
    succesMessage: 'Retreived Advisories Successfully'
  }
  // --end
  //----------------------------------------------------------------- 

  const state = {
    title: 'Getting Resources',
    message: 'Getting Advisers',
    actionTitle:'OK',
    type:'search',
    call: callSearchAdviser,
    payload: undefined
  }

  const reducer = (state: { call, title, message, actionTitle, type, payload }, calltype: string) => {
    switch (calltype) {
      case 'init-resource': {
        state.message = 'Getting Advisers'
        state.actionTitle = 'OK'
        state.type = 'search'
        state.call = callSearchAdviser
        return state
      }
      case 'search-advisories' : {
        state.message = 'Searching Advisoreis'
        state.actionTitle = 'OK'
        state.type = 'search'
        state.call = callApiSearchAdviser
        state.payload = payloadSearch.current
        return state
      }
    }
  }

  const [apiState, dispatch] = useReducer(reducer, state)

  useEffect(()=> {
    dispatch('init-resource')
    parse()
  }, [])


  const parse = async () => {
    let values: Adviser[] = await getLocal(StorageKeys.LOCAL_LIST_ADVISER)

    if (values === undefined || values === null) {
      setCall(true)
    } else if (values.length === 0) {
      setCall(true)
    } else {
      let adviserSelection = categorizedAdviserSx(values)
      setSelection(adviserSelection)
    }
  }

  const handleDrawerClose = () => {
    setOpen(false)
  }

  const handleDrawerOpen = () => {
    setOpen(true)
  }


  const handleConfirm = () => {
    saveLocal(StorageKeys.LOCAL_TEMP_ADVISORY, JSON.stringify(advisoryDetails.current))
    Router.push('/school/advisories/details')
  }

  const handleCancel = () => {
    setOpenConfirm(false)
  }

  const onSearch = async (searchBody: SearchAdviosryP) => {
    payloadSearch.current = searchBody
    dispatch('search-advisories')
    setCall(true)
  }

  const handleSelect = (from: AdvisoryTableEntry) => {
    setOpenConfirm(true)
    advisoryDetails.current = advisories.find(entry => entry.advisoryId == from.id)
  }

  return (
    <div>
      <HeaderLayout />
      <CommonLayout className={classes.container}>
        <div className={classes.root}>
          <EmptyDrawer
            variant='persistent'
            anchor='left'
            open={open}
            classes={{
              paper: classes.drawerPaper
            }}
            handleDrawerClose={handleDrawerClose}
            shrinkable
          >
            <AdvisorySelectionCtx
              onSearch={ onSearch }
              listAdviserSx={ selections }
            />
          </EmptyDrawer>
          <DrawerBuddy
            drawerClasses={classes}
            title='Find Adviser'
            onDrawerOpen={ handleDrawerOpen }
            open={open}
          >
            <SearchTable
              title='Adviser List'
              columns={ tableHeaders }
              data={ maptReusltAdvisoryEntry(advisories) }
              onSelect={ handleSelect }
            />
          </DrawerBuddy>
        </div>
        <LoadingOnApiCall
          title={ apiState.title }
          message={ apiState.message }
          actionTitle={ apiState.actionTitle }
          type={ apiState.type }
          autoClose
          call={{
            call: call,
            apiCall: apiState.call.apiCall,
            onSuccess: apiState.call.onSuccess,
            succesMessage: apiState.call.succesMessage,
            payload: apiState.payload,
            onError: apiState.call.onError,
          }}
        />
        <SimpleDialog
          title='View Advisory Details'
          open={ openConfirm }
          content={ 'View Details of '+ advisoryDetails.current.advisoryName }
          positiveAction={ handleConfirm }
          negativeAction={ handleCancel }
        />
      </CommonLayout>
    </div>
  )
}

SearchAdvisories.getInitialProps = async ctx => {
  whenAuthorized(ctx);
}

export default withAuhtSync(SearchAdvisories)