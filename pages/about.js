import {HeaderLayout} from '../components/layout/Layout'

export default function About() {
    return(
        <HeaderLayout>
            <p>About Page</p>
        </HeaderLayout>
    )
}