import { useState, RefObject } from 'react'
import { makeStyles, Paper } from '@material-ui/core';
import { TABLE_ICONS } from './TableIcons';
import { Edit } from '@material-ui/icons';
import MaterialTable, { } from 'material-table'


const useStyle = makeStyles(theme => ({
  root: {
    display: 'flex',
    background: '#8080801a',
    padding: 10
  },
  tableContaier: {
    '& .MuiPaper-elevation2' : {
      boxShadow: 'none'
    },
    flexGrow: 1,
  },
}))

const SearchTable = props => {
  const { title, data, columns, onSelect } = props
  const [selectedRow, setSelectedRow] = useState(null)
  const classes = useStyle()

  return (
    <div className={classes.root}>
      <Paper variant='outlined' className={classes.tableContaier}>
        <MaterialTable
          title={title}
          icons={TABLE_ICONS}
          columns={columns}
          data={data}
          actions={[
            (row) => ({
              icon: () => <Edit style={{ ...styleAction(selectedRow, row) }} />,
              tooltip: 'View and Edit',
              onClick: (event, rowData) => { onSelect(row) }
            })
          ]}
          options={{
            headerStyle: {
              paddingLeft: 15,
            },
            rowStyle: rowData => ({
              ...onSelectedRow(selectedRow, rowData),

            }),
            sorting: true,
          }}
          onRowClick={(event, row) => { setSelectedRow(row) }}
        />
      </Paper>
    </div>
  )
}

const onSelectedRow = (selectedRow, rowData) => {
  const background = (selectedRow && rowData.id === selectedRow.id) ? '#187461' : '#FFF'
  const colorText = (selectedRow && rowData.id === selectedRow.id) ? 'white' : 'black'
  return { backgroundColor: background, color: colorText }

}

const styleAction = (selectedRow, rowData) => {
  const color = (selectedRow && rowData.id === selectedRow.id) ? 'white' : 'black'
  return { color: color }
}

export default SearchTable