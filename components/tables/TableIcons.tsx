
import { AddBox, ArrowDownward, Clear, Edit, Search, SaveAlt, FilterList, FirstPage, LastPage, ChevronRight, ChevronLeft, DeleteOutline } from "@material-ui/icons";
import { forwardRef, RefObject } from "react";

export const TABLE_ICONS = {
    Search: forwardRef((props, ref:RefObject<SVGSVGElement>) => <Search {...props} ref={ref} />),
    Add: forwardRef((props, ref: RefObject<SVGSVGElement> ) => <AddBox {...props} ref={ref} />),
    Clear: forwardRef((props, ref: RefObject<SVGSVGElement>) => <Clear {...props} ref={ref} />),
    ResetSearch: forwardRef((props, ref: RefObject<SVGSVGElement>) => <Clear {...props} ref={ref} />),
    Export: forwardRef((props, ref: RefObject<SVGSVGElement>) => <SaveAlt {...props} ref={ref} />),
    Filter: forwardRef((props, ref: RefObject<SVGSVGElement>) => <FilterList {...props} ref={ref} />),
    FirstPage: forwardRef((props, ref: RefObject<SVGSVGElement>) => <FirstPage {...props} ref={ref} />),
    LastPage: forwardRef((props, ref: RefObject<SVGSVGElement>) => <LastPage {...props} ref={ref} />),
    NextPage: forwardRef((props, ref: RefObject<SVGSVGElement>) => <ChevronRight {...props} ref={ref} />),
    PreviousPage: forwardRef((props, ref: RefObject<SVGSVGElement>) => <ChevronLeft {...props} ref={ref} />),
    Delete: forwardRef((props, ref: RefObject<SVGSVGElement>) => <DeleteOutline {...props} ref={ref} />),
    SortArrow: forwardRef((props, ref: RefObject<SVGSVGElement>) => <ArrowDownward {...props} ref={ref} />),
    DetailPanel: forwardRef((props, ref: RefObject<SVGSVGElement>) => <ChevronRight {...props} ref={ref} />),
};