
import { useState } from 'react'
import { AddCircle } from "@material-ui/icons";
import { TABLE_ICONS } from './TableIcons'
import MaterialTable, {} from 'material-table'

const AddTable = props => {
  const {title, data, columns, onSelect} = props
  const [selectedRow, setSelectedRow] = useState(null)

  return (
    <MaterialTable
      title={title}
      icons = { TABLE_ICONS }
      columns={columns}
      data={data}
      actions={[
        (row) =>({
          icon: ()=> <AddCircle style={{...styleAction(selectedRow, row)}}/>,
          tooltip: 'Add to Advisory',
          onClick: (event, rowData) => {onSelect(row)}
        })
      ]}
      options= {{
        headerStyle: {
          paddingLeft: 15,
        },
        rowStyle: rowData => ({
          ...onSelectedRow(selectedRow, rowData),

        }),
        sorting: true,
      }}
      onRowClick={(event, row) => { setSelectedRow(row) }}
    />
  )
}

const onSelectedRow = (selectedRow, rowData) => {
  const background = (selectedRow && rowData.id === selectedRow.id) ? '#187461': '#FFF'
  const colorText = (selectedRow && rowData.id === selectedRow.id) ? 'white': 'black'
  return {backgroundColor: background, color: colorText}

}

const styleAction = (selectedRow, rowData) => {
  const color = (selectedRow && rowData.id === selectedRow.id) ? 'white': 'black'
  return {color: color}
}

export default AddTable