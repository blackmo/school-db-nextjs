import clsx from "clsx";
import { makeStyles, AppBar, IconButton, Typography, Toolbar } from "@material-ui/core";
import { drawerStyle } from "../../styles/make/component/drawer/drawerStyle";
import MenuIcon from '@material-ui/icons/Menu';


const DrawerBuddy = props => {
  const {drawerClasses: classes, onDrawerOpen, open, title} = props

  return (
    <div className={clsx(classes.content, {
      [classes.contentShift]: open,
    })}>
      <AppBar className={clsx(classes.appBar, {
        [classes.appBarShift]: open,
      })} position='relative' elevation={0}>
        <Toolbar>
          <IconButton
            color="inherit"
            aria-label="open drawer"
            onClick={onDrawerOpen}
            edge="start"
            className={clsx(classes.menuButton, open && classes.hide)}
          >
            <MenuIcon />
          </IconButton>
          <Typography variant="h6" noWrap>
            {title}
          </Typography>
        </Toolbar>
      </AppBar>
      <div className={classes.paper}>
        {props.children}
      </div>
    </div>
  )
}

export default DrawerBuddy