import 'date-fns'
import { useState, useEffect, useRef } from "react"
import { TextField, makeStyles } from '@material-ui/core'
import { KeyboardDatePicker, MuiPickersUtilsProvider } from '@material-ui/pickers'
import { Details_Style } from '../../styles/make/component/student/registerStyle'
import { CheckBoxAdornment } from '../actions/simpleCheckBox'
import { Select } from '../actions/selections'

import DateFnsUtils from '@date-io/date-fns'
import MaskedInput from 'react-text-mask'

const Masking = (props) => {
  const { inputRef, ...other } = props
  return (
    <MaskedInput
      {...other}
      ref={ref => {
        inputRef(ref ? ref.inputElement : null);
      }}
      mask={['0', '9', /\d/, /\d/, '-', /\d/, /\d/, /\d/, '-', /\d/, /\d/, /\d/, /\d/]}
      placeholderChar={'*'}
      showMask
    />
  )
}


const genderSelection = ['Male','Female']
const useStyle = makeStyles(theme => (Details_Style))

const PersonDetails = (props) => {
  const { entityName, form, required, disabled, resetCounter, person, enableNumber, checkedNumber } = props

  const [numberRequired, setNumbeRequired] = useState(enableNumber || checkedNumber)
  const [birthday, setBirthday] = useState(new Date())

  useEffect(() => {
    if (!form.register) {
      throw new Error('register not initialized')
    }

    setNumbeRequired((enableNumber !== undefined) ? enableNumber : true)
    setBirthday(new Date())
  }, [resetCounter, person, enableNumber])


  const InputProps = { readOnly: props.readOnly ? props.readOnly : false }
  const classes = useStyle()
  const setName = (name: string): string => {
    return `${entityName}_${name}`
  }

  const enableNumField = (event) => {
    setNumbeRequired(event.target.checked)
  }

  const handleBirthday = (date) => {
    setBirthday(date)
  }

  return (
    <div>
      <div className={classes.root}>
        <TextField
          className={classes.text} disabled={disabled}
          label='First Name' required={required} variant='outlined'
          defaultValue={person.firstName || ''}
          inputRef={form.register}
          name={setName('firstName')}
          InputProps={InputProps}
        />
        <TextField
          className={classes.text} disabled={disabled}
          defaultValue={person.middleName || ''}
          label='Middle Name' variant='outlined'
          inputRef={form.register}
          name={setName('middleName')}
          InputProps={InputProps}
        />
        <TextField
          className={classes.text} disabled={disabled}
          defaultValue={person.lastName || ''}
          inputRef={form.register}
          label='Last Name' required={required} variant='outlined'
          name={setName('lastName')}
          InputProps={InputProps}
          />
        <Select
          className={classes.text} disabled={disabled} style={{textAlign: 'left'}}
          defaultValue={person.gender || 'Male'}
          form={ form }
          list={genderSelection}
          label='Gender' required={required} variant='outlined'
          name={setName('gender')}
        />
      </div>
      <div className={classes.spacer} />
      <div className={classes.root}>
        <MuiPickersUtilsProvider
          utils={DateFnsUtils}
        >
          <KeyboardDatePicker
            required
            className={classes.text} disabled={disabled}
            inputRef={form.register}
            name={setName('birthday')}
            label='Birth Day' format='MM/dd/yyyy'
            inputVariant='outlined'
            value={person.birthday || birthday}
            onChange={e => { handleBirthday(e) }}
          />
        </MuiPickersUtilsProvider>
        <div className={classes.spacer} />
        <TextField
          className={classes.text} disabled={!numberRequired}
          label='Phone Number' variant='outlined'
          defaultValue={person.mobileNo || ''}
          name={setName('number')}
          inputRef={form.register}
          type='tel'
          InputProps={{
            ...InputProps,
            inputComponent: Masking,
            endAdornment: (<CheckBoxAdornment
              name={setName('includeNumber')}
              inputRef={form.register}
              defaultChecked={numberRequired}
              position='end'
              disabled={disabled}
              label='include'
              onChange={e => { enableNumField(e) }}
            />)
          }}
        />
        <TextField
          className={classes.text} InputProps={InputProps}
          label='Address' required={required} variant='outlined'
          defaultValue={person.address || ''}
          name={setName('address')} inputRef={form.register}
          disabled={disabled}
        />
      </div>
    </div>
  )
}

export default PersonDetails