import PersonDetailsControlled from "../person/detailsControlled";
import {
  makeStyles,
  AppBar,
  Tabs,
  Tab,
  Typography,
  Box
} from "@material-ui/core";
import { useState, useEffect, useRef } from "react";
import { SimpleCheckBox } from '../actions/simpleCheckBox'
import Person from "../../classes/person/Person";
import PersonState from "../../classes/person/PersonState";
import { composePerson } from "../../functions/form/utils";

const useStyle = makeStyles(theme => ({
  zeroPadding: {
    "& div": {
      paddingRight: 0,
      paddingLeft: 0
    }
  },
}));

const INDEX_FATHER = 0
const INDEX_MOTHER = 1
const includesDefault =  { father: true, mother: true }

const GuardianPanelControlled = props => {
  const { resetCounter, form } = props
  const { register, setValue, getValues } = form

  const classes = useStyle();
  const [includes, setIncludes] = useState(includesDefault)
  const [currentTab, setCurrentTab] = useState(0);
  const [mother, setMother] = useState<PersonState>(new PersonState(new Person()));
  const [father, setFather] = useState<PersonState>(new PersonState(new Person()));


  useEffect(() => {
    if (!register) {
      throw new Error('register is not initialized')
    }

    registerPersonForm(register, 'father')
    registerPersonForm(register, 'mother')

  })

  const composeParentOnIndex = (data, index: number) => {
    switch (index) {
      case INDEX_FATHER: {
        //-- render mother inputs
        let motherDetails = composePerson('mother', data)
        mother.person = motherDetails
        mother.includeNumField = data['mother_includeNumber']
        setMother({ ...mother })
        break;
      }
      case INDEX_MOTHER: {
        //-- render father inputs
        let fatherDetails = composePerson('father', data)
        father.person = fatherDetails
        father.includeNumField = data['father_includeNumber']
        setFather({ ...father })
        break;
      }
    }
  }

  const handleChange = (event, index) => {
    if (currentTab != index) {
      // disables multiple clicking the tab causing to re render
      setCurrentTab(index);

      const data = getValues()
      composeParentOnIndex(data, index)
    }
  }

  const handleIncludeMother = event => {
    includes.mother = event.target.checked
    setIncludes({...includes})
    if (event.target.checked) {
      setValue('include_mother', true)
    } else {
      setValue('include_mother', false)
    }
  }

  const handleIncludeFather = event => {
    includes.father = event.target.checked
    setIncludes({...includes})
    if (event.target.checked) {
      setValue('include_father', true)
    } else {
      setValue('include_father', false)
    }
  }


  const handleFieldChange = (name: string, value) => {
    setValue(name, value)
  }


  return (
    <div>
      <div>
        <AppBar position="relative" color="inherit" elevation={0}>
          <Tabs
            value={currentTab}
            onChange={handleChange}
            variant='fullWidth'
            indicatorColor="primary"
          >
            <Tab label="father" disableRipple />
            <Tab label="mother" disableRipple />
          </Tabs>
        </AppBar>
      </div>
      <TabPanel className={classes.zeroPadding} value={currentTab} index={0}>
        <SimpleCheckBox
          checked={includes.father}
          onChange={e => handleIncludeFather(e)}
          label='Include Father'
        />
        <PersonDetailsControlled
          entityName = 'father'
          person={father.person}
          numField={father.includeNumField}
          disabled={!includes.father}
          required={includes.father}
          onChange={handleFieldChange}
        />
      </TabPanel>
      <TabPanel className={classes.zeroPadding} value={currentTab} index={1}>
        <SimpleCheckBox
          checked={includes.mother}
          onChange={e => handleIncludeMother(e)}
          label='Include Mother'
        />
        <PersonDetailsControlled
          entityName = 'mother'
          person={mother.person}
          numField={mother.includeNumField}
          disabled={!includes.mother}
          required={includes.mother}
          onChange={handleFieldChange}
        />
      </TabPanel>
    </div>
  )
}

const TabPanel = props => {
  const { children, value, index, ...other } = props;

  return (
    <Typography
      component="div"
      role="tabpanel"
      hidden={value !== index}
      id={`srollable-force-tab-${index}`}
      {...other}
    >
      {value === index && <Box p={3}>{children}</Box>}
    </Typography>
  )
}




const registerPersonForm = (register, name: string) => {
  register({ name: `${name}_firstName` })
  register({ name: `${name}_middleName` })
  register({ name: `${name}_lastName` })
  register({ name: `${name}_gender` })
  register({ name: `${name}_number` })
  register({ name: `${name}_address` })
  register({ name: `${name}_birthday` })
  register({name: `${name}_includeNumber`})
  register({name: `include_${name}`})
}

export default GuardianPanelControlled
