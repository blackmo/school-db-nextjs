import 'date-fns'
import { useState, useEffect, useRef } from "react"
import {TextField, makeStyles} from '@material-ui/core'
import { KeyboardDatePicker, MuiPickersUtilsProvider, useStaticState} from '@material-ui/pickers'
import DateFnsUtils from '@date-io/date-fns'
import MaskedInput from 'react-text-mask'
import {Details_Style} from '../../styles/make/component/student/registerStyle'
import {CheckBoxAdornment} from '../actions/simpleCheckBox'
import moment from 'moment'

const Masking = (props) => {
    const { inputRef, ...other } = props
    return(
        <MaskedInput
            {...other}
            ref={ref => {
                inputRef(ref ? ref.inputElement : null);
            }}
            mask={[ '0', '9', /\d/, /\d/, '-', /\d/, /\d/, /\d/, '-', /\d/, /\d/, /\d/, /\d/]}
            placeholderChar={'*'}
            showMask
        />
    )
}


const useStyle = makeStyles(theme => (Details_Style))

const PersonDetailsControlled = ( props) => {
    const { entityName, required, disabled, person, onChange, numField} = props

    const [numberRequired, setNumbeRequired] = useState(numField)
    const [birthday, setBirthday] = useState(new Date())

    useEffect(() => {
        let birthdayMoment = moment(person.birthday, 'L')

        setBirthday(birthdayMoment.isValid() ? birthdayMoment.toDate() : new Date())
        setNumbeRequired(numField)
    }, [])

    const InputProps = {readOnly : props.readOnly ? props.readOnly : false}
    const classes = useStyle()
    const setName = (name: string): string =>  {
        return `${entityName}_${name}`
    }

    const enableNumField = (event) => {
        // numFielRequired.current = event.target.checked
        const checked = event.target.checked
        setNumbeRequired(checked)
        onChange(setName('includeNumber'), checked)
    }

    const handleBirthday = (date) => {
        setBirthday(date)
        onChange(setName('birthday'), date)
    }

    const handleFieldChange = (name:string, event) => {
        onChange(name, event.target.value)
    }

    return(
        <div>
            <div className={classes.root}>
                <TextField
                    className={classes.text} disabled = {disabled}
                    label='First Name' required = {required} variant='outlined'
                    defaultValue = {person.firstName || ''}
                    InputProps ={InputProps}
                    onChange = {e => {handleFieldChange(setName('firstName'), e)}}
                />
                <TextField
                    className={classes.text} disabled = {disabled}
                    defaultValue = {person.middleName || ''}
                    label='Middle Name' variant='outlined'
                    onChange = {e => {handleFieldChange(setName('middleName'), e)}}
                    InputProps ={InputProps} 
                />
                <TextField
                    className={classes.text} disabled = {disabled}
                    defaultValue = {person.lastName || ''}
                    label='Last Name' required = {required} variant='outlined'
                    onChange = {e => {handleFieldChange(setName('lastName'), e)}}
                    InputProps ={InputProps} 
                />
            </div>
            <div className={classes.spacer} />
            <div className={classes.root}>
                <MuiPickersUtilsProvider
                    utils={DateFnsUtils}
                >
                    <KeyboardDatePicker
                        required
                        className={classes.text} disabled = {disabled}
                        label='Birth Day [MM/dd/yyy]' format='MM/dd/yyyy'
                        inputVariant='outlined'
                        value = { birthday}
                        onChange={e => { handleBirthday(e) }}
                    />
                </MuiPickersUtilsProvider>
                <div className={classes.spacer} />
                <TextField
                    className={classes.text} disabled = {(disabled || !numberRequired)}
                    label='Phone Number' variant='outlined'
                    defaultValue = {person.mobileNo || ''}
                    type = 'tel'
                    onChange = {e => {handleFieldChange(setName('number'), e)}}
                    InputProps ={{
                        ...InputProps,
                        inputComponent: Masking,
                        endAdornment: (<CheckBoxAdornment
                            checked={numberRequired}
                            position = 'end'
                            disabled={disabled}
                            label='include' 
                            onChange = {e => {enableNumField(e)}}
                        />)
                    }}
                />
                <TextField
                    className={classes.text} InputProps ={InputProps}
                    label='Address' required = {required} variant='outlined'
                    defaultValue = {person.address || ''}
                    disabled = {disabled}
                    onChange = {e => {handleFieldChange(setName('address'), e)}}
                />
            </div>
        </div>
    )
}

export default PersonDetailsControlled