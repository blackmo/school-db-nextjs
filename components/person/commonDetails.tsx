import { makeStyles, Box} from "@material-ui/core"
import { useRef, useEffect, useState } from "react"
import PersonUtils from "../../functions/person/PersonUtils"
import Person from "../../classes/person/Person"

const usetStyle = makeStyles(theme => ({
  headerHorizontal: {
    display: 'flex',
    alignItems: 'center',
  },
  displayHorizontal: {
    padding: 0,
    display: 'flex',
    height: 30
  },
  headingLabel: {
    width: 140
  },
  appendToLeft: {
    margin: 7
  },
  spacer: {
    marginTop: 10,
    marginBottom: 10
  },
  spacer5: {
    marginTop: 5,
    marginBottom: 5
  }

}))

export const CommonDetails = props => {
  const { simpleInfo, editIcon} = props
  const { birthday, gender, address, mobileNo }  = simpleInfo || new Person ()

  const classes = usetStyle()
  return (
    <div>
      <div className={classes.headerHorizontal}>
        <Box className={classes.headingLabel} component='h3' >
          Personal Info
        </Box>
      </div>
      <div className={classes.displayHorizontal}>
        <span className={classes.headingLabel}>Mobile no: </span>
        <span>{ mobileNo || 'NA' }</span>
      </div>
      <div className={classes.displayHorizontal}>
        <span className={classes.headingLabel}>Gender: </span>
        <span>{gender}</span>
      </div>
      <div className={classes.displayHorizontal}>
        <span className={classes.headingLabel}>Birthd Date: </span>
        <span>{birthday}</span>
      </div>
      <div className={classes.displayHorizontal}>
        <span className={classes.headingLabel}>Age: </span>
        <span>{ PersonUtils.getCurrentAge(birthday) }</span>
      </div>
      <div className={classes.displayHorizontal}>
        <span className={classes.headingLabel}>Address: </span>
        <span>{address}</span>
      </div>
    </div>
  )
}

export const SimpleDetails = props => {
  const {name, mobileno} = props
  const classes = usetStyle()
  return (
    <div>
      <div className={classes.displayHorizontal}>
        <span className={classes.headingLabel}>Name : </span>
        <span>{name}</span>
      </div>
      <div className={classes.displayHorizontal}>
        <span className={classes.headingLabel}>Mobile no : </span>
        <span>{mobileno}</span>
      </div>
    </div>
  )
}

export const GuardianDetails = props => {
  const {father: fatherD, mother: motherD} = props

  const [father, setFather] = useState<Person>()
  const [mother, setMOther] = useState<Person>()

  useEffect(() => {

    if (fatherD || motherD) {
      setFather(fatherD)
      setMOther(motherD)
    }

  },[fatherD, motherD])

  const classes = usetStyle()
  return (
    <div>
      <div className={classes.headerHorizontal}>
        <Box className={classes.headingLabel} component='h3' >
          Guardian
        </Box>
      </div>
      <div className={ classes.spacer} />
      <div className={ classes.spacer} />
      <Box component='span' fontWeight='fontWeightBold' >
        Father
      </Box>
      <div className={ classes.spacer5} />
      <SimpleDetails name={ PersonUtils.fullName(father) || 'NA' } mobileno={ getMobileNo(father) }/>
      <div className={ classes.spacer} />
      <Box component='span' fontWeight='fontWeightBold' >
        Mother
      </Box>
      <div className={ classes.spacer5} />
      <SimpleDetails name={ PersonUtils.fullName(mother) || 'NA' } mobileno={ getMobileNo(mother)}/>
    </div>
  )

}

const getMobileNo = (person: Person): string =>  {
  if (person) {
    if (person.mobileNo) {
      return person.mobileNo 
    }
    return 'NA'
  } else {
    return 'NA'
  }
}