import '../../styles/cards/cards.global.scss'
import {
  CardContent,
  Typography,
  CardActionArea,
} from "@material-ui/core";
import Link from 'next/link'

const LinkCard = props => {
    const {href, icon, label} = props
    return (
        <CardActionArea disableRipple>
          <div className="card_captor">
            <Link href = {href}>
              <CardContent>
                {icon}
                <Typography>{label}</Typography>
              </CardContent>
            </Link>
          </div>
        </CardActionArea>
    )
}

export default LinkCard