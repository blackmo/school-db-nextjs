import { Box, TextField, makeStyles, Divider, Button, Typography, MenuItem, Fab } from "@material-ui/core"
import { CheckBoxAdornment } from "./simpleCheckBox"
import { useState, useEffect } from "react"
import { useForm } from "react-hook-form"
import { Details_Style } from "../../styles/make/component/student/registerStyle"
import { SX_DEFAULT_GRADE_LEVELS, SX_DEFAULT_GRADE_SECTIONS } from "../../configs/selections"
import { MuiPickersUtilsProvider, KeyboardDatePicker } from "@material-ui/pickers"
import { Select } from "./selections"

import DateFnsUtils from "@date-io/date-fns"
import SearchIcon from '@material-ui/icons/Search'
import SearchStudentP from "../../classes/response/paylaod/SearchStudentP"
import SimpleDialog from "../prompts/SimpleDialog"

const useStyle = makeStyles(theme => ({
  ...Details_Style,
  buttonSearch: {
    marginTop: 20,
    marginBottom: 20,
    marginRight: 20,
    marginLeft: 20,
    '& button': {
      color: 'white',
      width: '100%',
      background: '#079A7B'
    },
    '& button:hover': {
      backgroundColor: '#06C69DB5'
    }
  },
  spanToLeft: {
    marginLeft: 20,
    color: 'white'
  },
}))


const SelectionCriteria = props => {
  const { gradeLevelSelection, gradeSectionSelection, onSearch } = props

  const { register, handleSubmit, setValue, getValues } = useForm()
  const [gradeLevels, setGradeLevels] = useState(SX_DEFAULT_GRADE_LEVELS)
  const [gradeSections, setGradeSections] = useState(SX_DEFAULT_GRADE_SECTIONS)
  const [check, setCheck] = useState(false)
  const [disable, setDisable] = useState(false)
  const [disableAdvisory, setDisableAdvisory] = useState(true)
  const [year, setYear] = useState(new Date())
  const [open, setOpen] = useState(false)

  const form = {
    register: register,
    setValue: setValue,
  }

  const handleCheckBox = (event) => {
    setCheck(event.target.checked)
    setDisable(event.target.checked)
  }

  const onSubmit = async (data) => {
    const criteria = composeSearchCriteria(data)
    if (disableAdvisory && criteria.lrnID.length == 0 && criteria.lastName.length == 0) {
      setOpen(true)
    } else {
      onSearch(criteria)
    }
  }

  const handleStatusChange = event => {
    if (event.target.value.toUpperCase() == 'NEW') {
      setDisableAdvisory(true)
    } else {
      setDisableAdvisory(false)
    }
  }

  const handleDialogClose = () => {
    setOpen(false)
  }

  const classes = useStyle()
  return (
    <form
    
      autoComplete='off'
      onSubmit={handleSubmit(onSubmit)}
    >
      <div className={classes.root}>
        <Box className={classes.spanToLeft} component='span'>
          LRNID
        </Box>
        <TextField
          className={classes.textWhite}
          variant='outlined'
          name='lrnID'
          inputRef={register}
          InputProps={{
            startAdornment: (<CheckBoxAdornment
              checked={check}
              name='byLrnId'
              inputRef={register}
              position='start'
              onChange={e => { handleCheckBox(e) }}
            />)
          }}
        />
        <Box className={classes.spanToLeft} component='span'>
          Last Name
        </Box>
        <TextField
          variant='outlined'
          className={classes.textWhite}
          name='lastName'
          disabled={disable}
          inputRef={register}
        />
        <Box className={classes.spanToLeft} component='span'>
          Type
        </Box>
        <Select
          className={classes.textWhite}
          name = 'status'
          form = {form}
          variant='outlined'
          list={['New', 'Enrolled']}
          disabled={disable}
          onChange={e => (handleStatusChange(e))}
          defaultValue='New'
        />
        <Divider className={classes.spacer} />
        <div className={classes.spacer} />
        <Box className={classes.spanToLeft} component='span'>
          Class Year
        </Box>
        <MuiPickersUtilsProvider
          utils={DateFnsUtils}
        >
          <KeyboardDatePicker
            required
            views={['year']}
            className={classes.textWhite}
            inputRef={register}
            name={'classYear'}
            format='yyyy'
            inputVariant='outlined'
            value={year}
            onChange={e => { setYear(e) }}
            disabled={disable || disableAdvisory}
          />
        </MuiPickersUtilsProvider>
        <Box className={classes.spanToLeft} component='span'>
          Class Level
        </Box>
        <Select
          className={classes.textWhite}
          name={'classLevel'}
          variant='outlined'
          list={gradeLevels}
          form={form}
          disabled={disable || disableAdvisory}
          defaultValue='All'
        />
        <Box className={classes.spanToLeft} component='span'>
          Section
        </Box>
        <Select
          className={classes.textWhite}
          variant='outlined'
          defaultValue='All'
          form={form}
          name={'classSection'}
          disabled={disable || disableAdvisory}
          list={gradeSections}
        />
        <div className={classes.spacer} />
        <div className={classes.spacer} />
        <div className={classes.buttonSearch}>
          <Button
            type='submit'
          >
            <SearchIcon />
            <label>find student</label>
          </Button>
        </div>
        <div className={classes.spacer} />
      </div>
      <SimpleDialog
        title='Student Details'
        open={open}
        content= 'Criteria is Empty, Input One Field'
        positiveAction={handleDialogClose}
        negativeAction={handleDialogClose}
      />
    </form>
  )
}

const composeSearchCriteria = (data): SearchStudentP => {
  let criteria = new SearchStudentP()
  criteria.byLrnId = data['byLrnId']
  criteria.lrnID = data['lrnID']
  criteria.status = data['status']
  criteria.classLevel = data['classLevel']
  criteria.classSection = data['classSection']
  criteria.classYear = Number(data['classYear'])
  criteria.lastName = data['lastName']

  return criteria
}

export default SelectionCriteria