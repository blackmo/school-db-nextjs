import { FormControlLabel, Checkbox, makeStyles, InputAdornment } from "@material-ui/core"
import { green } from "@material-ui/core/colors"

const useStyle = makeStyles(theme =>({
  checkBox: {
    textAlign: "left",
    marginLeft: 20,
    root: {
      color: green[400],
      "& input": {
        "&$checked": {
          color: green[600]
        }
      }
    }
  }
}))

export const SimpleCheckBox = (props) => {
    const {className, label, ...customProps} = props
    const classes = useStyle()

    return (
      <div className={className || classes.checkBox}>
        <FormControlLabel control={<Checkbox {...customProps} />} label={label} />
      </div>
    );
}

export const CheckBoxAdornment = props => {
    const {position, ...others} = props
    return (
        <InputAdornment position={position}><SimpleCheckBox {...others}/></InputAdornment>
    )
}