import { TextField, MenuItem, makeStyles, createStyles, Theme } from "@material-ui/core"
import { useEffect, useState } from "react";
import register from "../../pages/school/student/batch/register";

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    formControl: {
      margin: theme.spacing(1),
      minWidth: 120,
    },
    selectEmpty: {
      marginTop: theme.spacing(2),
    },
  }),
);

export const SelectNative = props => {
  const { list, ...others } = props
  return (
    <TextField
      {...others}
      select
      SelectProps={{
        native: true
      }}
    >
      {
        list.map((option, index) => {
        return <option key={index} >{option}</option>
        })
      }
    </TextField>
  )
}

export const Select = props => {
  const { defaultValue, list, name, form: {register, setValue}, onChange, ...others } = props

  useEffect(() => {
    register({ name: name });
    setValue(name, defaultValue)
  }, [defaultValue])

   const handleChange = (event) => {
     setValue(name, event.target.value)
     if (onChange) {
       onChange(event)
     }
   }

  return (
    <TextField
      name='some'
      {...others}
      defaultValue={defaultValue}
      onChange={ e => handleChange(e)}
      select
    >
      {
        list.map((option, index) => {
        return <MenuItem key={index} value={option} >{option}</MenuItem>
        })
      }
    </TextField>
  )
}
