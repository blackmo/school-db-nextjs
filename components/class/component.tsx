import LinkCard from "../actions/linkcard"
import '../../styles/layout/layout.global.scss'
import '../../styles/cards/cards.global.scss';

const ClassComponent = props => {
    return(
    <div className="inside_layout line_titles">
      <h3>Student</h3>
      <div className="horizontal_align">
        <LinkCard
          href = '/school/student/register'
        //   icon = {<AccountCircle />}
          label = 'Register'
        />
        <div className="space5" />
        <LinkCard
          href = '/school/student/search'
        //   icon = {<SearchRounded />}
          label = 'Search'
        />
        <div className="space5" />
        <LinkCard
          href = '/school/student/batch/register'
        //   icon = {<Publish/>}
          label = 'Batch Registration'
        />
      </div>
    </div>
    )
}

export default ClassComponent