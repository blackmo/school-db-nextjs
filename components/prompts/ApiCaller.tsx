import LoadingDialog from "./loadingDialog"
import ApiResponseSate from "../../classes/response/ApiResponseState"
import { useState, useEffect } from "react"
import { detApiState } from "../../functions/api/ApiUtils"
import Response from "../../classes/response/Response"

const LoadingOnApiCall = props => {
  const { call, message, autoClose, ...others } = props
  const {call: callStatus,
    payload,
    onSuccess,
    onError,
    onAction,
    onUpdate,
    apiCall,
    successMessage,
    onClose,
  } = call
  const [apiState, setApiState] = useState<ApiResponseSate>(new ApiResponseSate())
  const [actionHidden, setActionHidden] = useState(false)
  const [openLoading, setOpenLoading] = useState(false)

  useEffect(()=> {
    if (autoClose) {
      setActionHidden(true)
    }
  },[])

  useEffect(() => {
    if (autoClose) {
      if(callStatus) {
        callApi()
      }
   } else {
      if(callStatus) {
       setOpenLoading(true)
      }
   }
  }, [callStatus])

  const callApi = async () => {
      setOpenLoading(true)
      setApiState({ ...apiState, called: true })
      let  response = await apiCall(payload)
      let api = detApiState(response, successMessage)
      setApiState(api)
      setTimeout(async () => {
        setApiState(api)
        setTimeout(() => {
          handleApiResponse(api, response)
        }, 300)
      }, 300)
  }

  const handleDialogClose = () => {
    setOpenLoading(false)
    if (onClose) { onClose() }
    
    setTimeout(() => {
      if (autoClose) {
        setActionHidden(true)
      }
      setApiState(new ApiResponseSate())
    }, 300)
  }

  const handleApiResponse = (api: ApiResponseSate, response: Response<any>) => {
    if (api.error) {
      setActionHidden(false)
      if (onError) { onError(api.error) }
    } else {
      onSuccess(response.data)
      onAfterCall(response.data)
    }
  }

  const onAfterCall = (data) => {
    if (autoClose) {
      setOpenLoading(false)
      setTimeout(() => {
        if (onUpdate) {
          onUpdate(data)
        }
        setApiState(new ApiResponseSate())
      }, 300)
    }
    else if (onUpdate) {
      onUpdate(data)
    }
  }

  const hanldeActionDisplay = () => {
    if (apiState.success && !apiState.called) {
      onAction()
      setOpenLoading(false)
    }
    else callApi()
  }

  return (
    <LoadingDialog
      {...others}
      open={ openLoading }
      message={apiState.message || message}
      onClose={handleDialogClose}
      loading={apiState.called}
      onAction={hanldeActionDisplay}
      success={apiState.success}
      error={apiState.error}
      actionHidden={actionHidden}
    />
  )
}

export default LoadingOnApiCall