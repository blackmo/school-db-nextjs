
import { Dialog, DialogTitle, DialogContent, DialogContentText, Button, DialogActions, makeStyles } from "@material-ui/core"
import { Button_Style } from "../../styles/make/component/student/registerStyle"

const themeConfig = {
  root: {
    '& .MuiDialogActions-root': {
      marginBottom: 20,
      marginRight: 20
    }
  },
  maringButtons: {
    marginLeft: 20,
    marginRight: 20,
    marginBottom: 20
  },
  ...Button_Style
}

const useStyle = makeStyles(theme => (themeConfig))

const WithFormDialog = props => {
  const classes = useStyle()
  const { open, handleSubmit, handleClose, title, negativeAction, labeling, autoComplete } = props

  return (
    <Dialog
      className={ classes.root }
      open={ open }
      onClose={ handleClose }
      aria-labelledby='alert-dialog-title'
      aria-describedby='alert-dialog-title'
      disableBackdropClick={ true }
      fullWidth={ true }
    >
      <form autoComplete={ autoComplete } onSubmit={ handleSubmit }>
        <DialogTitle id='dialog-prompt-title' style={{ marginLeft: 20 }}>{title}</DialogTitle>
        <DialogContent id='dialog-prompt-content' >
          { props.children }
        </DialogContent>
        <DialogActions >
          <div className={ classes.buttonNegative }>
            <Button
              id='dialog-prompt-button-negative'
              onClick={e => { negativeAction() }}
            >
              {labeling ? (labeling.negative ? labeling.nagative : 'Cancel') : 'Cancel'}
            </Button>
          </div>
          <div className={ classes.buttonPositive }>
            <Button
              type='submit'
            >
              {labeling ? labeling.positive : 'Confirm'}
            </Button>
          </div>
        </DialogActions>
      </form>
    </Dialog>
  )

}

export default WithFormDialog