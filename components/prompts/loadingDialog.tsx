import {
  Dialog, DialogTitle,
  DialogContent, makeStyles,
  Fab, DialogActions, Button, CircularProgress, Box
} from "@material-ui/core"
import {
  Check,
  ErrorOutlined,
  Search,
  Save,
  Publish} from  '@material-ui/icons/'

import { loadingDialogStyle } from '../../styles/make/component/dialog/dialogStyle'
import clsx from 'clsx'

const useStyle = makeStyles(theme => (loadingDialogStyle(theme)))


const LoadingDialog = props => {
  const {
    open,
    onClose,
    onAction,
    title,
    success,
    error,
    message,
    actionTitle,
    loading,
    type,
    actionHidden
  } = props

  const classes = useStyle()
  let buttonClassName =  clsx({
      [classes.buttonSuccess]: success,
      [classes.buttonError]: error
    })
  let labelActionClassName = clsx({
    [classes.buttonTextAction]: !error
  })

return (
    <Dialog
      aria-labelledby = 'alert-dialog-title'
      aria-describedby = 'alert-dialog-title'
      open={open}
      fullWidth={true} >
      <DialogTitle>{title}</DialogTitle>
      <DialogContent className={classes.root}>
        <div className={classes.wrapper}>
          <Fab
            aria-label='save'
            color='primary'
            className={buttonClassName}
            onClick={onAction}
            disabled={error}
          >
            {(error ? <ErrorOutlined /> : <IconType type={ type } className={buttonClassName} success = {success} />)}
          </Fab>
          {loading && <CircularProgress size={68}  className={classes.fabProgress}/>}
        </div>
        <div className={classes.wrapper}>
          <Button
            className={labelActionClassName}
            disabled={loading}
            onClick={onAction}
            style={{display: error?'none': 'inherit'}}
            >
              {message}
          </Button>
          <Box component='p' style={{display: error?'inherit': 'none'}}>{message}</Box>
        </div>
      </DialogContent>
      <DialogActions>
        <div>
          <Button
            disabled={ actionHidden }
            onClick = {onClose}
            style={{ color: actionHidden? 'white': 'black'}}
          >{actionTitle}</Button>
        </div>
      </DialogActions>
    </Dialog>
  )
}

const IconType = props => {
  const {success, type} = props
  return (
    <div style={{display:'inherit'}}>
      { success ?  <Check />: determineComponent(type)}
    </div>
  )
}

const determineComponent = (type: String) => {
  switch(type) {
    case 'search' : {
      return <Search/>
    }
    case 'upload' : {
      return <Publish/>
    }
    default : {
      return  <Save/>
    }
  }
}

export default LoadingDialog