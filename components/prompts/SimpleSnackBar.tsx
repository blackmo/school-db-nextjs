import { Snackbar } from "@material-ui/core"
import Alert from "@material-ui/lab/Alert"
import { useEffect } from "react"

const SimpleSnackBar = props => {
    const {
      anchorOrigin,
      onClose,
      variant,
    } = props

      let {open,message,severity,} = props


    return (
      <Snackbar
        open={open}
        autoHideDuration={6000}
        onClose={onClose}
        anchorOrigin={ anchorOrigin || { vertical: 'bottom', horizontal: 'right' }}
      >
        <Alert
          severity= {severity}
          elevation={6}
          variant={variant}
          onClose={onClose}
        >
          {message}
        </Alert>
      </Snackbar>
    )
}

export default SimpleSnackBar