import { Dialog, DialogTitle, DialogContent, DialogContentText, Button, DialogActions, makeStyles } from "@material-ui/core"
import { Button_Style } from "../../styles/make/component/student/registerStyle"

const themeConfig = {
    root: {
    },
    ...Button_Style
}

const useStyle = makeStyles(theme => (themeConfig))

const SimpleDialog = props => {
    const classes = useStyle()
    const {open, handleClose, title, content, positiveAction, negativeAction, labeling} = props
    return (
        <Dialog
            className={classes.root}
            open = {open}
            onClose = {handleClose}
            aria-labelledby = 'alert-dialog-title'
            aria-describedby = 'alert-dialog-title'
            fullWidth = {true}
        >
            <DialogTitle id = 'dialog-prompt-title'>{title}</DialogTitle>
            <DialogContent id = 'dialog-prompt-content' >
                <DialogContentText style={{color: 'black'}}>
                    {content}
                </DialogContentText>
                {props.children}
            </DialogContent>
            <DialogActions>
                <div className = {classes.buttonNegative}>
                    <Button
                        id = 'dialog-prompt-button-negative'
                        onClick = { e => {negativeAction()}}
                    >
                        {labeling? (labeling.negative ? labeling.nagative: 'Cancel') : 'Cancel'}
                    </Button>
                </div>
                <div className = {classes.buttonPositive}>
                    <Button
                        id = 'dialog-prompt-button-positive'
                        onClick = { e => {positiveAction()}}
                    >
                        {labeling?  labeling.positive  : 'Confirm'}
                    </Button>
                </div>
            </DialogActions>
        </Dialog>
    )    
}

export default SimpleDialog