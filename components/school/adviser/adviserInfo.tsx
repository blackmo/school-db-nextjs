
import { CommonDetails } from "../../person/commonDetails"
import { Divider, makeStyles, Button, TextField } from "@material-ui/core"
import { useEffect, useState } from "react"
import { useForm } from "react-hook-form"
import { Edit } from "@material-ui/icons"
import { validateMobileNum } from "../../../utils/string"
import { composePerson } from "../../../functions/form/utils"
import { updatePersonDetails } from "../../../functions/person/PersonApiCalls"

import WithFormDialog from "../../prompts/formDialog"
import PersonDetails from "../../person/details"
import SimpleSnackBar from "../../prompts/SimpleSnackBar"
import PrompStatus from "../../../classes/prompt/PromptStatus"
import Person from "../../../classes/person/Person"
import LoadingOnApiCall from "../../prompts/ApiCaller"

const INDEX_PERSON = 0
const INDEX_MOTHER = 1
const INDEX_FATHER = 2

const usetStyle = makeStyles(theme => ({
  root: {
    '& button': {
      minWidth: 100,
      color: 'white',
      background: '#187461',
    },
    '& button:hover': {
      backgroundColor: '#079A7B'
    }
  },
  spacer: {
    marginTop: 10,
    marginBottom: 10
  },
  divider: {
    width: 10,
    height: 'auto',
    display: 'inline-block'
  },
  displayVertical: {
    display: 'flex'
  }
}))

const defaultValue = { entityName: '', person: new Person(), code: '' }

const AdviserInfo = props => {
  const { person, onUpdate } = props
  const { register, setValue, handleSubmit} = useForm()

  const [dialogTitle, setDialogTitle] = useState<String>()
  const [openAdviserDialog, setOpenAdviserDialog] = useState(false)
  const [enableNumber, setEnableNumber] = useState(false)
  const [promptStatus, setStatus] = useState<PrompStatus>(new PrompStatus(false, '', 0))
  const [updateValue, setUpdateValue] = useState<{ entityName: string, person: Person, code: String }>(defaultValue)
  const [call, setCall] = useState(false)

  const classes = usetStyle()
  useEffect(() => {
    if (updateValue) {
      if (!updateValue.person.mobileNo) {
        setEnableNumber(false)
      } else {
        setEnableNumber(true)
      }
    }
  }, [updateValue, openAdviserDialog])

  const onOpenDialog = index => {
    switch (index) {
      default: {
        let updateValue = {
          entityName: 'adviser',
          person: person,
          code: 'update-adviser-person'
        }

        setDialogTitle('Edit Adviser Details')
        setUpdateValue(updateValue)
        break;
      }
    }
    setOpenAdviserDialog(true)
  }

  const onSubmit = async (data) => {
    let person = composePerson(updateValue.entityName, data)
    person.id = updateValue.person.id


    let prompStatus = handleErrors(person)
    setStatus(prompStatus.prompt)

    if (!prompStatus.hasError) {
      setOpenAdviserDialog(false)
      setCall(true)
    }
  }

  const handleCloseAdviserDialog = () => {
    setOpenAdviserDialog(false)
  }

  const handleOnNextPrompt = (event, reason?) => {
    if (reason == 'clickeaway') {
      return
    }

    promptStatus.open = false
    setStatus({ ...promptStatus })
  }

  const handleErrors = (person: Person) => {
    let validNumber = validateMobileNum(person.mobileNo)
    let status = PrompStatus.SUCCESS
    let prompstatus = new PrompStatus(false, '', 0, status)
    let hasError = false


    if (!validNumber) {
      hasError = true
      status = PrompStatus.ERROR
      prompstatus = new PrompStatus(!validNumber, 'ERROR: Invalid Mobile Number', 3, status)
    }

    return { hasError: hasError, prompt: prompstatus }
  }

  const callApi = async (data) =>  {
    return await updatePersonDetails(person)
  }  
  const handleOnSuccess= (person: Person) => {
    setUpdateValue({ ...updateValue, person: person })
    setCall(false)
  }

  const handleUpdate = (data: Person) => {
    onUpdate(updateValue.code, person)
  }

  const handleOnError = () => {
    setCall(false)
  }

  return (
    <div >
      <div className={classes.root}>
        <CommonDetails simpleInfo={person} />
        <div className={classes.spacer} />
        <Button startIcon={<Edit />} onClick={e => { onOpenDialog(INDEX_PERSON) }}>Edit Adviser</Button>
        <Divider className={classes.spacer} />
      </div>
      <WithFormDialog
        title={dialogTitle}
        open={openAdviserDialog}
        negativeAction={handleCloseAdviserDialog}
        handleSubmit={handleSubmit(onSubmit)}
        labeling={{ positive: 'save' }}
        autoComplete='off'
      >
        <PersonDetails
          entityName={updateValue.entityName}
          form={{register, setValue}}
          person={updateValue.person}
          enableNumber={enableNumber}
          required
        />
      </WithFormDialog>
      <SimpleSnackBar
        open={promptStatus.open}
        onClose={handleOnNextPrompt}
        message={promptStatus.message}
        severity={promptStatus.severity}
        variant='filled'
      />
      <LoadingOnApiCall
        title='Update Adviser'
        message='Saving Details'
        actionTitle='OK'
        type='save'
        autoClose
        call={{
          call:call,
          apiCall: callApi ,
          onSuccess: handleOnSuccess,
          onUpdate: handleUpdate,
          onError: handleOnError
        }}
      />
    </div>
  )
}

export default AdviserInfo