
import { Box, TextField, InputAdornment, makeStyles, Divider, Button, Typography, MenuItem, Fab } from "@material-ui/core"
import { useForm } from "react-hook-form"
import { Details_Style } from "../../../styles/make/component/student/registerStyle"
import SearchIcon from '@material-ui/icons/Search'
import SearchAdviserP from "../../../classes/response/paylaod/SearchAdviserP"


const useStyle = makeStyles(theme => ({
  ...Details_Style,
  buttonSearch: {
    marginTop: 20,
    marginBottom: 20,
    marginRight: 20,
    marginLeft: 20,
    '& button': {
      color: 'white',
      width: '100%',
      background: '#079A7B'
    },
    '& button:hover': {
        backgroundColor: '#06C69DB5'
    }
  }, 
  spanToLeft: {
    marginLeft: 20,
    color: 'white'
  }, 
}))


const AdviserSelectionCtx = props => {
  const {onSearch} = props
  const { register, handleSubmit, reset, getValues} = useForm()

  const onSubmit = async (data) => {
    let adviserPayload = new SearchAdviserP()
    adviserPayload.lastName  = data['lastName']
    onSearch(adviserPayload)
  }

  const classes = useStyle()
  return (
    <form
      autoComplete='off'
      onSubmit={ handleSubmit(onSubmit) }
    >
      <div className={ classes.root }>
        <Box className= {classes.spanToLeft} component = 'span'>
          Last Name
        </Box>
        <TextField
          className={classes.textWhite}
          variant='outlined'
          name='lastName'
          inputRef={register}
        />
      <div className={ classes.spacer }/>
      <div className = {classes.buttonSearch}>
        <Button
          type='submit'
        >
            <SearchIcon/>
            <label>Search Adviser</label>
        </Button>
      </div>
      <div className={ classes.spacer }/>
      </div>
    </form>
  )
}

export default AdviserSelectionCtx