import { makeStyles, TextField, Button} from "@material-ui/core";
import { Details_Style } from "../../../styles/make/component/student/registerStyle";
import { useForm } from "react-hook-form";
import { composeAdviser } from "../../../functions/form/utils";
import { validateMobileNum } from "../../../utils/string";
import { getErrorPrompt } from "../../../functions/error/prompts/hanldeError";
import { useEffect } from "react";

import Adviser from "../../../classes/school/Adviser";
import PrompStatus from "../../../classes/prompt/PromptStatus";
import AdviserDetailsForm from "./AdviserDetailsForm";

const useStyle = makeStyles(theme => ({
  ...Details_Style,
  innerBox: {
    maxWidth: 500,
    margin: 'auto',
    textAlign: 'center'
  },
}))


const AviserRegister = props => {

  const {resetCounter, onSubmit: onFormSubmit, onCancel  } = props
  const {register, handleSubmit, reset, setValue} = useForm()
  const classes = useStyle()

  useEffect(() => {
    reset()
  }, [resetCounter])

  const onSubmit = data => {
    const adviser = composeAdviser(data)
    const submitStatus = handleErrors(adviser)

    onFormSubmit({...submitStatus, adviser})
  }

  const handleErrors = (adviser: Adviser) => {
    let validNumber =validateMobileNum(adviser.person.mobileNo)
    let status = PrompStatus.ERROR

    let hasError = false

    let prompts = [
      new PrompStatus(!validNumber, 'ERROR: Invalid Number', 0, status),
    ]

    let prompstatus = getErrorPrompt(prompts)
    hasError = prompstatus.open

    return { hasError: hasError, prompt: prompstatus }
  }
  return (
    <div className={ classes.innerBox}>
      <form autoComplete='off' onSubmit={handleSubmit(onSubmit)}>
        <h1>Register Adviser</h1>
        <AdviserDetailsForm
          form={{register, setValue}}
        />
        <div className={classes.submit}>
          <Button type="submit">{props.positiveLabel || "Save"}</Button>
        </div>
      </form>
      <div className={classes.spacer} />
      <div className={classes.cancel}>
        <Button onClick={e => { onCancel() }}>
          Cancel
         </Button>
      </div>
    </div>
  )
}

export default AviserRegister

