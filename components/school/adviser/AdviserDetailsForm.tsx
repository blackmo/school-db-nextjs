import { makeStyles, TextField } from "@material-ui/core"
import { Details_Style } from "../../../styles/make/component/student/registerStyle"

import PersonDetails from "../../person/details"
import Adviser from "../../../classes/school/Adviser"

const useStyle = makeStyles(theme => (Details_Style))

const AdviserDetailsForm = props => {
  const {value, form, resetCounter, ...others} = props
  const classes = useStyle()
  const adviser = props.adviser ? props.adviser : new Adviser()
  return (
    <div>
      <div className={classes.root}>
        <TextField
          className={classes.text}
          variant="outlined"
          inputRef={form.register}
          defaultValue={adviser.licenseNumber || ''}
          name="adviser_licenseNumber"
          label="PRC ID"
        />
      </div>
      <PersonDetails
        entityName='adviser'
        form={form}
        required
        person={adviser.person}
        resetCounter={resetCounter}
        checkedNumber
      />
    </div>
  )
}

export default AdviserDetailsForm