import "../../../styles/layout/layout.global.scss";
import "../../../styles/cards/cards.global.scss";

import { AccountCircle, SearchRounded, Publish } from "@material-ui/icons";
import LinkCard from '../../actions/linkcard'


export const AdviserComponent = props => {

  return (
    <div className="inside_layout line_titles">
      <h3>Adviser</h3>
      <div className="horizontal_align">
        <LinkCard
          href = '/school/adviser/register'
          icon = {<AccountCircle />}
          label = 'Register'
        />
        <div className="space5" />
        <LinkCard
          href = '/school/adviser/search'
          icon = {<SearchRounded />}
          label = 'Search'
        />
        <div className="space5" />
      </div>
    </div>
  );
 }