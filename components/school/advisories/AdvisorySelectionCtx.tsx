
import { Box, TextField, makeStyles, Button } from '@material-ui/core'
import { useForm } from 'react-hook-form'
import { Details_Style } from '../../../styles/make/component/student/registerStyle'
import { SX_DEFAULT_GRADE_LEVELS, SX_DEFAULT_GRADE_SECTIONS } from '../../../configs/selections'
import { SelectNative } from '../../actions/selections'
import { useState, useEffect } from 'react'
import { CategorizedSelection } from '../../../classes/school/adviser/AdviserSelection'
import { MuiPickersUtilsProvider, KeyboardDatePicker } from '@material-ui/pickers'

import SearchIcon from '@material-ui/icons/Search'
import Autocomplete from '@material-ui/lab/Autocomplete'
import DateFnsUtils from '@date-io/date-fns'
import SearchAdviosryP from '../../../classes/response/paylaod/SeachAdvisoryP'


const useStyle = makeStyles(theme => ({
  ...Details_Style,
  buttonSearch: {
    marginTop: 20,
    marginBottom: 20,
    marginRight: 20,
    marginLeft: 20,
    '& button': {
      color: 'white',
      width: '100%',
      background: '#079A7B'
    },
    '& button:hover': {
      backgroundColor: '#06C69DB5'
    }
  },
  spanToLeft: {
    marginLeft: 20,
    color: 'white'
  },
}))


const AdvisorySelectionCtx = props => {
  const { onSearch, listAdviserSx } = props
  const { register, handleSubmit, setValue } = useForm()

  const [year, setYear] = useState(new Date())
  const [gradeLevels, setGradeLevels] = useState(SX_DEFAULT_GRADE_LEVELS)
  const [gradeSections, setGradeSections] = useState(SX_DEFAULT_GRADE_SECTIONS)

  useEffect(()=> {
    register({name: 'adviser'})
  },[])
  
  const onSubmit = async (data) => {
    let advisoryPayload = composeCriteria(data)
    onSearch(advisoryPayload)
  }

  const handleSelectioOnchange = (selection: CategorizedSelection)  => {
    if (selection == null) {
      setValue('adviser', null)
    } else {
      setValue('adviser', selection.id)
    }
  }


  const classes = useStyle()
  return (
    <form
      autoComplete='off'
      onSubmit={ handleSubmit(onSubmit) }
    >
      <div className={ classes.root }>
        <Box className={ classes.spanToLeft } component='span'>
          Class Year
        </Box>
        <MuiPickersUtilsProvider
          utils={DateFnsUtils}
        >
          <KeyboardDatePicker
            required
            views={['year']}
            className={classes.textWhite}
            inputRef={register}
            name={'classYear'}
            format='yyyy'
            inputVariant='outlined'
            value={year}
            onChange={e => { setYear(e) }}
          />
        </MuiPickersUtilsProvider>
        <div className={ classes.spacer } />
        <Box className={ classes.spanToLeft } component='span'>
          Grade Level
        </Box>
        <SelectNative
          className={ classes.textWhite }
          inputRef={ register }
          variant='outlined'
          name='classLevel'
          list={ gradeLevels }
        />
        <div className={ classes.spacer } />
        <Box className={ classes.spanToLeft } component='span'>
          Grade Section
        </Box>
        <SelectNative
          className={ classes.textWhite }
          inputRef={ register }
          variant='outlined'
          name='classSection'
          list={ gradeSections }
        />
        <div className={ classes.spacer } />
        <Box className={ classes.spanToLeft } component='span'>
          Adviser
        </Box>
        <Autocomplete
          className={ classes.textWhite }
          options={ listAdviserSx.sort((a: CategorizedSelection, b: CategorizedSelection) => -b.firstLetter.localeCompare(a.firstLetter)) }
          groupBy={ (options: CategorizedSelection) => options.firstLetter }
          getOptionLabel={(option: CategorizedSelection) => option.fullName}
          renderInput={ (params) => <TextField { ...params } variant="outlined" /> }
          onChange={ (event, value: CategorizedSelection )=> { handleSelectioOnchange(value)} }
          />
        <div className={classes.buttonSearch}>
          <Button
            type='submit'
          >
            <SearchIcon />
            <label>Search Adviser</label>
          </Button>
        </div>
        <div className={ classes.spacer } />
      </div>
    </form>
  )
}

const composeCriteria = (data) => {
  let criteria = new SearchAdviosryP()

  criteria.adviserId = data['adviser']
  criteria.classSection = data['classSection']
  criteria.classLevel = data['classLevel']
  criteria.classYear = data['classYear']



  return criteria
}

export default AdvisorySelectionCtx