import { mapAdvisoryStudentToList } from "../../../../functions/school/advisory/AdvisoryUtil"
import { callDeleteRefAdvisory } from "../../../../functions/school/ref/AdvisoryStudent"
import { useState, useRef } from "react"

import ListAdvisoryStudent from "./lIstAdvisoryStudent"
import LoadingOnApiCall from "../../../prompts/ApiCaller"
import SimpleDialog from "../../../prompts/SimpleDialog"
import { RefAdvisoryStudent } from "../../../../classes/school/ref/refAdvisory"
import Advisory from "../../../../classes/school/Advisory"
import Student from "../../../../classes/school/Student"

const ListPaneStudent = props => {
  const { advisory, onUpdate } = props
  const [call, setCall] = useState(false)
  const [open, setOpen] = useState(false)
  const studentName = useRef<string>('')
  const ref = useRef<RefAdvisoryStudent>()
  const selected = useRef<Student>(advisory)


  const handleDelete = (item: {lrnId: string, fullName: string, id: number}) => {
    selected.current = advisory.listStudent.find((entry: Student) => entry.lrnID == item.lrnId )
    studentName.current = item.fullName
    ref.current = new RefAdvisoryStudent(advisory.id, item.id)

    setOpen(true)
  }

  const handleCall = async () => {
    return await callDeleteRefAdvisory(ref.current)
  }

  const handleSuccess = (data) => {

    let newlist = advisory.listStudent.filter((item: Student)=> item.lrnID != selected.current.lrnID)
    advisory.listStudent = newlist

    onUpdate(advisory)
    setCall(false)
  }

  const handleError = () => {
    setCall(false)
  }

  const handleConfirm = () => {
    setCall(true)
    setOpen(false)
  }
  
  const handleCancel = () => {
    setOpen(false)
  }

  return (
    <div>
      <h3>Student List</h3>
      <ListAdvisoryStudent
        contents={mapAdvisoryStudentToList(advisory.listStudent)}
        onDelete={ handleDelete }
      />
      <SimpleDialog
        open={ open }
        title='Confirm Action'
        content={ 'Remove ' + studentName.current +  ' from ' + advisory.advisoryName}
        positiveAction={ handleConfirm }
        negativeAction={ handleCancel }
      />
      <LoadingOnApiCall
        title= 'Updating Advisory'
        message='Removing Student'
        actionTitle='Ok'
        type='save'
        autoClose
        call={{
          call: call,
          apiCall: handleCall,
          onSuccess: handleSuccess,
          onError: handleError,
          successMessage: 'SuccessFully Removed Student'

        }}
      />
    </div>
  )
}

export default ListPaneStudent