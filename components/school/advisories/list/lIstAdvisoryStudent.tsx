import { List, ListItem, ListItemIcon, makeStyles, Divider, Fab, IconButton, Grid, Typography, Tooltip } from "@material-ui/core"
import { Delete } from "@material-ui/icons"

const useStyle = makeStyles(theme=>({
  root: {
    width: '100%',
    maxHeight: 500,
    overflow: 'auto',
    position: 'relative'

  },
  horizontal: {
    display: 'flex'
  },
  divider: {
    with: '100%',
    background: 'black'
  },
  icon: {
    '&.MuiIconButton-root' : {
        height: 40,
        width: 40,
        backgroundColor: '#a69f9f',
        color: 'white',
    },
    '&.MuiIconButton-root:hover': {
        backgroundColor: '#187461'
    }
  },
  listItem: {
    minWidth: 200
  },
  alignRight: {
    '&.MuiGrid-item': {
      flexGrow: 1
    }
  }
  
}))

const ListAdvisoryStudent = props => {
  const { contents, onDelete , sortBy} = props
  const classes = useStyle()

  const handleDelete = (lrnId: string) => {
    onDelete(lrnId)
  }

  return (
    <div className={ classes.root }>
      <Divider/>
      <List >
        {contents.map((item, index) => {
          return(
            <div>
              <ListItem key={index}>
                <ListItemIcon >
                  <DeleteButton className={ classes.icon } item={item} onDelete={ handleDelete }/>
                </ListItemIcon>
                  <Grid sm={12} container spacing={2}>
                    <Grid item xs={8} spacing={2}>
                      <Typography gutterBottom variant='body2'>{item.fullName}</Typography>
                    </Grid>
                    <Grid xs={4} className={ classes.alignRight} item spacing={6}>
                      <Typography  gutterBottom variant='body2'>{item.lrnId}</Typography>
                    </Grid>
                  </Grid>
              </ListItem>
              <Divider/>
            </div>
          )
        })}
      </List>
    </div>
  )
}

const DeleteButton = props => {
  const {className, onDelete, item} = props
  return(
    <Tooltip title='delete'>
       <IconButton  className={ className } onClick={ e => { onDelete(item) }}>
         <Delete height={10} width={10}/>
       </IconButton>
    </Tooltip>
  )
}

export default ListAdvisoryStudent