import '../../../styles/layout/layout.global.scss'
import LinkCard from '../../actions/linkcard';
import { Group, SearchRounded } from '@material-ui/icons';

export const AdvisoriesCompnent = props => {
  return (
    <div className="inside_layout line_titles">
      <h3>Adviser</h3>
      <div className="horizontal_align">
        <LinkCard
          href = '/school/advisories/register'
          icon = {<Group/>}
          label = 'Register'
        />
        <div className="space5" />
        <LinkCard
          href = '/school/advisories/search'
          icon = {<SearchRounded />}
          label = 'Search'
        />
        <div className="space5" />
      </div>
    </div>
  );
}