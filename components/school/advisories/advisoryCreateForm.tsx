import { makeStyles, Button } from "@material-ui/core"
import { Details_Style } from "../../../styles/make/component/student/registerStyle"
import { useForm } from "react-hook-form"
import { useState, useEffect } from "react"
import { callGetAllAdviser } from "../../../functions/school/adviser/AdviserAPICalls"
import { AdviserSelection } from "../../../classes/school/adviser/AdviserSelection"
import { saveLocal, getLocal } from "../../../utils/LocalStorageUtil"
import { StorageKeys as StorageKeys } from "../../../configs/constants/keys"
import { comsposeAdvisory } from "../../../functions/form/utils"

import Adviser from "../../../classes/school/Adviser"
import AdvisoryDetailForm from "./advisoryDetailForm"
import PersonUtils from "../../../functions/person/PersonUtils"
import LoadingOnApiCall from "../../prompts/ApiCaller"

const useStyle = makeStyles(theme => ({
  ...Details_Style,
  root: {
    height: "100%",
    zIndex: 1,
    overflow: "hidden",
    position: "relative",
    display: "grid",
  },
  innerBox: {
    maxWidth: 500,
    margin: 'auto',
    textAlign: 'center'
  },
}))

const AdvisoryCreateForm = props => {
  const {onSubmit: formSubmit, onCancel, resetCounter} = props
  const { register, handleSubmit, setValue, reset, getValues } = useForm()
  const [call, setCall] = useState(false)
  const [listSelections, setListSelections] = useState<AdviserSelection[]>([])
  const [listAdvisers, setAdvisers] =  useState<Adviser[]>([])

  useEffect(() => {
    parse()
  },[])

  const parse = async () => {
    let values: Adviser[] = await getLocal(StorageKeys.LOCAL_LIST_ADVISER)

    if (values === undefined || values === null) {
      setCall(true)
    } else if (values.length === 0) {
      setCall(true)
    } else {
      let adviserSelection = transformToAdvisersSelction(values)
      setAdvisers(values)
      setListSelections(adviserSelection)
    }
  }

  const callApi = async () => {
    return await callGetAllAdviser()
  }

  const handleOnSuccess = ( advisers: Adviser[]) => {
    let selectionAdvisers = transformToAdvisersSelction(advisers)

    saveLocal(StorageKeys.LOCAL_LIST_ADVISER, advisers)
    setListSelections(selectionAdvisers)
    setAdvisers(advisers)
    setCall(false)
  }

  const onSubmit =  data => {
    let advisory = comsposeAdvisory(data)
    let selected: AdviserSelection = data['adviser']
    advisory.adviser = listAdvisers[selected.index]

    formSubmit(advisory)
  }

  const classes = useStyle()
  return (
    <div className={ classes.innerBox }>
      <h1>Create and Register Class Advisory</h1>
      <form  autoComplete='off' onSubmit={ handleSubmit(onSubmit)}>
        <AdvisoryDetailForm
          form= {{
            register: register,
            setValue: setValue,
            reset: reset
          }}
          listAdvisers={ listSelections }
          resetCounter={ resetCounter }
        />
        <div className={ classes.spacer }/>
        <div className={ classes.submit }>
          <Button type='submit'>Create and Register</Button>
        </div>
      </form>
      <div className={ classes.spacer}/>
      <div className={ classes.cancel} >
        <Button onClick={ e => { onCancel() }}>Cancel</Button>
      </div>
      <LoadingOnApiCall
          title='Getting Resources'
          message='Fetching List Of Advisers'
          actionTitle='cancel'
          type='search'
          autoClose
          call={{
            call: call,
            apiCall: callApi,
            onSuccess: handleOnSuccess,
            succesMessage: 'Retreived Data Successfully'
          }}
      />
    </div>
  )
}

const transformToAdvisersSelction = (advisers: Adviser[]) : AdviserSelection[] => {
  return advisers.map((adviser: Adviser, index) => {
      return new AdviserSelection(adviser.id, PersonUtils.fullName(adviser.person), index)
  })

}

export default AdvisoryCreateForm