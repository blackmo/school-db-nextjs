
import { makeStyles, TextField, Button, IconButton, Tooltip } from "@material-ui/core"
import { Details_Style, Details_Edit } from "../../../styles/make/component/student/registerStyle"
import { useState, useEffect, useRef } from "react"
import { useForm } from "react-hook-form"
import { Edit } from "@material-ui/icons"
import { getLocal } from "../../../utils/LocalStorageUtil"
import { StorageKeys } from "../../../configs/constants/keys"
import { AdviserSelection } from "../../../classes/school/adviser/AdviserSelection"
import { transformToAdvisersSelction } from "../../../functions/school/adviser/AdviserUtils"
import { comsposeAdvisory as composeAdvisory } from "../../../functions/form/utils"
import { callUpdateAdvisory } from "../../../functions/school/advisory/AdvisoryApiCalls"

import WithFormDialog from "../../prompts/formDialog"
import Adviser from "../../../classes/school/Adviser"
import AdvisoryDetailForm from "./advisoryDetailForm"
import Advisory from "../../../classes/school/Advisory"
import PersonUtils from "../../../functions/person/PersonUtils"
import LoadingOnApiCall from "../../prompts/ApiCaller"

const usetStyle = makeStyles(theme => ({
  ...Details_Style,
  ...Details_Edit
}))

const SimpleInfoAdvisory = props => {
  const classes = usetStyle()
  const { advisory: inputAdvisory } = props
  const { register, handleSubmit, setValue, reset } = useForm()

  const [openDialog, setOpenDialog] = useState(false)
  const [advisers, setAdvisers] = useState<Adviser[]>([])
  const [advisory, setAdvisory] =useState<Advisory>(inputAdvisory)
  const [call, setCall] = useState(false)

  const adviserSelection = useRef<AdviserSelection[]>([])
  const updatedAdvisory = useRef<Advisory>()

  useEffect(() => {
    register({ name: 'index' })
    parse()
  }, [])

  useEffect(()=> {
    setAdvisory(inputAdvisory)
  }, [inputAdvisory])

  const parse = async () => {
    const listAdviser: Adviser[] = await getLocal(StorageKeys.LOCAL_LIST_ADVISER)
    setAdvisers(listAdviser)
    adviserSelection.current = transformToAdvisersSelction(listAdviser)
  }

  const handleClose = event => {
    setOpenDialog(false)
  }

  const handleNegativeAction = event => {
    setOpenDialog(false)
  }

  const handleEditId = event => {
    setOpenDialog(true)
  }

  const formSubmit = data => {
    let updatedValue = composeAdvisory(data)
    let adviser: Adviser = data['adviser']

    updatedValue.id = advisory.id
    let found =   advisers.find((entry: Adviser) => entry.id == adviser.id)
    updatedValue.adviser = found

    updatedAdvisory.current = updatedValue

    setOpenDialog(false)
    setCall(true)
  }

  const apiCall = async () => {
    return await callUpdateAdvisory(updatedAdvisory.current)
  }

  const handleSuccess = (advisory: Advisory) => {
    setAdvisory(updatedAdvisory.current)
    setCall(false)
  }

  const handleError = (data) => {
    setCall(false)
  }

  return (
    <div >
      <div className={classes.root}>
        <div className={classes.displayHorizontal}>
          <span>Class Year: </span>
        &nbsp;
        <span>{advisory.classYear}</span>
        </div>
        <div className={classes.displayHorizontal}>
          <span>Grade & Section: </span>
        &nbsp;
        <span>{advisory.classLevel} - {advisory.sectionNumber}</span>
        </div>
        <div className={classes.displayHorizontal}>
          <span>Advisory Name: </span>
        &nbsp;
        <span>{advisory.advisoryName}</span>
        </div>
        <div className={classes.displayHorizontal}>
          <span>Adviser: </span>
        &nbsp;
        <span>{ PersonUtils.fullName(advisory.adviser.person)}</span>
        </div>
        <Tooltip
          title={<span>{'Edit'}</span>}
        >
          <Button
            onClick={e => { handleEditId(e) }}
            className={classes.submit}
            endIcon={<Edit />}
          >
            Edit Details
        </Button>
        </Tooltip>
      </div>
      <WithFormDialog
        title='Update Advisory Details'
        handleClose={handleClose}
        negativeAction={handleNegativeAction}
        handleSubmit={handleSubmit(formSubmit)}
        open={openDialog}
      >
        <div>
        <AdvisoryDetailForm
          form= {{
            register: register,
            setValue: setValue,
            reset: reset
          }}
          listAdvisers={ adviserSelection.current }
          advisory={ advisory }
        />
        </div>
      </WithFormDialog>
      <LoadingOnApiCall
        title='Update Advisory Details'
        message='Saving Details'
        actionTitle='Ok'
        type='save'
        autoClose
        call = {{
          call: call,
          apiCall: apiCall,
          onSuccess: handleSuccess,
          onError: handleError,
          succesMessage: 'Successfully Updated Advisory'
        }}
      />
    </div>
  )
}

export default SimpleInfoAdvisory