import { makeStyles } from "@material-ui/styles"
import { Details_Style } from "../../../styles/make/component/student/registerStyle"
import { TextField, IconButton, Avatar, Tooltip, Collapse, List, ListItem, ListItemText, Divider, Paper, Typography } from "@material-ui/core"
import { useState, useEffect } from "react"
import { SC_GRADLE_LEVELS, SC_GRADE_SECTIONS } from "../../../configs/selections"
import { AdviserSelection } from "../../../classes/school/adviser/AdviserSelection"
import { SelectNative } from "../../actions/selections"
import { MuiPickersUtilsProvider, KeyboardDatePicker, DatePicker } from "@material-ui/pickers"

import DateFnsUtils from "@date-io/date-fns"
import PersonIcon from '@material-ui/icons/Person';
import Autocomplete from '@material-ui/lab/Autocomplete'
import Advisory from "../../../classes/school/Advisory"
import PersonUtils from "../../../functions/person/PersonUtils"

const useStyle = makeStyles(theme => ({
  ...Details_Style,
  autoComplete: {
    '& .MuiAutocomplete-listbox': {
      background: '#187461',
      color: 'red'
    }
  }
}))


const AdvisoryDetailForm = props => {
  const { form, listAdvisers, resetCounter, advisory: inputAdvisory} = props
  const advisory: Advisory = {...inputAdvisory}
  const classes = useStyle()
  const {register, setValue, reset} = form
  const [gradeLevels, setGradeLevels] = useState(SC_GRADLE_LEVELS)
  const [gradeSections, setGradeSections] = useState(SC_GRADE_SECTIONS)
  const [advisers, setAdvisers] = useState<AdviserSelection[]>(listAdvisers)
  const [selectedName, setSelectedName] = useState('')
  const [open, setOpen] = useState(false)
  const [year, setYear] = useState(new Date())

  useEffect(()=> {
    reset()
    setSelectedName('')
    setValue('adviser', selectedName)
  }, [resetCounter])

  useEffect(() => {
    const name = PersonUtils.fullName(advisory.adviser.person)
    setSelectedName(name)
    setAdvisers(listAdvisers)
    if (register) {
      register({ name: 'adviser' })
    } else {
      throw Error('register is not defined')
    }
  }, [])

  const handleSelection = (event, value: AdviserSelection) => {
    setValue('adviser', value)
    setTimeout(() => {
      setSelectedName(value.fullName)
      setOpen(false);
    }, 300)
  }

  return (
    <div className={classes.root}>
      <MuiPickersUtilsProvider
        utils={DateFnsUtils}
      >
        <KeyboardDatePicker
          required
          views={['year']}
          className={classes.text}
          inputRef={register}
          name={'classYear'}
          label='Class Year'
          format='yyyy'
          inputVariant='outlined'
          value={ year }
          onChange={e => { setYear(e) }}
        />
      </MuiPickersUtilsProvider>
      <div className={classes.spacer} />
      <SelectNative
        defaultValue={ advisory.classLevel || gradeLevels[0]}
        className={classes.text}
        name='classLevel'
        inputRef={register}
        label='Grade Level'
        variant='outlined'
        required
        list={gradeLevels}
      />
      <div className={classes.spacer} />
      <TextField
        className={classes.text}
        defaultValue={ advisory.advisoryName }
        name='advisoryName'
        inputRef={register}
        label='Advisory Name'
        variant='outlined'
        required
      />
      <TextField
        className={classes.text}
        name='sectionName'
        defaultValue={ advisory.sectionName }
        inputRef={register}
        label='Section Name'
        variant='outlined'
        required
      />
      <div className={ classes.spacer } />
      <SelectNative
        className={ classes.text }
        defaultValue={ advisory.sectionNumber || gradeSections[0] }
        name='sectionNumber'
        inputRef={register}
        label='Grade Section'
        variant='outlined'
        required
        list={ gradeSections }
      />
      <div className={ classes.spacer } />
      <TextField
        className={ classes.text }
        value={ selectedName }
        label='Adviser'
        variant='outlined'
        required
        
        InputLabelProps={{ shrink: true}}
        InputProps={{
          endAdornment: (<ButtonAdviser onClick={e => { setOpen(true); }} />)
        }}
      />
      <Collapse in={open} timeout='auto' unmountOnExit className={classes.autoComplete}>
        <div className={ classes.spacer }/>
        <Paper variant='outlined' style={{padding: 10}}>
        <Typography component='h6' style={{textAlign:'left', marginLeft:20}}>Select Adviser</Typography>
        <Autocomplete
          className={classes.text}
          id="combo-box-demo"
          options={advisers}
          onChange={(e, value) => { handleSelection(e, value)} }
          getOptionLabel={(option: AdviserSelection) => option.fullName}
          renderInput={(params) => <TextField  {...params}  variant="outlined" />}
        />
        </Paper>
      </Collapse>
    </div>
  )
}

const ButtonAdviser = props => {
  const { ...others } = props
  return (
    <Tooltip title='Select an Adviser'>
      <IconButton {...others} ><PersonIcon /></IconButton>
    </Tooltip>
  )
}

export default AdvisoryDetailForm
