import { Paper, Divider, Typography } from "@material-ui/core"
import { useForm } from "react-hook-form"
import { makeStyles } from "@material-ui/styles"
import { useState, useReducer, useRef } from "react"
import { CallStatus, ApiStateReducer } from "../../../classes/api/ApiStateReducer"
import { callSearchStudentBy } from "../../../functions/school/student/StudenApiCalls"
import { mapToStudentDetails } from "../../../functions/school/student/StudentUtils"
import { callAddRefAdvisory } from "../../../functions/school/ref/AdvisoryStudent"
import { RefAdvisoryStudent } from "../../../classes/school/ref/refAdvisory"

import AddTable from '../../tables/AddTable'
import SimpleStudentCriteria from "../student/StudentSimpleCriteria"
import LoadingOnApiCall from "../../prompts/ApiCaller"
import Student from "../../../classes/school/Student"
import SearchStudentP from "../../../classes/response/paylaod/SearchStudentP"
import SimpleDialog from "../../prompts/SimpleDialog"
import StudentDetails from "../../../classes/school/student/StudentDetails"
import PersonUtils from "../../../functions/person/PersonUtils"
import ListPaneStudent from "./list/listpaneStudent"

const tableHeaders = [
  {title:'LRN ID', field: 'lrnID'},
  {title: 'Last Name', field: 'lastName'},
  {title: 'First Name', field: 'firstName'},
  {title: 'Middle Name', field: 'middleName'}
]

const useStyle = makeStyles(theme => ({
  root: {
    display: 'flex',
    background: '#8080801a',
    padding: 10
  },
  tableContaier: {
    '& .MuiPaper-elevation2' : {
      boxShadow: 'none'
    },
    flexGrow: 1,
    marginLeft: 10,
  },
  searchContainer: {
    height: 'auto',
    '& form': {
      display: 'flext-root',
      height: '100%'
    },
    rounded: {
      borderRadius: 1
    }
  }
}))

const AdvisoryStudents = props => {
  const {advisory, onUpdate} = props
  const {register, handleSubmit} = useForm()
  const [students, setStudents] = useState<Student[]>([])
  const [call, setCall] = useState(false)
  const [open, setOpen] = useState(false)
  const [toCall, setToCall] = useState(false)

  const classes =useStyle()
  const searchCriteria = useRef<SearchStudentP>()
  const selectedStudent = useRef<Student>(new Student())

  const searchStudent: CallStatus = {
    async apiCall(data: SearchStudentP) {
      return await callSearchStudentBy(searchCriteria.current)
    },
    onSuccess(data: Student[]) {
      setStudents(data)
      setCall(false)
    },
    onError(data) {
      setCall(false)
    },
    successMessage: 'Successfully Retrieved Student List'
  }

  const addRefference: CallStatus = {
    async apiCall(data: RefAdvisoryStudent) {
      let ref = new RefAdvisoryStudent(advisory.id, selectedStudent.current.id)
      return await callAddRefAdvisory(ref)
    },
    onSuccess(data: Student[]) {
      if (onUpdate) {
        advisory.listStudent.push(selectedStudent.current)
        onUpdate(advisory)
      }
      setCall(false)
    },
    onError(data) {
      setCall(false)
    },
    successMessage: 'Successfully Save Deails to Adviosry'
  }

  const defaultState = new ApiStateReducer()
  defaultState.call = searchStudent
  defaultState.actionTitle = 'OK'
  defaultState.message = 'Getting Student List'
  defaultState.title = 'Getting Resources'
  defaultState.type = 'search'

  const reducer = (state: ApiStateReducer, callType: string) => {
    switch (callType) {
      case 'searh-student': {
        return defaultState
      }
      case 'add-refernce': {
        state.call = addRefference
        state.actionTitle = 'OK'
        state.message = 'Saving Student To Advisory'
        state.title = 'Saving Detials'
        state.type = 'save'

        return state
      }
    }
  }

  const [apiState, dispatch] = useReducer(reducer, defaultState)

  const onSubmit = (data) => {
      let criteria: SearchStudentP = new SearchStudentP()
      criteria.lrnID = data['lrnid']
      criteria.lastName = data['lastName']
      searchCriteria.current = criteria

      if ( criteria.lrnID || criteria.lastName) {
        dispatch('searh-student')
        setCall(true)
      } else {
        setOpen(true)
      }
  }

  const handleDialogClose = () => {
    setOpen(false)
  }

  const handleSelect= (data: StudentDetails) => {
    selectedStudent.current = students.find((value: Student) => value.lrnID == data.lrnID)
    setToCall(true)
  }

  const handlePositiveCall = () => {
    dispatch('add-refernce')
    setCall(true)
    setToCall(false)    
  }

  const handleNegativeCall = () => {
    setToCall(false)    
  }

  return (
    <div>
      <div className={classes.root}>
        <Paper elevation={0} className={ classes.searchContainer }>
          <form autoComplete='off' onSubmit={handleSubmit(onSubmit)}>
            <SimpleStudentCriteria
              register={ register }
            />
          </form>
        </Paper>
        <Paper variant='outlined' className={ classes.tableContaier }>
          <AddTable
            title='Select Student to Add'
            columns={ tableHeaders }
            data={ mapToStudentDetails(students) }
            onSelect={ handleSelect }
          />
        </Paper>
      </div>
      <ListPaneStudent
        advisory={ advisory }
        onUpdate={ onUpdate }
      />
      <SimpleDialog
        open={ open }
        content='Empty Search Criteria, Set Atleast One'
        positiveAction={ handleDialogClose }
        negativeAction={ handleDialogClose }
      />
      <SimpleDialog
        open={ toCall }
        title='Confirm Action'
        content={ 'Save Student into ' + `${advisory.advisoryName} : ${advisory.classLevel} - ${advisory.sectionNumber}`}
        positiveAction={ handlePositiveCall }
        negativeAction={ handleNegativeCall }
      >
        <Typography>
          <span>Student Details:</span>
          <span>{PersonUtils.fullName(selectedStudent.current.person)}</span>
        </Typography>
      </SimpleDialog>
      <LoadingOnApiCall
        title={apiState.title}
        message={apiState.message}
        actionTitle={apiState.actionTitle}
        type={apiState.type.toString()}
        autoClose
        call={{
          call: call,
          apiCall: apiState.call.apiCall,
          onSuccess: apiState.call.onSuccess,
          onError: apiState.call.onError,
          successMessage: apiState.call.successMessage

        }}
      />
    </div>
  )
}

export default AdvisoryStudents