import { makeStyles, Button } from '@material-ui/core'
import { Details_Style } from '../../../styles/make/component/student/registerStyle'
import { useForm } from 'react-hook-form'
import { useState, useEffect } from 'react'
import { composeStudent, composePerson } from '../../../functions/form/utils'
import { isValidatedFields, isValidNumber } from '../../../functions/CheckSum'

import StudentDetailForm from './studentDetailForm'
import GuardianPanelControlled from '../../person/guardianPaneControlled'
import PrompStatus from '../../../classes/prompt/PromptStatus'
import Person from '../../../classes/person/Person'
import Student from '../../../classes/school/Student'

const useStyle = makeStyles(theme => ({
  ...Details_Style,
  root: {
    height: "100%",
    zIndex: 1,
    overflow: "hidden",
    position: "relative",
    display: "grid",
  },
  innerBox: {
    maxWidth: 500,
    margin: 'auto',
    textAlign: 'center'
  },
}))

const RegisterForm = props => {
  const { onSubmit: onFormSubmit, resetCounter, onCancel } = props

  const classes = useStyle()
  const [promptStatus, setStatus] = useState<PrompStatus>(new PrompStatus(false, '', 0))
  const { register, handleSubmit, setValue, reset, getValues } = useForm({
    defaultValues: {
      'student_includeNumber': true,
      'father_includeNumber': true,
      'father_gender': 'Male',
      'mother_includeNumber': true,
      'mother_gender': 'Female',
      'include_mother': true,
      'include_father': true
    }
  })

  useEffect(() => {
    reset()
  }, [resetCounter])


  const onSubmit = async (data) => {
    const student = composeStudent(data)
    student.father = setParent('father', data)
    student.mother = setParent('mother', data)

    const includes = {
      father: data.include_father,
      mother: data.include_mother
    }

    const submitStatus = handleErrors(student, includes)

    onFormSubmit({ ...submitStatus, student: student })
  }

  const handleErrors = (student: Student, includes: { father: boolean, mother: boolean }) => {
    let validFields = isValidatedFields(student, includes)
    let validNumber = isValidNumber(student, includes)
    let status = PrompStatus.ERROR

    let hasError = false
    let prompstatus = null

    let prompts = [
      new PrompStatus(!validFields.father, 'ERROR: Required fields on Parent Father with * not set', 0, status),
      new PrompStatus(!validFields.mother, 'ERROR: Required fields on Parent Mother with * not set', 1, status),
      new PrompStatus(!validNumber.student, 'ERROR: Student Number is invalid', 2, status),
      new PrompStatus(!validNumber.father, 'ERROR: Father Number is invalid', 3, status),
      new PrompStatus(!validNumber.mother, 'ERROR: Mother Number is invalid', 4, status),
    ]

    for (let i = 0; i < prompts.length; i++) {
      if (prompts[i].open) {
        hasError = true
        prompstatus = prompts[i]
        break;
      }
    }

    return { hasError: hasError, prompt: prompstatus }
  }

  const handleOnNextPrompt = (event, reason?) => {
    if (reason == 'clickeaway') {
      return
    }

    promptStatus.open = false
    setStatus(Object.assign({}, promptStatus))
  }

  return (
    <div className={classes.innerBox}>
      <h1>Register Student</h1>
      <div className={classes.innerBox}>
        <form autoComplete='off' onSubmit={handleSubmit(onSubmit)}>
          <StudentDetailForm
            form={{ register, setValue}}
            resetCounter={resetCounter}
            positiveLabel='Register'>
            <h1>Guardian</h1>
            <GuardianPanelControlled
              key = {resetCounter}
              setValue={setValue}
              form={{ register: register, setValue: setValue, getValues: getValues }}
            />
          </StudentDetailForm>
          <div className={classes.submit}>
            <Button type="submit">{props.positiveLabel || "Save"}</Button>
          </div>
        </form>
        <div className={classes.spacer} />
        <div className={classes.cancel}>
          <Button
            onClick={e => {
              onCancel();
            }}
          >
            Cancel
           </Button>
        </div>
      </div>
    </div>
  )
}

const setParent = (name: string, data): Person => {
  if (data[`include_${name}`]) {
    return composePerson(name, data)
  }
  return null
}

export default RegisterForm