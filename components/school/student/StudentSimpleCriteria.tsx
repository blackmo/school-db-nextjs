import { makeStyles } from "@material-ui/styles"
import { Style_Search_Student, Button_Style, Details_Style } from "../../../styles/make/component/student/registerStyle"
import { TextField, Button, Paper, Box, Typography } from "@material-ui/core"
import { Search, Height } from "@material-ui/icons"

const useStyle = makeStyles(theme => ({
  ...Style_Search_Student,
  spacer: {
    ...Details_Style.spacer
  },
  buttonSubmit: {
    ...Details_Style.submit
  }, 
  innerBox: {
    '& .MuiPaper-root': {
      display: 'inherit',
    },
    maxWidth: 300,
    padding: 10,
    height: '100%'
    
  },
  title: {
    textAlign: 'center',
    margin: 0
  }
}))

const SimpleStudentCriteria = props => {
  const {register} = props
  const classes = useStyle()
  return (
    <Paper variant='outlined' className={ classes.innerBox }>
      <Typography ><h3 className={ classes.title }>Filter Student</h3></Typography>
      <div className={ classes.spacer}/>
      <TextField
        className={ classes.text}
        name='lrnid'
        label='LRN ID'
        inputRef={ register }
        variant='outlined'
      />
      <div className={ classes.spacer}/>
      <TextField
        className={ classes.text}
        name='lastName'
        label='LastName'
        inputRef={ register }
        variant='outlined'
      />
      <div className={ classes.spacer} style={{flexGrow: 1}}/>
      <div className={ classes.buttonSubmit }>
        <Button
          type='submit'
          startIcon={<Search/>}
        >
          Search
        </Button>
      </div>
    </Paper>
  )
}

export default SimpleStudentCriteria