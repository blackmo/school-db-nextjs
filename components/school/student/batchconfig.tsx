import { makeStyles, Box, TextField, IconButton, Button } from "@material-ui/core"
import { Details_Style, Button_Style } from "../../../styles/make/component/student/registerStyle"
import { useForm } from "react-hook-form"
import { useState } from "react"
import { FolderOpen, Publish, Attachment } from '@material-ui/icons';


const useStyle = makeStyles(theme => ({
  ...Details_Style,
  ...Button_Style,
  root: {
    height: "100%",
    zIndex: 1,
    overflow: "hidden",
    position: "relative",
    display: "grid",
  },
  innerBox: {
    maxWidth: 500,
    margin: 'auto',
    textAlign: 'center'
  },
  spanToLeft: {
    textAlign: 'left',
    marginLeft: 20,
  },
  fileDisplay: {
    textAlign: 'center',
    minHeight: 65
  },
  actionNeutral: {
    '& .MuiButton-root': {
      minWidth: '150px',
      backgroundColor: '#0fa0cc',
      color: 'white',
    },
    '& .MuiButton-root:hover': {
        backgroundColor: '#51cdf2'
    }
  }
}))

const defaultType = ['register']
const defaultGradeLevels = [7, 8, 9, 10, 11, 12]
const defaultGradeSections = ['I', 'II', 'III', 'IV']

const BatchConfig = props => {

  const { onSubmit, onChange } = props

  const [types, setTypes] = useState(defaultType)
  const [gradleLevels, setGradeLevels] = useState(defaultGradeLevels)
  const [gradeSections, setGradeSections] = useState(defaultGradeSections)
  const [file, setFile] = useState<File>()

  const classes = useStyle()
  const { register, handleSubmit } = useForm()


  const handleUpload= event => {
    const csvFile = event.target.files[0]
    setFile(csvFile)
    onChange(csvFile)
  }

  return (
    <div className={classes.innerBox}>
      <Box component='h2'>
        Batch Register
      </Box>
      <form autoComplete='off' onSubmit={handleSubmit(onSubmit)}>
        <div className={classes.root}>
          <div className={classes.spacer} />
          <Box className={classes.spanToLeft} component='span'>Upload Type</Box>
          <TextField
            className={classes.text}
            variant='outlined'
            defaultValue='All'
            select
            inputRef={register}
            name={'type'}
            SelectProps={{
              native: true
            }}
          >
            {types.map((option, index) => {
              return <option key={`grade-level-${index}`} >{option}</option>
            })}
          </TextField>
          <div className={classes.spacer} />
          <Box className={classes.spanToLeft} component='span'>Grade Level</Box>
          <TextField
            className={classes.text}
            variant='outlined'
            defaultValue='All'
            select
            inputRef={register}
            name={'year'}
            SelectProps={{
              native: true
            }}
          >
            {gradleLevels.map((option, index) => {
              return <option key={`grade-level-${index}`} >{option}</option>
            })}
          </TextField>
          <div className={classes.spacer} />
          <Box className={classes.spanToLeft} component='span'>Grade Section</Box>
          <TextField
            className={classes.text}
            variant='outlined'
            defaultValue='All'
            select
            inputRef={register}
            name={'section'}
            SelectProps={{
              native: true
            }}
          >
            {gradeSections.map((option, index) => {
              return <option key={`grade-level-${index}`} >{option}</option>
            })}
          </TextField>
          <div className={classes.spacer} />
          <div className={classes.fileDisplay}>
            <Box>
              {
                () => {
                  if (file) {
                  return  <AttachmentText text={ file.name } />
                  }
                }
              }
            </Box>
          </div>
          <input
            accept='.csv'
            id='contained-button-file'
            type="file"
            style={{ display: 'none' }}
            onChange={ e => {handleUpload(e)}}
          />
          <div className={classes.actionNeutral}>
            <label htmlFor='contained-button-file'>
                <Button style={{ width: '100%' }} component='span' startIcon={<FolderOpen />}>
                  Attach CSV
                </Button>
            </label>
          </div>
          <div className={classes.submit}>
            <Button
              type='submit'
              style={{ width: '100%' }}
              startIcon={<Publish />}
              disabled={(file===null || file === undefined)}
            >
              Upload
            </Button>
          </div>
          <div className={classes.spacer} />
        </div>
      </form>
    </div>
  )
}

const AttachmentText = props => {
  return (
    <div style={{display: 'flex', justifyContent:'center'}}>
      <Attachment style={{height: 'auto'}}/>
      <p style={{marginLeft: 10}}>{props.text}</p>
    </div>
  )
}

export default BatchConfig