import { Grid, Typography, GridList, GridListTile, makeStyles, createStyles, Theme } from "@material-ui/core"
import PersonUtils from "../../../functions/person/PersonUtils"
import { Details_Style } from "../../../styles/make/component/student/registerStyle"
import { useState, useEffect } from "react"

const useStyle = makeStyles((theme: Theme)=>
  createStyles({
    ...Details_Style,
    root: {
      flexGrow: 1,
      maxWidth: 500,
      margin: 'auto',
      marginTop: 30,
      background: 'antiquewhite'
    },
    gridItem: {
      justifyContent: 'center',
      '& .MuiGridListTile-tile': {
        padding:20
      }
    },
    paper: {
      padding: theme.spacing(2),
      margin: 'auto',
      maxWidth: 500,
    },
    image: {
      width: 128,
      height: 128,
    },
    img: {
      margin: 'auto',
      display: 'block',
      maxWidth: '100%',
      maxHeight: '100%',
    },
    displayGridList: {
        display: 'flex',
      flexWrap: 'wrap',
      justifyContent: 'space-around',
      overflow: 'hidden',
      backgroundColor: theme.palette.background.paper,
    },
    gridContent: {
      '& .MuiGridListTile-tile': {
        margin:20
      }
    }
  })
)

export const BathRegistrationResultPane = props => {

  const { failed, successFull } = props
  const classes = useStyle()
  const [show, setShow] = useState(false)

  useEffect(() => {
    if (failed) {
      if (failed.length > 0) {
        setShow(true)
      } else {
        setShow(false)
      }
    } else {
      setShow(false)
    }
  })

  return (
    <div className={ classes.root} style={{ display: show? 'inherit': 'none'}} >
      <Grid  item container className={ classes.gridItem }>
          <FailedPane className={classes.gridContent} failed={ failed } />
      </Grid>
    </div>
  )
}

const FailedPane = props => {
  const {  failed } = props
  return (
    <Grid classes={props.className} item container>
      <div style={{background: '#f47777', width: '100%', textAlign:'center'}}>
      <Typography style={{color: 'white'}}>Failed Registration</Typography>
      </div>
      <GridList cellHeight={150} cols={1}>
        {
          failed? (failed.map((entry, index) => (
            <GridListTile key={`fialed-${index}`} cols={1}>
              <StudentName student={ entry.student }/>
              <Typography variant='inherit'>{ entry.errorDesc }</Typography>
            </GridListTile>
          ))): ''
        }

      </GridList>
    </Grid>
  )
}

const successFull = props => {
  const { students } = props

  return (
    <Grid item container>
      <GridList cellHeight={170} cols={1}>
        {
          students.map((student) => {
            <GridListTile key={student.lrnID} cols={1}>
              <StudentName student={ student }/>
              <Typography variant='caption'></Typography>
            </GridListTile>
          })
        }
      </GridList>
    </Grid>
  )
}

const StudentName= props => {
  const {student} = props

  return (
    <Grid item xs={12} container >
      <Grid item xs container direction='column' spacing={2}>
        <Grid item xs>
          <Typography gutterBottom variant='body2'>
            LRNID: {student.lrnID}
          </Typography>
          <Typography gutterBottom variant='body2'>
            {PersonUtils.fullName(student.person)}
          </Typography>
        </Grid>
      </Grid>
    </Grid>
  )
}