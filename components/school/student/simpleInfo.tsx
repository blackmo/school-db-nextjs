import { makeStyles, TextField, Button, IconButton, Tooltip } from "@material-ui/core"
import { Details_Style } from "../../../styles/make/component/student/registerStyle"
import { useState } from "react"
import { useForm } from "react-hook-form"
import { Edit } from "@material-ui/icons"

import WithFormDialog from "../../prompts/formDialog"

const usetStyle = makeStyles(theme => ({
  ...Details_Style,
  displayHorizontal: {
    padding: 0,
    display: 'flex',
    height: 30,
    '& button': {
      marginLeft: 20,
      color: 'white',
      background: '#187461',
    },
    '& button:hover': {
      backgroundColor: '#079A7B'
    }
  },
  smallIcon: {
    height: 25,
    width: 25
  }
}))

const SimpleInfo = props => {
  const { idx, name, idName, onUpdate, toolTip } = props
  const [openDialog, setOpenDialog] = useState(false)
  const { register, handleSubmit } = useForm()

  const handleClose = event => {
    setOpenDialog(false)
  }

  const handleNegativeAction = event => {
    setOpenDialog(false)
  }

  const handleEditId = event => {
    setOpenDialog(true)
  }

  const formSubmit = data => {
    onUpdate(data['id'])
    setOpenDialog(false)
  }


  const classes = usetStyle()
  return (
    <div>
      <div className={classes.displayHorizontal} style={{ display: idx ? 'inherit' : 'none' }}>
        <span>{idName}:</span>
        <span style={{ paddingLeft: 10 }}>{idx}</span>
        <Tooltip
          title={<span>{toolTip || 'Edit'}</span>}
        >
        <IconButton
          size='small'
          onClick={e => { handleEditId(e)}}
          className={ classes.submit}
        > 
          <Edit className={ classes.smallIcon }/>
        </IconButton>
        </Tooltip>
      </div>
      <div>
        <span>{name}</span>
      </div>
      <WithFormDialog
        title={ `Edit ${idName}` }
        handleClose={handleClose}
        negativeAction={ handleNegativeAction }
        handleSubmit={handleSubmit(formSubmit)}
        open={openDialog}
      >
        <div className={classes.root}>
          <TextField
            name='id'
            variant='outlined'
            className={classes.text}
            inputRef={register}
            defaultValue={idx}
          />
        </div>
      </WithFormDialog>
    </div>
  )
}

export default SimpleInfo