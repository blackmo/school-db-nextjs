import { CommonDetails, GuardianDetails } from "../../person/commonDetails"
import { Divider, makeStyles, Button } from "@material-ui/core"
import { useEffect, useState } from "react"
import { useForm } from "react-hook-form"
import { Edit } from "@material-ui/icons"
import { validateMobileNum } from "../../../utils/string"
import { composePerson } from "../../../functions/form/utils"
import { updatePersonDetails } from "../../../functions/person/PersonApiCalls"

import WithFormDialog from "../../prompts/formDialog"
import PersonDetails from "../../person/details"
import SimpleSnackBar from "../../prompts/SimpleSnackBar"
import PrompStatus from "../../../classes/prompt/PromptStatus"
import Person from "../../../classes/person/Person"
import LoadingDialog from "../../prompts/loadingDialog"
import ApiResponseSate from "../../../classes/response/ApiResponseState"
import Response from "../../../classes/response/Response"
import { detApiState } from "../../../functions/api/ApiUtils"

const INDEX_PERSON = 0
const INDEX_MOTHER = 1
const INDEX_FATHER = 2

const usetStyle = makeStyles(theme => ({
  root: {
    '& button': {
      minWidth: 100,
      color: 'white',
      background: '#187461',
    },
    '& button:hover': {
      backgroundColor: '#079A7B'
    }
  },
  spacer: {
    marginTop: 10,
    marginBottom: 10
  },
  divider: {
    width: 10,
    height: 'auto',
    display: 'inline-block'
  },
  displayVertical: {
    display: 'flex'
  }
}))

const defaultValue = {entityName: '', person: new Person(), code: ''}

const StudentInfo = props => {
  const {father, mother, student,onUpdate} = props
  const {register, handleSubmit, setValue} = useForm() 

  const [dialogTitle, setDialogTitle] = useState<String>()
  const [openStudentDialog, setOpenStudentDialog] = useState(false)
  const [openLoading, setOpenLoading] = useState(false)
  const [enableNumber, setEnableNumber] = useState(true)
  const [promptStatus, setStatus] = useState<PrompStatus>(new PrompStatus(false, '', 0))
  const [updateValue, setUpdateValue] =useState<{entityName: string, person: Person, code: String}>(defaultValue)
  const [apiState, setApiState] = useState<ApiResponseSate>(new ApiResponseSate())
  const [actionHidden, setActionHidden] = useState(true)

  const classes = usetStyle()
  useEffect(()=> {
    if (updateValue) {
      if (!updateValue.person.mobileNo) {
        setEnableNumber(false)
      } else {
        setEnableNumber(true)
      }
    }
  }, [updateValue])

  const onOpenDialog = index => {
    switch(index) {
      case INDEX_PERSON : {

        let updateValue = {
          entityName: 'student',
          person: student,
          code: 'update-student-person'
        }

        setDialogTitle('Edit Student Details')
        setUpdateValue(updateValue)
        break;
      }

      case INDEX_MOTHER: {
        let updateValue = {
          entityName: 'mother',
          person: mother,
          code: 'update-student-mother'
        }

        setDialogTitle('Edit Mother Details')
        setUpdateValue(updateValue)
        break;
      }

      case INDEX_FATHER: {
        let updateValue = {
          entityName: 'mother',
          person: father,
          code: 'update-student-father'
        }

        setDialogTitle('Edit Father Details')
        setUpdateValue(updateValue)
        break;
      }
    }
    setOpenStudentDialog(true)
  }

  const onSubmit = async (data) => {
    let person = composePerson(updateValue.entityName, data)
    person.id = updateValue.person.id


    let prompStatus = handleErrors(person)
    setStatus(prompStatus.prompt)

    if (!prompStatus.hasError) {
      setOpenStudentDialog(false)
      setOpenLoading(true)
      setApiState({...apiState, called: true})

      let response = await updatePersonDetails(person)
      let api = detApiState(response, 'Updated Details Successfully')

      setTimeout(async () => {
        setApiState(api)
        setTimeout(() => {
          handleApiResponse(api, response)
        }, 500)
      }, 500)

    }
  }

  const handleCloseStudentDialog = () => {
    setOpenStudentDialog(false)
  }

  const handleOnNextPrompt = (event, reason?) => {
    if (reason == 'clickeaway') {
      return
    }

    promptStatus.open = false
    setStatus({...promptStatus})
  }

  const handleErrors = (person: Person) => {
    let validNumber = validateMobileNum(person.mobileNo)
    let status = PrompStatus.SUCCESS
    let prompstatus = new PrompStatus(false, '', 0, status)
    let hasError = false
    

    if (!validNumber) {
      hasError = true
      status = PrompStatus.ERROR
      prompstatus = new PrompStatus(!validNumber, 'ERROR: Invalid Mobile Number', 3, status)
    }

    return { hasError: hasError, prompt: prompstatus }
  }

  const handleDialogClose = () => {
    setOpenLoading(false)
    setTimeout(()=>{
      setActionHidden(true)
      setApiState(new ApiResponseSate()) 
    })
  }

  const handleApiResponse = (api: ApiResponseSate, response: Response<Person> ) => {
    if (api.error) {
      setActionHidden(false)
    } else {
      setUpdateValue({ ...updateValue, person:  response.data})
      setOpenLoading(false)
      setTimeout(() => {
        onUpdate(updateValue.code, response.data)
        setApiState(new ApiResponseSate())
      }, 500)
    }
  }


  return (
    <div >
      <div className={ classes.root }>
      <CommonDetails simpleInfo={student} />
      <div className={ classes.spacer }/>
      <Button  startIcon={<Edit/>} onClick={e=>{ onOpenDialog(INDEX_PERSON) }}>Edit Student</Button>
      <Divider className={ classes.spacer } />
      <GuardianDetails father={ father } mother={ mother } />
      <div className={ classes.spacer }/>
      <Button  startIcon={<Edit/>} onClick={e=>{onOpenDialog(INDEX_FATHER)}}> Edit Father</Button>
      <div className={ classes.divider } />
      <Button  startIcon={<Edit/>} onClick={e=>{onOpenDialog(INDEX_MOTHER)}}>Edit Mother</Button>
      </div>
      <WithFormDialog
        title={ dialogTitle }
        open={ openStudentDialog }
        negativeAction={ handleCloseStudentDialog }
        handleSubmit={ handleSubmit(onSubmit) }
        labeling={{ positive: 'save'}} 
      >
        <PersonDetails
          entityName = { updateValue.entityName}
          form={{ register, setValue }}
          person={ updateValue.person }
          enableNumber={ enableNumber }
          required
          disable={false}
        />
      </WithFormDialog>
      <SimpleSnackBar
        open={promptStatus.open}
        onClose={handleOnNextPrompt}
        message={promptStatus.message}
        severity= {promptStatus.severity}
        variant='filled'
      />
      <LoadingDialog
        title={'Register Student'}
        open={ openLoading}
        message={ apiState.message || 'Saving Details'}
        onClose={ handleDialogClose }
        actionTitle={'OK'}
        loading={ apiState.called }
        success={ apiState.success }
        error ={ apiState.error }
        type = 'save'
        actionHidden = {actionHidden}
       />
    </div>
  )
}

export default StudentInfo