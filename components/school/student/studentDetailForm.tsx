import PersonDetails from '../../person/details'
import {makeStyles, TextField} from '@material-ui/core'
import {Details_Style} from '../../../styles/make/component/student/registerStyle'
import BaseStudent from '../../../classes/school/BaseStudent'



const useStyle = makeStyles(theme => (Details_Style))
const StudentDetailForm = (props) => {
    const {value, form, resetCounter, ...others} = props
    const classes = useStyle()
    const student = props.student ? props.student : new BaseStudent()

    return (
      <div>
          <div className={classes.root}>
            <TextField
              className={classes.text}
              variant="outlined"
              inputRef = {form.register}
              defaultValue={student.lrnID || ""}
              name = "student_lrnID"
              label="LRN ID"
              required
            ></TextField>
          </div>
          <PersonDetails
            entityName='student'
            form={form}
            required
            person = {student.person}
            resetCounter = {resetCounter}
            checkedNumber
          />
          <div className={classes.spacer} />
          {props.children}
        <div className={classes.spacer} />
      </div>
    );
}


export default StudentDetailForm