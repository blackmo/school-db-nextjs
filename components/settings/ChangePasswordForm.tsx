import { makeStyles, TextField } from "@material-ui/core"
import { Details_Style } from "../../styles/make/component/student/registerStyle"

const useStyle = makeStyles(theme => (Details_Style))

const ChangePasswordForm = props => {
  const{register} = props
  const classes = useStyle()
  return (
    <div className={ classes.root }>
      <TextField
        className={ classes.text }
        label='old password'
        required
        type='password'
        variant='outlined'
        inputRef={ register }
        name='oldpw'
      />
     <div className={ classes.spacer }/>
      <TextField
        className={ classes.text }
        label='new password'
        required
        type='password'
        variant='outlined'
        inputRef={ register }
        name='newpw'
      />
     <div className={ classes.spacer }/>
      <TextField
        className={ classes.text }
        label='confirm password'
        required
        type='password'
        variant='outlined'
        inputRef={ register }
        name='conpw'
      />
     <div className={ classes.spacer }/>
    </div>
  )
}

export default ChangePasswordForm