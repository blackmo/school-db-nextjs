
import '../../styles/layout/layout.global.scss'
import '../../styles/cards/cards.global.scss'

import { useState, useRef, useEffect } from 'react';
import { useForm } from 'react-hook-form';
import { CardActionArea, CardContent, Typography } from '@material-ui/core';
import { updateUserPassword } from '../../functions/user/UserApiCall';

import VpnKeyIcon from '@material-ui/icons/VpnKey';
import WithFormDialog from '../prompts/formDialog';
import ChangePasswordForm from './ChangePasswordForm';
import LoadingOnApiCall from '../prompts/ApiCaller';
import ChangePassword from '../../classes/user/ChangePassword';
import PrompStatus from '../../classes/prompt/PromptStatus';
import SimpleSnackBar from '../prompts/SimpleSnackBar';


const defaultPw = new ChangePassword('', '', '')
const defaultStatus  = new PrompStatus(false, 'Password Input Does not Match', 0, PrompStatus.ERROR)

export const SettingsComponent = props => {
  const [openChange, setOpenChange] = useState(false)
  const [call, setCall] = useState(false)
  const [promptStatus, setStatus] = useState<PrompStatus>(defaultStatus)

  const password = useRef<ChangePassword>(defaultPw)
  const {register, reset, handleSubmit} = useForm()

  useEffect(()=> {
    promptStatus.open = false
    setStatus({...promptStatus})
  }, [])

  const handleCancel = () => {
    setOpenChange(false)
    reset()
  }

  const onSubmit = data => {
    const pw = composePassword(data)
    password.current = pw
    checkPassword(pw)
  }

  const apiCall = async () => {
    return await updateUserPassword(password.current)
  }

  const handleOnError = (data) => {
    setCall(false)
  }

  const handleOpen = () => {
    setOpenChange(true)
  }

  const checkPassword = (password: ChangePassword) => {
    const equal = equalPassword(password)

    promptStatus.message = equal.message
    if (!equal.status) {
      promptStatus.open = true
      setStatus({...promptStatus})
    }
    setOpenChange(!equal.status)
    setCall(equal.status)
  }

  const handleOnNextPrompt = (event, reason?) => {
    if (reason == 'clickeaway') {
      return
    }

    promptStatus.open = false
    setStatus({...promptStatus})
  }

  return (
    <div className='inside_layout line_titles'>
      <h3>Settings</h3>
      <div className='horizontal_align'>
        <CardButon
          icon = {<VpnKeyIcon/> }
          label = 'Change Password'
          onClick ={ handleOpen }
        />
        <div className='space5' />
      </div>
      <WithFormDialog
        title= 'Change Password'
        open={ openChange }
        negativeAction={ handleCancel}
        handleSubmit={ handleSubmit(onSubmit) }
        labeling={{ positive: 'save'}} 
      >
        <ChangePasswordForm register={ register }/>
      </WithFormDialog>
      <SimpleSnackBar
        open={promptStatus.open}
        onClose={handleOnNextPrompt}
        message={promptStatus.message}
        severity= {promptStatus.severity}
        variant='filled'
      />
      <LoadingOnApiCall
        title='Password Change'
        message='Confirm Change Password'
        actionTitle='Close'
        type='save'
        call={{
          call: call,
          apiCall: apiCall,
          onSuccess: () => {setCall(false)},
          succesMessage: 'Successfully Change Password',
          onError: handleOnError,
        }}
      />
    </div>
  );
}

const CardButon = props => {
  const {label, icon, onClick} = props
  return (
    <CardActionArea disableRipple
      onClick = {e => { onClick() }}
    >
      <div className='card_captor' style={{'width':150}}>
        <CardContent>
          {icon}
          <Typography>{label}</Typography>
        </CardContent>
      </div>
    </CardActionArea>
  )
}

const composePassword = (data): ChangePassword => {
  return new ChangePassword(
    data['oldpw'],
    data['newpw'],
    data['conpw']
  )
}

const equalPassword = (password: ChangePassword): {status: boolean, message: string } => {
  let message = 'Password Input Does not Match'
  const status1 = password.newPassword == password.conPassword
  const status2 = password.oldPassword == password.newPassword
   
  if (status2) {
    message = 'New Password must not be equal to Old Password'
  }
  return {status:(status1 && !status2), message: message}
}