import { Drawer, ListItem, ListItemIcon, ListItemText, makeStyles, useTheme, createStyles } from "@material-ui/core"
import { useState, useEffect, useRef } from "react"
import IconButton from '@material-ui/core/IconButton'
import ChevronLeftIcon from '@material-ui/icons/ChevronLeft'
import ChevronRightIcon from '@material-ui/icons/ChevronRight'

const CommonDrawer = props => {
  const { contentList, width, onClick, withIcon, color, handleDrawerClose, shrinkable, ...others } = props

  const useStyle = makeStyles(theme => createStyles({
    listItem: {
      color: "#ffffff",
      '&.MuiListItem-button': {
        maxHeight: '70px',
      }
    },
    icon: {
      color: 'white'
    },
    drawer: {
      width: width,
      flexShrink: 0,
    },
    drawerPaper: {
      width: width,
    },
    drawerHeader: {
      '& .MuiIconButton-root': {
        color: 'white'
      },
      display: 'flex',
      alignItems: 'center',
      padding: theme.spacing(0, 1),
      ...theme.mixins.toolbar,
      justifyContent: 'flex-end',
    },
  }))

  const [currentLink, setCurrentLink] = useState(0)

  const classes = useStyle()
  const theme = useTheme()

  return (
    <Drawer
      className={classes.drawer}
      {...others}
    >
      {
        shrinkable &&
        <div className={classes.drawerHeader}>
          <IconButton onClick={handleDrawerClose}>
            {theme.direction === 'ltr' ? <ChevronLeftIcon /> : <ChevronRightIcon />}
          </IconButton>
        </div>
      }
      <div>
        {contentList.map((item, index) => (
          <ListItem
            key={index}
            button
            selected={index === currentLink}
            onClick={() => {
              setCurrentLink(index)
              onClick(index)
            }}
          >
            {withIcon && item.icon && <ListItemIcon className={classes.icon}>{<item.icon />}</ListItemIcon>}
            <ListItemText
              primary={
                <p className={classes.listItem}>{item.label}</p>
              }
            />
          </ListItem>
        ))}
      </div>
    </Drawer>
  )

}

export default CommonDrawer