# RND Project: School-DB-NextJs
Front End Project Using React with Next js as frame work, using react hooks, in functional style

with backend host running on locahost:3000 ATM

## Project setup
navigate to project root directory

```bash
npm install
npm run build
npm run start -- -p <some-port>

```

## Contributing
pull request are welcome