export const Details_Style = {
    root: {
      display: 'grid',
    },
    text: {
        margin: 10,
        '& label.Mui-focused': {
          color: '#079A7B',
        },
        '& .MuiInput-underline:after': {
          borderBottomColor: '#079A7B',
        },
        '& .MuiOutlinedInput-root': {
          '& fieldset': {
            borderColor: '#187461',
          },
          '&:hover fieldset': {
            borderColor: '#079A7B',
          },
          '&.Mui-focused fieldset': {
            borderColor: '#079A7B',
          },
          '&.Mui-disabled:hover fieldset': {
                borderColor: '#BDBDBD',
          }
        }
    },
    spacer: {
        marginTop: 20
    },
    submit : {

        '& button' : {
            width : '100%',
            marginTop: 20,
            backgroundColor: '#187461',
            color: 'white',
        },
        '& button:hover': {
            backgroundColor: '#079A7B'
        }
    },
    cancel : {

        '& button' : {
            width : '100%',
            marginBottom: 20,
            backgroundColor: '#E0523C',
            color: 'white',
        },
        '& button:hover': {
            backgroundColor: '#FC7F6C'
        }
    }
}