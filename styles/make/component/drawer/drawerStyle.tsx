import { createStyles } from "@material-ui/core";



export const drawerStyle = (theme, others?) => {
  let drawerWidth = setWidth(others)
  const extra = others? others : {}

  return createStyles({
    ...extra,
    appBar: {
      color: 'black',
      marginLeft: 0,
      backgroundColor: 'white',
      transition: theme.transitions.create(['margin', 'width'], {
        easing: theme.transitions.easing.sharp,
        duration: theme.transitions.duration.leavingScreen,
      }),
    },
    appBarShift: {
      // width: `calc(100% - ${drawerWidth}px)`,
      // marginLeft: drawerWidth,
      transition: theme.transitions.create(['margin', 'width'], {
        easing: theme.transitions.easing.easeOut,
        duration: theme.transitions.duration.enteringScreen,
      }),
    },
    menuButton: {
      marginRight: theme.spacing(2),
    },
    hide: {
      display: 'none',
    },
    paper: {
      '& .MuiPaper-rounded': {
        borderRadius: 0,
      },
      paddingLeft: 30,
      paddingRight: 30,
      paddingBottom: 0,
      width: '100%'
    },
    spacer: {
      marginTop: 20,
      marginBottom: 20
    },
    content: {
      flexGrow: 1,
      transition: theme.transitions.create('margin', {
        easing: theme.transitions.easing.sharp,
        duration: theme.transitions.duration.leavingScreen,
      }),
      marginLeft: -drawerWidth,
      marginBottom: 20
    },
    contentShift: {
      transition: theme.transitions.create('margin', {
        easing: theme.transitions.easing.easeOut,
        duration: theme.transitions.duration.enteringScreen,
      }),
      marginLeft: 0,
    },
    root: {
      overflow: "hidden",
      display: "flex",
      height: '100%',
      minHeight: 540,
    },
    container: {
      margin: 20,
      marginTop: 0,
      background: "white",
      overFlow: 'hidden',
      position: 'relative'
    },
    drawerPaper: {
      position: "relative",
      width: drawerWidth,
      backgroundColor: "#187461"
    },
    toolbar: theme.mixins.toolbar,
  })
}

const setWidth = (others) => {
  if (others) {
    return others.width? others.width : 240
  }
  return 240
}