export const Details_Style = {
  root: {
    display: 'grid',
  },
  text: {
    margin: 10,
    '& label.Mui-focused': {
      color: '#079A7B',
    },
    '& .MuiInput-underline:after': {
      borderBottomColor: '#079A7B',
    },
    '& .MuiOutlinedInput-root': {
      '& fieldset': {
        borderColor: '#187461',
      },
      '&:hover fieldset': {
        borderColor: '#079A7B',
      },
      '&.Mui-focused fieldset': {
        borderColor: '#079A7B',
      },
      '&.Mui-disabled:hover fieldset': {
            borderColor: '#BDBDBD',
      }
    }
  },
  textActiveDisabled: {
    margin: 10,
    "& .MuiOutlinedInput-root": {
      "&.Mui-disabled:hover fieldset": {
        borderColor: "#187461"
      },
      "&.Mui-disabled fieldset": {
        borderColor: "#187461"
      },
    }
  },
  textWhite: {
    textColor: 'white',
    backgroundColor: '#fffff',
    margin: 10,
    marginLeft: 20,
    marginRight: 20,
    '& .MuiInput-underline:after': {
      borderBottomColor: '#ffffff',
    },
    '& .MuiOutlinedInput-root': {
       background: '#ffffff',
      '& fieldset': {
        borderColor: '#ffffff',
      },
      '&:hover fieldset': {
        borderColor: '#ffffff',
      },
      '&.Mui-focused fieldset': {
        borderColor: '#ffffff',
      },
      '&.Mui-disabled': {
        background: '#c8c4c4',
        borderColor: '#c8c4c4',
      },
      '&.Mui-disabled:hover fieldset': {
        borderColor: '#c8c4c4',
      },
      '&.Mui-disabled fieldset': {
        borderColor: '#c8c4c4',
      },
    },
    '& .MuiInputLabel-outlined' : {
      color: '#ffffff'
    },
    '& .MuiFormHelperText-root.Mui-error' : {
      color: 'white'
    }
  },
    spacer: {
        marginTop: 20
    },
    submit : {

        '& button' : {
            width : '100%',
            marginTop: 20,
            backgroundColor: '#187461',
            color: 'white',
        },
        '& button:hover': {
            backgroundColor: '#079A7B'
        }
    },
    cancel : {

        '& button' : {
            width : '100%',
            marginBottom: 20,
            backgroundColor: '#E0523C',
            color: 'white',
        },
        '& button:hover': {
            backgroundColor: '#FC7F6C'
        }
    }
}

export const Button_Style = {
  buttonPositive: {
    '& button': {
      minWidth: '150px',
      backgroundColor: '#187461',
      color: 'white',
    },
    '& button:hover': {
        backgroundColor: '#079A7B'
    }
  },

  buttonNegative: {
    '& button' :  {
      minWidth: '150px',
      backgroundColor: '#E0523C',
      color: 'white',
    },
   '& button:hover': {
      backgroundColor: '#FC7F6C'
    }
  },
  buttonNeutral: {
    '& button': {
      minWidth: '150px',
      backgroundColor: '#0fa0cc',
      color: 'white',
    },
    '& button:hover': {
        backgroundColor: '#51cdf2'
    }
  },
}

export const Style_Search_Student = {
  root: {
    display: 'grid'
  },
  text: {
    '& label.Mui-focused': {
      color: '#079A7B',
    },
    '& .MuiInput-underline:after': {
      borderBottomColor: '#079A7B',
    },
    '& .MuiOutlinedInput-root': {
      '& fieldset': {
        borderColor: '#187461',
      },
      '&:hover fieldset': {
        borderColor: '#079A7B',
      },
      '&.Mui-focused fieldset': {
        borderColor: '#079A7B',
      },
      '&.Mui-disabled:hover fieldset': {
            borderColor: '#BDBDBD',
      }
    }
  }
}

export const Details_Edit = {
  root: {
    '& button': {
      minWidth: 100,
      color: 'white',
      background: '#187461',
    },
    '& button:hover': {
      backgroundColor: '#079A7B'
    }
  },
  displayHorizontal: {
    padding: 0,
    display: 'flex',
    height: 30,
    '& button': {
      marginLeft: 20,
      color: 'white',
      background: '#187461',
    },
    '& button:hover': {
      backgroundColor: '#079A7B'
    }
  },
  smallIcon: {
    height: 25,
    width: 25
  }
}