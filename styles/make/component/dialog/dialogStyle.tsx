import { green, red } from "@material-ui/core/colors"
import { createStyles } from "@material-ui/core"

export const loadingDialogStyle = theme => createStyles({
    root: {
        display: 'flex',
        alignItems: 'center',
        minHeight: 100
    },
    wrapper: {
        margin: theme.spacing(1),
        position: 'relative',
    },
    buttonSuccess: {
        backgroundColor: green[500],
        '&:hover': {
            backgroundColor: green[700],
        },
    },
    buttonError: {
        '&.Mui-disabled' : {
            backgroundColor: red[500],
            color: 'white',
        },
    },
    fabProgress: {
        color: green[500],
        position: 'absolute',
        top: -6,
        left: -6,
        zIndex: 1,
    },
    buttonProgress: {
        color: green[500],
        position: 'absolute',
        top: '50%',
        left: '50%',
        marginTop: -12,
        marginLeft: -12,
    },
    buttonTextAction: {
        '&.Mui-disabled': {
            color: '#796464'
        }, 
        textTransform: 'none',
        fontSize: 'medium'
    },
})