FROM node:12

RUN mkdir /app

COPY . /app

EXPOSE 3000

WORKDIR /app


RUN npm install
RUN npm run build


CMD [ "npm", "run",  "start" ]
